package com.nirons.midland.propertycms.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by jaquesyang on 15/6/10.
 */
@ComponentScan({"com.nirons.midland.propertycms"})
@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.nirons.midland.propertycms.repository")
@EntityScan(basePackages = "com.nirons.midland.propertycms")
public class Application extends SpringBootServletInitializer {


    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }
}
