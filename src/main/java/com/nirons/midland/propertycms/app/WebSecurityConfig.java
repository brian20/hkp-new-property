package com.nirons.midland.propertycms.app;

import com.nirons.midland.propertycms.entity.User;
import com.nirons.midland.propertycms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/dashboard/**").authenticated()
                .antMatchers("/profile/**").authenticated()
                .antMatchers("/entry/**").authenticated()
                .antMatchers("/approval/**").hasRole("ADMIN")
                .antMatchers("/email_notification/**").hasRole("ADMIN")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/")
                .defaultSuccessUrl("/dashboard", true)
                .usernameParameter("username")
                .passwordParameter("password")
                .failureUrl("/?error")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/?logout")
                .permitAll();
*/
        http.csrf().disable()
        .authorizeRequests()
                .antMatchers("/cms/**").authenticated()
                .antMatchers("/cms/approval/**").hasRole("ADMIN")
                .antMatchers("/cms/email_notification/**").hasRole("ADMIN")
                .antMatchers("/cms/entry/delete").hasRole("ADMIN")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/cms/").permitAll()
                .defaultSuccessUrl("/cms/dashboard", true)
                .usernameParameter("username")
                .passwordParameter("password")
                .failureUrl("/cms/?error")
                .and()
                .logout()
                .logoutUrl("/cms/logout")
                .logoutSuccessUrl("/cms/?logout")
                .permitAll();

    }


    @Autowired
    UserRepository userRepository;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider daoAuthenticationProvider =
                new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(new Md5PasswordEncoder());
        daoAuthenticationProvider.setUserDetailsService(new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
                User user = userRepository.findByUsername(s);
                return user;
            }
        });
        auth.authenticationProvider(daoAuthenticationProvider);
    }
}