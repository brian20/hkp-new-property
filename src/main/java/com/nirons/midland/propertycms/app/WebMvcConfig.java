package com.nirons.midland.propertycms.app;

//import net.sourceforge.html5val.Html5ValDialect;
//import nz.net.ultraq.thymeleaf.LayoutDialect;

import com.nirons.midland.propertycms.dao.DeveloperDao;
import com.nirons.midland.propertycms.dao.DistrictDao;
import com.nirons.midland.propertycms.dao.SettingDao;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.apache.catalina.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.validation.Validator;

/**
 * Created by jaquesyang on 15/6/10.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    protected static Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    TemplateEngine templateEngine;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/error").setViewName("error");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(localeChangeInterceptor());
        //registry.addInterceptor(new ThymeleafLayoutInterceptor());
        registry.addInterceptor(mylocaleInterceptor());
    }

    @Bean
    public LocaleResolver localeResolver() {
        logger.debug("localeResolver");
        SessionLocaleResolver slr = new SessionLocaleResolver();

        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        logger.debug("localeChangeInterceptor");
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Configuration
    @ConditionalOnClass({SpringSecurityDialect.class/*, Html5ValDialect.class, GetUrlDialect.class*/})
    protected static class ThymeleafSecurityDialectConfiguration {

        @Bean
        public SpringSecurityDialect securityDialect() {
            return new SpringSecurityDialect();
        }


        /*
        @Bean
        public Html5ValDialect html5ValDialect() {
            return new Html5ValDialect();
        }
        @Bean
        public GetUrlDialect getUrlDialect(){
            return new GetUrlDialect();
        }
        */

    }


    @Autowired
    MessageSource messageSource;

    @Bean
    public Validator localValidatorFactoryBean() {

        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();

        bean.setValidationMessageSource(messageSource);

        return bean;
    }

    @Autowired
    SettingDao settingDao;

    @Autowired
    DistrictDao districtDao;

    @Autowired
    DeveloperDao developerDao;

    @Bean
    public ServletContextListener servletContextListener() {
        return new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent servletContextEvent) {
                logger.debug("contextInitialized");

                settingDao.reloadStaticSettings();

                districtDao.reloadDistrictCache();

                developerDao.reloadDeveloperCache();
            }

            @Override
            public void contextDestroyed(ServletContextEvent servletContextEvent) {

            }
        };
    }

    @Bean
    public HttpSessionListener httpSessionListener() {
        return new HttpSessionListener() {
            @Override
            public void sessionCreated(HttpSessionEvent httpSessionEvent) {
                logger.debug("sessionCreated");

            }

            @Override
            public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
                logger.debug("sessionDestroyed");

            }
        };
    }

    @Bean
    public HandlerInterceptor mylocaleInterceptor() {
        return new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

                String serverName = httpServletRequest.getServerName();
                String sessionServerName = (String) httpServletRequest.getSession().getAttribute("SESSION_SERVER_NAME");
                if (serverName != null && (sessionServerName == null || !Utils.equals(serverName, sessionServerName))) {
                    logger.debug("serverName: {}", serverName);
                    logger.debug("sessionServerName: {}", sessionServerName);
                    httpServletRequest.getSession().setAttribute("SESSION_SERVER_NAME", serverName);
                    String newLocale = "zh_HK";
                    if (Constants.STATIC_SC_APP_SERVER.toLowerCase().startsWith("http://" + serverName.toLowerCase()) ||
                            Constants.STATIC_SC_APP_SERVER.toLowerCase().startsWith("https://" + serverName.toLowerCase())) {
                        newLocale = "zh_CN";
                    } else if (Constants.STATIC_EN_APP_SERVER.toLowerCase().startsWith("http://" + serverName.toLowerCase()) ||
                            Constants.STATIC_EN_APP_SERVER.toLowerCase().startsWith("https://" + serverName.toLowerCase())) {
                        newLocale = "en_HK";
                    }

                    LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(httpServletRequest);
                    if (localeResolver == null) {
                        throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");
                    }

                    localeResolver.setLocale(httpServletRequest, httpServletResponse, StringUtils.parseLocaleString(newLocale));
                }

                return true;
            }

            @Override
            public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {


            }

            @Override
            public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

            }
        };
    }

}
