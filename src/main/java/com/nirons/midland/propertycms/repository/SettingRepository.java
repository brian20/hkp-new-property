package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.Setting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by jaquesyang on 15/6/23.
 */
public interface SettingRepository extends JpaRepository<Setting, String> {
    @Query("SELECT e.value FROM Setting e WHERE e.key = ?1")
    String stringValue(String key);
}
