package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.EntryDocUrl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by jaquesyang on 15/9/9.
 */
public interface EntryDocUrlRepository extends JpaRepository<EntryDocUrl, Long> {
    List<EntryDocUrl> findByPropertyId(Long propertyId);

    List<EntryDocUrl> findByPropertyIdAndType(Long propertyId, String type);
}
