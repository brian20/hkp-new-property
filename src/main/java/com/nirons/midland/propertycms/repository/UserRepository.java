package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jaquesyang on 15/6/16.
 */
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
