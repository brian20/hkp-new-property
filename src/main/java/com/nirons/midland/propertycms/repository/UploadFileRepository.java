package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.UploadFile;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jaquesyang on 15/6/22.
 */
public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {
    @Modifying
    @Transactional
    @Query("UPDATE UploadFile e SET e.path = ?2 WHERE e.id = ?1")
    int updatePath(Long id, String path);

    @Modifying
    @Transactional
    @Query("UPDATE UploadFile e SET e.status = '"+ Constants.STATUS_DELETE+"' WHERE  e.id = ?1")
    int del(Long id);
}
