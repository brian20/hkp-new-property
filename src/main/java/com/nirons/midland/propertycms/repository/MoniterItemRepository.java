package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.MoniterItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/13.
 */
@Transactional
public interface MoniterItemRepository extends JpaRepository<MoniterItem, Long> {
    @Query("SELECT e FROM MoniterItem e WHERE e.propertyId = ?1 ORDER BY e.id")
    List<MoniterItem> findByPropertyId(Long propertyId);
}
