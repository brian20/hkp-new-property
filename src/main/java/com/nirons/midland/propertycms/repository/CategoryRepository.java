package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/13.
 */
@Transactional
public interface CategoryRepository extends JpaRepository<Category,Long> {
    @Query("SELECT e FROM Category e ORDER BY e.id")
    List<Category> findAll();
}
