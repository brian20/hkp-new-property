package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.entity.EntryDoc;
import com.nirons.midland.propertycms.entity.UploadFile;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jaquesyang on 15/6/22.
 */
public interface EntryDocRepository extends JpaRepository<EntryDoc, Long> {
    @Query("SELECT e FROM EntryDoc e WHERE e.entry = ?1 AND e.status <> '" + Constants.STATUS_DELETE + "' ORDER BY e.ordering ASC, e.id DESC ")
    List<EntryDoc> findByEntry(Entry entry);


    @Query("SELECT e FROM EntryDoc e WHERE e.entry = ?1 AND LOWER(e.type) = LOWER(?2) AND e.status <> '" + Constants.STATUS_DELETE + "'")
    List<EntryDoc> findByEntryAndType(Entry entry, String type, Sort sort);

    @Modifying
    @Transactional
    @Query("UPDATE EntryDoc e SET e.status = ?2 WHERE e.id = ?1")
    int updateStatus(Long id, String status);

    @Modifying
    @Transactional
    @Query("UPDATE EntryDoc e SET e.status = '" + Constants.STATUS_DELETE + "' WHERE e.id = ?1")
    int del(Long id);

    @Modifying
    @Transactional
    @Query("UPDATE EntryDoc e SET e.file = ?2 WHERE e.id = ?1")
    int updateFile(Long id, UploadFile uploadFile);


}
