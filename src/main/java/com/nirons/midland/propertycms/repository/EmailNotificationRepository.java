package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.EmailNotification;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 * Created by jaquesyang on 15/7/3.
 */
public interface EmailNotificationRepository extends JpaRepository<EmailNotification, Long> {
    @Query("SELECT e FROM EmailNotification e WHERE e.status <> '" + Constants.STATUS_DELETE + "' AND (?1 IS NULL  OR LOWER(e.email) LIKE ?1) ")
    Page<EmailNotification> findByEmail(String email, Pageable pageable);

    @Query("SELECT e FROM EmailNotification e WHERE e.status <> '" + Constants.STATUS_DELETE + "' AND LOWER(e.email) = LOWER(?1) ")
    EmailNotification findByEmail(String email);
}
