package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jaquesyang on 15/8/18.
 */
public interface DeveloperRepository extends JpaRepository<Developer, Long> {
}
