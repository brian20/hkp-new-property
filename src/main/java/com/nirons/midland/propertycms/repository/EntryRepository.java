package com.nirons.midland.propertycms.repository;

import com.nirons.midland.propertycms.entity.Developer;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by jaquesyang on 15/6/18.
 */
@Transactional
public interface EntryRepository extends JpaRepository<Entry, Long> {
    @Query("SELECT e FROM Entry e WHERE e.status <> '" + Constants.STATUS_OVERRIDED + "' ORDER BY e.modified DESC")
    Page<Entry> findByLastUpdate(Pageable pageable);

    @Query(value = "SELECT COUNT (e.property_id) FROM fh_property e WHERE e.status <> '" + Constants.STATUS_OVERRIDED + "' AND TRUNC(e.modified) = :date", nativeQuery = true)
    int countByUpdateDate(@Param("date") @Temporal(TemporalType.DATE) Date date);

    @Query(value = "SELECT e FROM Entry e WHERE e.status <> '" + Constants.STATUS_OVERRIDED + "'" +
            " AND (:name IS NULL OR :name = ''" +
            " OR LOWER(e.nameTc) LIKE LOWER(:name)" +
            " OR LOWER(e.nameEn) LIKE LOWER(:name))" +
            " AND e.modified BETWEEN :start AND :end")
    Page<Entry> findByNameAndModified(@Param("name") String name,
                                      @Param("start") @Temporal(TemporalType.DATE) Date start,
                                      @Param("end") @Temporal(TemporalType.DATE) Date end,
                                      Pageable pageable);

    int countByStatus(String status);

    Entry findByUidAndStatus(String uid, String status);


    @Query("SELECT e FROM Entry e WHERE e.status <> '" + Constants.STATUS_OVERRIDED + "'" +
            " AND (:status IS NULL OR :status = '' OR e.status = :status)" +
            " AND (:name IS NULL OR :name = ''" +
            " OR LOWER(e.nameTc) LIKE LOWER(:name)" +
            " OR LOWER(e.nameEn) LIKE LOWER(:name)) ")
    Page<Entry> findByNameAndStatus(@Param("name") String name, @Param("status") String status, Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE (e.status = '" + Constants.STATUS_APPROVED + "' OR e.status = '" + Constants.STATUS_PENDING + "') ORDER BY e.ordering, LOWER(e.nameTc)")
    List<Entry> findForInitReorder();


    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "')" +
            " AND (:name IS NULL OR :name = ''" +
            " OR LOWER(e.nameEn) LIKE LOWER(:name)" +
            " OR LOWER(e.nameTc) LIKE LOWER(:name)) ORDER BY e.ordering, LOWER(e.nameTc)")
    List<Entry> findApprovedByName(@Param("name") String name);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "')" +
            " AND (:name IS NULL OR :name = ''" +
            " OR LOWER(e.nameTc) LIKE LOWER(:name)" +
            " OR LOWER(e.nameEn) LIKE LOWER(:name))")
    Page<Entry> findApprovedByName(@Param("name") String name, Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "')" +
            " AND (:name IS NULL OR :name = ''" +
            " OR LOWER(e.nameTc) LIKE LOWER(:name)" +
            " OR LOWER(e.nameEn) LIKE LOWER(:name))")
    Page<Entry> findHkpApprovedByName(@Param("name") String name, Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.hot = TRUE ORDER BY e.ordering, LOWER(e.nameEn)")
    List<Entry> findApprovedHot();

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.hot = TRUE")
    Page<Entry> findApprovedHot(Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.hot = TRUE")
    Page<Entry> findHkpApprovedHot(Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "(LOWER(e.district.urlKey) LIKE LOWER(:district) " +
            " OR LOWER(e.district.nameTc) LIKE LOWER(:district) " +
            " OR LOWER(e.district.nameEn) LIKE LOWER(:district)) ORDER BY e.ordering, LOWER(e.nameEn)")
    List<Entry> findApprovedByDistrict(@Param("district") String district);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "(LOWER(e.district.urlKey) LIKE LOWER(:district) " +
            " OR LOWER(e.district.nameTc) LIKE LOWER(:district) " +
            " OR LOWER(e.district.nameEn) LIKE LOWER(:district))")
    Page<Entry> findApprovedByDistrict(@Param("district") String district, Pageable pageable);


    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.district.id = :district ORDER BY e.ordering, LOWER(e.nameEn)")
    List<Entry> findApprovedByDistrict(@Param("district") Long district);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.district.id = :district")
    Page<Entry> findApprovedByDistrict(@Param("district") Long district, Pageable pageable);



    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.district.id = :district")
    Page<Entry> findHkpApprovedByDistrict(@Param("district") Long district, Pageable pageable);


    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "(e.district.id = :district) AND " +
            "(LOWER(e.areaTc) LIKE LOWER(:area) " +
            " OR LOWER(e.areaEn) LIKE LOWER(:area))")
    Page<Entry> findApprovedByDistrictAndArea(@Param("district") Long district, @Param("area") String area, Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "(e.district.id = :district) AND " +
            "(LOWER(e.areaTc) LIKE LOWER(:area) " +
            " OR LOWER(e.areaEn) LIKE LOWER(:area))")
    Page<Entry> findHkpApprovedByDistrictAndArea(@Param("district") Long district, @Param("area") String area, Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "e.developer.id = :developer ORDER BY e.ordering, LOWER(e.nameEn)")
    List<Entry> findApprovedByDeveloper(@Param("developer") Long developer);

    @Query("SELECT e FROM Entry e WHERE e.hidden = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "e.developer.id = :developer")
    Page<Entry> findApprovedByDeveloper(@Param("developer") Long developer, Pageable pageable);



    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND " +
            "e.developer.id = :developer")
    Page<Entry> findHkpApprovedByDeveloper(@Param("developer") Long developer, Pageable pageable);


    @Query("SELECT e FROM Entry e WHERE (e.status = '" + Constants.STATUS_APPROVED + "')" +
            " AND (LOWER(e.uid) LIKE LOWER(:urlKey) OR LOWER(e.urlKeyEn) LIKE LOWER(:urlKey)" +
            " OR LOWER(e.urlKeyTc) LIKE LOWER(:urlKey))")
    List<Entry> findApprovedByUrlKey(@Param("urlKey") String urlKey);


    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') " +
            " AND (:district IS NULL OR :district = '' OR e.district.urlKey = :district) " +
            " AND (:area IS NULL OR :area = '' OR LOWER(e.areaTc) LIKE LOWER(:area) OR LOWER(e.areaEn) LIKE LOWER(:area)) " +
            " AND (:name IS NULL OR :name = '' OR LOWER(e.nameTc) LIKE :name OR LOWER(e.nameEn) LIKE :name)")
    Page<Entry> findApprovedByDistrictAndAreaAndName(@Param("district") String district,
                                                     @Param("area") String area,
                                                     @Param("name") String name,
                                                     Pageable pageable);

    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') " +
            " AND (:district IS NULL OR :district = '' OR e.district.urlKey = :district) " +
            " AND (:area IS NULL OR :area = '' OR LOWER(e.areaTc) LIKE LOWER(:area) OR LOWER(e.areaEn) LIKE LOWER(:area)) " +
            " AND (:name IS NULL OR :name = '' OR LOWER(e.nameTc) LIKE :name OR LOWER(e.nameEn) LIKE :name) ORDER BY e.ordering ASC , e.id DESC ")
    List<Entry> findApprovedByDistrictAndAreaAndName(@Param("district") String district,
                                                     @Param("area") String area,
                                                     @Param("name") String name);

    @Query("SELECT e FROM Entry e WHERE e.hiddenForHkp = FALSE AND (e.status = '" + Constants.STATUS_APPROVED + "') AND e.hot = TRUE " +
            " AND (:name IS NULL OR :name = '' OR LOWER(e.nameTc) LIKE :name OR LOWER(e.nameEn) LIKE :name)")
    Page<Entry> findApprovedHotByName(@Param("name") String name,
                                      Pageable pageable);
}
