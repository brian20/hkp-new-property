package com.nirons.midland.propertycms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jaquesyang on 15/6/25.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestResp implements Serializable {

    private int errno = -1;
    private String message = "";
    private Object data;

    public RestResp(int errno) {
        this(errno, "");
    }

    public RestResp(int errno, String message) {
        this();

        this.errno = errno;
        this.message = message;
    }

    public RestResp(int errno, String message, Object... dataKeyAndValue) {
        this(errno, message);

        if (dataKeyAndValue != null) {
            if (dataKeyAndValue.length == 1) {
                setData(dataKeyAndValue[0]);
            } else {
                HashMap<String, Object> map = new HashMap<>();
                for (int i = 0; i < dataKeyAndValue.length; i += 2) {
                    if (i + 1 >= dataKeyAndValue.length) {
                        break;
                    }
                    map.put(dataKeyAndValue[i].toString(), dataKeyAndValue[i + 1]);
                }
                setData(map);
            }
        }
    }
}
