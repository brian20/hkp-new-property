package com.nirons.midland.propertycms.model;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;

/**
 * Created by jaquesyang on 15/7/3.
 */

public class SinglePage<T> implements Page<T> {
    private List<T> list;
    private Sort sort;

    public SinglePage(List<T> list) {
        this(list, null);
    }

    public SinglePage(List<T> list, Sort sort) {
        this.list = list;
        this.sort = sort;
    }

    @Override
    public int getTotalPages() {
        return 1;
    }

    @Override
    public long getTotalElements() {
        return list.size();
    }

    @Override
    public int getNumber() {
        return 0;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public int getNumberOfElements() {
        return list.size();
    }

    @Override
    public List<T> getContent() {
        return list;
    }

    @Override
    public boolean hasContent() {
        return list != null;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public boolean isFirst() {
        return true;
    }

    @Override
    public boolean isLast() {
        return true;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

    @Override
    public Pageable nextPageable() {
        return null;
    }

    @Override
    public Pageable previousPageable() {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
