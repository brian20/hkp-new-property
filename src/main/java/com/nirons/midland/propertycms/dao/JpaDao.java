package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.Log;
import com.nirons.midland.propertycms.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by jaquesyang on 15/6/18.
 */
@SuppressWarnings("unchecked")
public abstract class JpaDao<T, ID extends Serializable> {

    protected Class<T> clazz;

    @PersistenceContext
    EntityManager entityManager;

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public JpaDao() {
        setClazz((Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    public final void setClazz(Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    public T findOne(ID id) {
        return entityManager.find(clazz, id);
    }

    public List<T> findAll() {
        return entityManager.createQuery("from " + clazz.getName())
                .getResultList();
    }

    public T create(T entity) {
        entityManager.persist(entity);

        return entity;
    }

    public T update(T entity) {
        return entityManager.merge(entity);
    }

    public void delete(T entity) {
        entityManager.remove(entity);
    }

    public void deleteById(ID entityId) {
        T entity = findOne(entityId);
        delete(entity);
    }


    public Log createLog(String username, String action, String description) {
        Log log = new Log();

        log.setAction(action);
        log.setUsername(username);
        log.setDescription(description);

        entityManager.persist(log);

        return log;
    }
}
