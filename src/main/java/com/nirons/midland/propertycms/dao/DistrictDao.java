package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.Area;
import com.nirons.midland.propertycms.entity.District;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/12.
 */
@Service
@Transactional
@SuppressWarnings("unchecked")
public class DistrictDao extends JpaDao<District, Long> {
    public List<District> districtWithAreaList() {
        //List<District> districts = entityManager.createQuery("SELECT e FROM District e WHERE e.status = :active ORDER BY e.ordering").setParameter("active", Constants.STATUS_ACTIVE).getResultList();
        List<District> districts = entityManager.createQuery("SELECT e FROM District e ORDER BY e.ordering").getResultList();
        for (District district : districts) {
            district.setNameSc(Chinese.toSc(district.getNameTc()));

            List<Area> areas = areasByDistrict(district);
            for (Area area : areas) {
                if (!district.getAreas().contains(area)) {
                    district.getAreas().add(area);
                }
            }
        }
        return districts;
    }

    public List<Area> areasByDistrict(District district) {
        List<Area> areas = entityManager.createNativeQuery("SELECT DISTINCT area_tc, area_en FROM fh_property WHERE (area_tc IS NOT NULL) AND district_id = :district AND status = :approved ORDER BY area_tc, area_en", Area.class)
                .setParameter("district", district.getId())
                .setParameter("approved", Constants.STATUS_APPROVED)
                .getResultList();

        return areas;
    }

    public void reloadDistrictCache() {
        Cache.add("districts", districtWithAreaList());
    }
}
