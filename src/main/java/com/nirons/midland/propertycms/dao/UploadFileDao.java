package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.UploadFile;
import com.nirons.midland.propertycms.entity.User;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by jaquesyang on 15/6/26.
 */
@Service
@Transactional
public class UploadFileDao extends JpaDao<UploadFile, Long> {
    @Autowired
    SettingDao settingDao;

    public UploadFile create(UploadFile uploadFile, String user) {

        uploadFile.setCreatedBy(user);
        entityManager.persist(uploadFile);

        createLog(user, Constants.ACTION_CREATE, "create upload_file " + uploadFile.getId());

        if (uploadFile.getMultipartFile() != null && uploadFile.getMultipartFile().getSize() > 0) {
            uploadFile.setName(uploadFile.getMultipartFile().getOriginalFilename());
            uploadFile.setSize(uploadFile.getMultipartFile().getSize());

            String fileStoreDir = settingDao.stringSetting(Constants.FILE_STORE_DIR, Constants.FILE_STORE_DIR_DEFAULT);

            String date = Constants.DF_yyMMdd.format(new Date());
            File dir = new File(fileStoreDir + File.separator + date);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            String fileName = uploadFile.getId() + "_" + Utils.uploadFileName(uploadFile.getName());
            String path = date + File.separator + fileName;
            uploadFile.setPath(path);
            uploadFile.setSize(uploadFile.getMultipartFile().getSize());
            File saveToFile = new File(dir.getAbsolutePath() + File.separator + fileName);
            try {
                uploadFile.getMultipartFile().transferTo(saveToFile);
            } catch (IOException e) {
                e.printStackTrace();
                logger.error(e.getMessage(), e);
            }
        }

        return uploadFile;
    }
}
