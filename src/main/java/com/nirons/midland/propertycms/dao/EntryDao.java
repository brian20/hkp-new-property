package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.*;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by jaquesyang on 15/6/18.
 */
@Service
@Transactional
public class EntryDao extends JpaDao<Entry, Long> {

    @Autowired
    SettingDao settingDao;

    @Autowired
    UploadFileDao uploadFileDao;

    @Autowired
    EntryDocDao entryDocDao;

    public Entry findPending(String uid) {
        return Utils.findByUniqueField(entityManager, Entry.class, "uid", uid, "status", Constants.STATUS_PENDING);
    }


    public Entry duplicateForPending(Long id, String user) {
        Entry entry = findOne(id);
        if (entry != null && !Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
            if (!Constants.STATUS_APPROVED.equalsIgnoreCase(entry.getStatus())) {
                return null;
            }
            String ql = "SELECT e FROM Entry e WHERE e.uid = :uid AND e.status = :pending";
            Query q = entityManager.createQuery(ql, Entry.class);
            q.setParameter("uid", entry.getUid());
            q.setParameter("pending", Constants.STATUS_PENDING);
            q.setMaxResults(1);

            Entry pending = null;
            try {
                pending = (Entry) q.getSingleResult();
            } catch (Exception e) {
                //e.printStackTrace();
            }
            if (pending == null) {
                //entry.setModified(new Date());

                pending = new Entry();
                pending.updateFields(entry);
                pending.setCreatedBy(user);

                entityManager.persist(pending);

                pending.setOrdering(entry.getOrdering());

                createLog(user, Constants.ACTION_CREATE, "create pending fh_property " + entry.getId() + " for fh_property " + id);


                List<EntryDoc> entryDocs = Utils.findByField(entityManager, EntryDoc.class, "entry", entry, "status !", Constants.STATUS_DELETE);
                for (EntryDoc doc : entryDocs) {
                    EntryDoc entryDoc = new EntryDoc();
                    entryDoc.setEntry(pending);
                    entryDoc.updateFields(doc);

                    entryDoc.setCreatedBy(user);

                    entityManager.persist(entryDoc);

                    createLog(user, Constants.ACTION_CREATE, "create fh_property_doc " + entryDoc.getId() + " for fh_property " + pending.getId());
                }

                List<MoniterItem> items = Utils.findByField(entityManager, MoniterItem.class, "propertyId", entry.getId());
                for (MoniterItem item : items) {
                    MoniterItem newItem = new MoniterItem();

                    newItem.setPropertyId(pending.getId());
                    newItem.setCategoryId(item.getCategoryId());
                    newItem.setTrackingUrl(item.getTrackingUrl());

                    entityManager.persist(newItem);

                }


                List<EntryDocUrl> docUrls = Utils.findByField(entityManager, EntryDocUrl.class, "propertyId", entry.getId());
                for (EntryDocUrl doc : docUrls) {
                    EntryDocUrl entryDoc = new EntryDocUrl();
                    entryDoc.setPropertyId(pending.getId());
                    entryDoc.setType(doc.getType());
                    entryDoc.setUrl(doc.getUrl());

                    entityManager.persist(entryDoc);

                    createLog(user, Constants.ACTION_CREATE, "create fh_property_doc_url " + entryDoc.getId() + " for fh_property " + pending.getId());
                }
            }

            return pending;

        }

        return entry;
    }

    public Entry update(Entry entry, Long entryId, String user) {


        Entry pending = duplicateForPending(entryId, user);

        if (pending == null) {
            return null;
        }

        UploadFile uploadFile = entry.getUploadFile();
        if (uploadFile != null) {
            if (uploadFile.getId() == null) {
                uploadFile.setCreatedBy(user);
                uploadFile = uploadFileDao.create(uploadFile, user);
                entry.setUploadFile(uploadFile);
            }
        } else {
            entry.setUploadFile(pending.getUploadFile());
        }

        pending.updateFields(entry);
        pending.setModified(new Date());
        pending.setModifiedBy(user);

        createLog(user, Constants.ACTION_UPDATE, "update fh_property " + pending.getId());


        saveOrUpdateEntryDocUrl(entry.getDocUrls(), pending.getId());


        return pending;
    }

    public Entry create(Entry entry, String user) {

        entry.setCreatedBy(user);
        if (entry.getUploadFile() != null) {
            entry.getUploadFile().setCreatedBy(entry.getCreatedBy());
        }

        entityManager.persist(entry);

        createLog(user, Constants.ACTION_CREATE, "create fh_property " + entry.getId());

        saveOrUpdateEntryDocUrl(entry.getDocUrls(), entry.getId());

        UploadFile uploadFile = entry.getUploadFile();
        if (uploadFile != null && uploadFile.getMultipartFile() != null && uploadFile.getMultipartFile().getSize() > 0) {
            String fileStoreDir = settingDao.stringSetting(Constants.FILE_STORE_DIR, Constants.FILE_STORE_DIR_DEFAULT);

            String date = Constants.DF_yyMMdd.format(new Date());
            File dir = new File(fileStoreDir + File.separator + date);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            String fileName = uploadFile.getId() + "_" + Utils.uploadFileName(uploadFile.getName());
            String path = date + File.separator + fileName;
            uploadFile.setPath(path);
            uploadFile.setSize(uploadFile.getMultipartFile().getSize());
            File saveToFile = new File(dir.getAbsolutePath() + File.separator + fileName);
            try {
                uploadFile.getMultipartFile().transferTo(saveToFile);
            } catch (IOException e) {
                e.printStackTrace();
                logger.error(e.getMessage(), e);
            }
        }

        return entry;
    }


    public EntryDoc addEntryDoc(EntryDoc doc, Long entryId, String user) {
        Entry pending = duplicateForPending(entryId, user);

        if (pending == null) {
            return null;
        }

        String ql = "SELECT MAX(e.ordering) FROM EntryDoc e WHERE e.entry = :entry AND e.type = :type";
        Query q = entityManager.createQuery(ql);
        q.setParameter("entry", pending);
        q.setParameter("type", doc.getType());
        int ordering = Utils.getQueryCount(q) + 1;

        doc.setOrdering(ordering);

        doc.setEntry(pending);

        doc.setCreatedBy(user);
        doc.getFile().setCreatedBy(doc.getCreatedBy());

        switch (doc.getType()) {
            case Constants.DOC_SALES_MANUAL_FULL:
            case Constants.DOC_SALES_MANUAL_PART:
                doc.setCategoryId(2L);
                break;
            case Constants.DOC_PRICE_LIST:
                doc.setCategoryId(3L);
                break;
            case Constants.DOC_SALES_ARRANGEMENT:
                doc.setCategoryId(4L);
                break;
            case Constants.DOC_COMPLETED_RECORD:
                doc.setCategoryId(5L);
                break;
        }


        entityManager.persist(doc);

        createLog(user, Constants.ACTION_CREATE, "create fh_property doc " + doc.getId());

        String date = Constants.DF_yyMMdd.format(new Date());
        String fileStoreDir = settingDao.stringSetting(Constants.FILE_STORE_DIR, Constants.FILE_STORE_DIR_DEFAULT);
        File dir = new File(fileStoreDir + File.separator + date);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        UploadFile uploadFile = doc.getFile();
        if (uploadFile.getMultipartFile() != null && uploadFile.getMultipartFile().getSize() > 0) {
            String fileName = uploadFile.getId() + "_" + Utils.uploadFileName(uploadFile.getName());
            String path = date + File.separator + fileName;

            uploadFile.setPath(path);

            File file = new File(dir.getAbsolutePath() + File.separator + fileName);
            try {
                uploadFile.getMultipartFile().transferTo(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return doc;

    }

    public UploadFile docUpdateUploadFile(Long entryId, Long docId, String lang, UploadFile uploadFile, String user) {
        if (entryId == null || docId == null || StringUtils.isEmpty(lang)) {
            return null;
        }
        Entry pending = duplicateForPending(entryId, user);
        if (pending == null) {
            return null;
        }

        EntryDoc doc = entityManager.find(EntryDoc.class, docId);
        EntryDoc docToReplace = null;
        if (doc == null || !doc.getEntry().getId().equals(entryId)) {
            return null;
        }
        if (pending.getId().equals(entryId)) {
            docToReplace = doc;
        } else {
            docToReplace = Utils.findByUniqueField(entityManager, EntryDoc.class, "entry", pending, "ordering", doc.getOrdering(), "type", doc.getType());
        }

        if (docToReplace == null) {
            return null;
        }

        if (uploadFile != null && uploadFile.getId() == null) {
            uploadFileDao.create(uploadFile);
        }

        entryDocDao.updateUploadFile(docToReplace.getId(), lang, uploadFile);

        return uploadFile;
    }


    public EntryDoc updateEntryDoc(Long entryId, EntryDoc doc, String user) {
        if (entryId == null || doc == null || doc.getId() == null) {
            return null;
        }
        Entry pending = duplicateForPending(entryId, user);
        if (pending == null) {
            return null;
        }

        EntryDoc entryDoc = entityManager.find(EntryDoc.class, doc.getId());
        EntryDoc docToReplace = null;
        if (entryDoc == null || (!entryDoc.getEntry().getId().equals(entryId) && !entryDoc.getEntry().getId().equals(pending.getId()))) {
            return null;
        }
        if (pending.getId().equals(entryId)) {
            docToReplace = entryDoc;
        } else {
            docToReplace = Utils.findByUniqueField(entityManager, EntryDoc.class, "entry", pending, "ordering", doc.getOrdering(), "type", doc.getType());
        }

        if (docToReplace == null) {
            return null;
        }


        UploadFile uploadFile = doc.getFile();

        if (uploadFile != null && uploadFile.getId() == null) {
            uploadFile.setCreatedBy(user);
            uploadFileDao.create(uploadFile, user);
            doc.setFile(uploadFile);
        } else {
            doc.setFile(docToReplace.getFile());
        }

        docToReplace.setModifiedBy(user);
        docToReplace.setModified(new Date());
        docToReplace.updateFields(doc);

        createLog(user, Constants.ACTION_UPDATE, "update fh_property doc " + docToReplace.getId());

        return docToReplace;
    }


    public int deleteEntryDoc(Long entryId, Long docId, String user) {
        if (entryId == null || docId == null) {
            return -1;
        }
        Entry pending = duplicateForPending(entryId, user);
        if (pending == null) {
            return -1;
        }

        EntryDoc doc = entityManager.find(EntryDoc.class, docId);
        EntryDoc docToReplace = null;
        if (doc == null || !doc.getEntry().getId().equals(entryId)) {
            return -1;
        }
        if (pending.getId().equals(entryId)) {
            docToReplace = doc;
        } else {
            docToReplace = Utils.findByUniqueField(entityManager, EntryDoc.class, "entry", pending, "ordering", doc.getOrdering(), "type", doc.getType());
        }

        if (docToReplace == null) {
            return -1;
        }


        return entryDocDao.delete(docToReplace.getId(), user);
    }

    private String propertyToField(String property) {
        if (property != null) {
            switch (property) {
                case "uid":
                    return "u_id";
                case "nameTc":
                    return "name_tc";
                case "nameEn":
                    return "name_en";
                case "nameSc":
                    return "name_tc";
                case "phaseNum":
                    return "phase_num";
                case "phaseNumEn":
                    return "phase_num_en";
                case "phaseNameEn":
                    return "phase_name_en";
                case "phaseNameSc":
                    return "phase_name_tc";
                case "areaEn":
                    return "area_en";
                case "areaTc":
                    return "area_tc";
                case "areaSc":
                    return "area_tc";
                case "addrTc":
                    return "addr_tc";
                case "addrEn":
                    return "addr_en";
                case "addrSc":
                    return "addr_tc";
                case "projectUrl":
                    return "project_url";
                case "salesManualUrl":
                    return "sales_manual_url";
                case "printDate":
                    return "print_date";
                case "viewDate":
                    return "view_date";
                case "createdBy":
                    return "created_by";
                case "modifiedBy":
                    return "modified_by";
                case "approvedBy":
                    return "approved_by";
                case "urlKeyTc":
                    return "url_key_tc";
                case "urlKeyEn":
                    return "url_key_en";
                default:
                    return property2Field(property);
            }
        }
        return null;
    }


    private String property2Field(String property) {
        if (StringUtils.isEmpty(property)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(property.charAt(0));
        for (int i = 1; i < property.length(); i++) {
            char c = property.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                sb.append("_");
            }
            sb.append(c);
        }

        return sb.toString().toLowerCase();
    }

    @SuppressWarnings("unchecked")
    public Page<Entry> listing(String name, String status, Pageable pageable) {

        String inner = "SELECT e.* , ROW_NUMBER() OVER (PARTITION BY u_id ORDER BY status desc) AS rn FROM fh_property e " +
                "WHERE e.status IN ('" + Constants.STATUS_APPROVED + "', '" + Constants.STATUS_PENDING + "') AND " +
                "(:name IS NULL OR LOWER(e.name_tc) LIKE LOWER(:name) OR LOWER(e.name_en) LIKE LOWER(:name))";
        if (!StringUtils.isEmpty(status)) {
            inner = inner + " AND e.status = '" + status + "'";
        }

        String countQl = "SELECT COUNT(*) FROM (" + inner + ") t WHERE t.rn = 1";
        logger.debug("count query: {}", countQl);
        Query query = entityManager.createNativeQuery(countQl);
        query.setParameter("name", name);
        long count = Utils.getQueryCount(query);
        logger.debug("count : {}", count);

        final List<Entry> content;

        if (count > 0) {
            StringBuilder sb = new StringBuilder(inner);
            StringBuilder orderBy = new StringBuilder();
            if (pageable != null && pageable.getSort() != null) {
                for (Sort.Order order : pageable.getSort()) {
                    if (orderBy.length() > 0) {
                        orderBy.append(", ");
                    }
                    String field = propertyToField(order.getProperty());
                    if (field == null) {
                        continue;
                    }
                    orderBy.append("e." + propertyToField(order.getProperty()));

                    if (Sort.Direction.DESC.equals(order.getDirection())) {
                        orderBy.append(" DESC ");
                    }
                }

                if (orderBy.length() > 0) {
                    sb.append(" ORDER BY ");
                    sb.append(orderBy);
                }
            }

            String table = "SELECT t.* FROM (" + sb.toString() + ") t WHERE t.rn = 1";
            logger.debug("table: {}", table);
            String qlString = table;

            query = entityManager.createNativeQuery(qlString, Entry.class).setParameter("name", name);

            if (pageable != null && pageable.getPageSize() > 0) {

                int page = pageable.getPageNumber();
                int size = pageable.getPageSize();
                query.setMaxResults(size);
                if (page == 0) {
                    //qlString += " AND rownum <= " + size;
                } else {
                    /*
                    int first = page + 1 * size;
                    int second = page * size;

                    qlString = "select entry.* from (" + table + " AND rownum <= " + first + ") fh_property where rownum_ > " + second;
                    */
                    query.setFirstResult(page * size);

                }


            }
            logger.debug("qlString: {}", qlString);


            content = query.getResultList();

        } else {
            content = new ArrayList<>();
        }


        return new Page<Entry>() {
            @Override
            public int getTotalPages() {
                int size = getSize();
                if (size <= 0) {
                    return 1;
                }
                long pages = getTotalElements() / size;
                if (getTotalElements() % size != 0) {
                    pages++;
                }
                return (int) pages;
            }

            @Override
            public long getTotalElements() {
                return count;
            }

            @Override
            public int getNumber() {
                return pageable == null ? 0 : pageable.getPageNumber();
            }

            @Override
            public int getSize() {
                return pageable == null ? -1 : pageable.getPageSize();
            }

            @Override
            public int getNumberOfElements() {
                return content.size();
            }

            @Override
            public List<Entry> getContent() {
                return content;
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Sort getSort() {
                return pageable == null ? null : pageable.getSort();
            }

            @Override
            public boolean isFirst() {
                return getNumber() == 0;
            }

            @Override
            public boolean isLast() {
                if (pageable == null) {
                    return true;
                }

                return getNumber() == getTotalPages() - 1;
            }

            @Override
            public boolean hasNext() {
                return !isLast();
            }

            @Override
            public boolean hasPrevious() {
                return !isFirst();
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Entry> iterator() {
                return content.iterator();
            }
        };
    }

    public long sort(Collection<String> unsorted, Collection<String> sorted, String user) {
        long count = 1;
        if (unsorted != null && !unsorted.isEmpty()) {
            StringBuilder sb = new StringBuilder("UPDATE Entry e SET e.ordering = :ordering WHERE ");
            //sb.append(Utils.getArrayParamsQueryString(unsorted, "e", "uid"));
            sb.append(" e.uid IN :unsorted");
            Query query = entityManager.createQuery(sb.toString());
            query.setParameter("ordering", Constants.UNSORTED_VALUE);
            //Utils.setArrayParamsQuery(query, unsorted, "e", "uid");
            query.setParameter("unsorted", unsorted);

            count += query.executeUpdate();
        }

        if (sorted != null && !sorted.isEmpty()) {
            StringBuilder sb = new StringBuilder("UPDATE Entry e SET e.ordering = :ordering WHERE  e.uid = :uid");
            Query query = entityManager.createQuery(sb.toString());
            int i = 0;
            for (String uid : sorted) {
                query.setParameter("ordering", Constants.UNSORTED_VALUE - sorted.size() + i);
                query.setParameter("uid", uid);

                count += query.executeUpdate();

                i++;
            }
        }

        if (count > 0) {
            createLog(user, Constants.ACTION_SORT, "sort entry");
        }

        return count;
    }

    public Entry approve(Long id, String user) {
        Entry entry = findOne(id);
        if (entry == null) {
            return null;
        }
        if (Constants.STATUS_APPROVED.equals(entry.getStatus())) {
            return entry;
        }
        if (!Constants.STATUS_PENDING.equals(entry.getStatus())) {
            return null;
        }

        Entry approved = Utils.findByUniqueField(entityManager, Entry.class, "uid", entry.getUid(), "status", Constants.STATUS_APPROVED);

        String qlString = "UPDATE Entry e SET e.status = :approved , e.approvedBy = :user , e.modified = :date WHERE e.id = :id";
        Query query = entityManager.createQuery(qlString);
        query.setParameter("approved", Constants.STATUS_APPROVED);
        query.setParameter("id", id);
        query.setParameter("user", user);
        query.setParameter("date", new Date());
        query.executeUpdate();

        createLog(user, Constants.ACTION_APPROVE, "approve pending fh_property " + id);

        if (approved != null) {
            approved.setStatus(Constants.STATUS_OVERRIDED);
            approved.setModified(new Date());

            createLog(user, Constants.ACTION_OVERRIDE, "override approved fh_property " + approved.getId());
        }

        return entry;

    }

    public Entry reject(Long id, String user) {
        Entry entry = findOne(id);
        if (entry == null) {
            return null;
        }
        if (Constants.STATUS_REJECTED.equals(entry.getStatus())) {
            return entry;
        }
        if (!Constants.STATUS_PENDING.equals(entry.getStatus())) {
            return null;
        }

        String qlString = "UPDATE Entry e SET e.status = :rejected , e.approvedBy = :user , e.modified = :date WHERE e.id = :id";
        Query query = entityManager.createQuery(qlString);
        query.setParameter("rejected", Constants.STATUS_REJECTED);
        query.setParameter("id", id);
        query.setParameter("user", user);
        query.setParameter("date", new Date());
        query.executeUpdate();

        createLog(user, Constants.ACTION_REJECT, "reject pending fh_property " + id);


        return entry;

    }

    public boolean validUrlKey(String uid, String urlKey) {
        if (StringUtils.isEmpty(urlKey) || StringUtils.isEmpty(uid) || Utils.memberOf(urlKey.toLowerCase(), Constants.KEY_WORDS)) {
            return false;
        }
        String qlString = "SELECT COUNT(e) FROM Entry e WHERE :uid != LOWER(e.uid)" +
                " AND (e.status LIKE :approved OR e.status LIKE :pending)" +
                " AND (LOWER(e.urlKeyTc) LIKE :urlKey OR LOWER(e.urlKeyEn) LIKE :urlKey)";
        int result = Utils.getQueryCount(entityManager.createQuery(qlString)
                .setParameter("uid", uid.toLowerCase())
                .setParameter("urlKey", urlKey.toLowerCase())
                .setParameter("approved", Constants.STATUS_APPROVED)
                .setParameter("pending", Constants.STATUS_PENDING));

        return result == 0;
    }

    public boolean validName(String uid, String name) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(uid) || Utils.memberOf(name.toLowerCase(), Constants.KEY_WORDS)) {
            return false;
        }
        String qlString = "SELECT COUNT(e) FROM Entry e WHERE :uid != LOWER(e.uid)" +
                " AND (e.status LIKE :approved OR e.status LIKE :pending)" +
                " AND (LOWER(e.nameTc) LIKE :name OR LOWER(e.nameEn) LIKE :name)";
        int result = Utils.getQueryCount(entityManager.createQuery(qlString)
                .setParameter("uid", uid.toLowerCase())
                .setParameter("name", name.toLowerCase())
                .setParameter("approved", Constants.STATUS_APPROVED)
                .setParameter("pending", Constants.STATUS_PENDING));

        return result == 0;
    }

    public boolean uniqueIdExists(String uid) {
        return Utils.getQueryCount(entityManager.createQuery("SELECT COUNT(e) FROM Entry e WHERE e.uid LIKE :uid AND (e.status LIKE :approved OR e.status LIKE :pending)")
                .setParameter("uid", uid)
                .setParameter("approved", Constants.STATUS_APPROVED)
                .setParameter("pending", Constants.STATUS_PENDING)) > 0;
    }

    public void delete(Long id, String user) {
        Entry entry = entityManager.find(Entry.class, id);
        if (entry != null) {
            entry.setModified(new Date());
            entry.setModifiedBy(user);
            entry.setStatus(Constants.STATUS_OVERRIDED);

            entityManager.createNativeQuery("UPDATE fh_property SET u_id = :uid WHERE property_id = :id")
                    .setParameter("uid", "DEL-" + entry.getUid())
                    .setParameter("id", id)
                    .executeUpdate();

            createLog(user, Constants.ACTION_REJECT, "delete fh_property " + id);
        }
    }


    public void saveOrUpdateEntryDocUrl(List<EntryDocUrl> docUrls, Long propertyId) {
        if (docUrls == null || docUrls.isEmpty()) {
            return;
        }
        String qlString = "FROM EntryDocUrl e WHERE e.propertyId = :propertyId AND e.type = :type";
        Query query = entityManager.createQuery(qlString);
        for (EntryDocUrl docUrl : docUrls) {


            query.setParameter("propertyId", propertyId);
            query.setParameter("type", docUrl.getType());

            EntryDocUrl entityDocUrl = (EntryDocUrl) Utils.getSingleResult(query);
            if (entityDocUrl != null) {
                entityDocUrl.setUrl(docUrl.getUrl());
            } else {
                entityDocUrl = new EntryDocUrl();
                entityDocUrl.setPropertyId(propertyId);
                entityDocUrl.setType(docUrl.getType());
                entityDocUrl.setUrl(docUrl.getUrl());

                entityManager.persist(entityDocUrl);
            }
        }
    }
}
