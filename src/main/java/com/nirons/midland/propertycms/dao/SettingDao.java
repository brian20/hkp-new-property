package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.Setting;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jaquesyang on 15/6/23.
 */
@Service
@Transactional
public class SettingDao extends JpaDao<Setting, String> {
    public String stringSetting(String key, String defaultValue) {
        Setting setting = findOne(key);
        if (setting == null) {
            return defaultValue;
        }
        return setting.getValue();
    }

    public Boolean booleanSetting(String key, Boolean defaultValue) {
        String value = stringSetting(key, null);
        if (value == null) {
            return defaultValue;
        }
        return "true".equalsIgnoreCase(value);
    }

    public int intSetting(String key, int defaultValue) {
        try {
            return Integer.valueOf(stringSetting(key, null));
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public double doubleSetting(String key, double defaultValue) {
        try {
            return Double.valueOf(stringSetting(key, null));
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public void reloadStaticSettings() {

        Constants.STATIC_FILE_SERVER = stringSetting(Constants.FILE_SERVER, Constants.FILE_SERVER_DEFAULT);
        Constants.STATIC_FILE_STORE_DIR = stringSetting(Constants.FILE_STORE_DIR, Constants.FILE_STORE_DIR_DEFAULT);
        Constants.STATIC_ASSETS_SERVER = stringSetting(Constants.ASSETS_SERVER, Constants.ASSETS_SERVER_DEFAULT);

        Constants.STATIC_APP_SERVER = stringSetting(Constants.APP_SERVER, Constants.APP_SERVER_DEFAULT);
        Constants.STATIC_EN_APP_SERVER = stringSetting(Constants.EN_APP_SERVER, Constants.EN_APP_SERVER_DEFAULT);
        Constants.STATIC_SC_APP_SERVER = stringSetting(Constants.SC_APP_SERVER, Constants.SC_APP_SERVER_DEFAULT);

        Constants.STATIC_TRICK_FIRST_TIME_URL = stringSetting(Constants.TRICK_FIRST_TIME_URL, Constants.TRICK_FIRST_TIME_URL_DEFAULT);

        Constants.STATIC_PROTOCOL = stringSetting(Constants.PROTOCOL, Constants.PROTOCOL_DEFAULT);
        Constants.STATIC_TPL_TOP_MAIN_TAG = stringSetting(Constants.TPL_TOP_MAIN_TAG, Constants.TPL_TOP_MAIN_TAG_DEFAULT);
        Constants.STATIC_TPL_TOP_TITLE_TAG = stringSetting(Constants.TPL_TOP_TITLE_TAG, Constants.TPL_TOP_TITLE_TAG_DEFAULT);

        Constants.STATIC_DEV_MODE = booleanSetting(Constants.DEV_MODE, Constants.DEV_MODE_DEFAULT);

        Constants.STATIC_SRPE_URL_TC = stringSetting(Constants.SRPE_URL_TC, Constants.SRPE_URL_TC_DEFAULT);
        Constants.STATIC_SRPE_URL_EN = stringSetting(Constants.SRPE_URL_EN, Constants.SRPE_URL_EN_DEFAULT);
        Constants.STATIC_SRPE_URL_SC = stringSetting(Constants.SRPE_URL_SC, Constants.SRPE_URL_SC_DEFAULT);

        Constants.STATIC_HKP_FILE_SERVER = stringSetting(Constants.HKP_FILE_SERVER, Constants.HKP_FILE_SERVER_DEFAULT);

        Constants.STATIC_HEADER_URL_TC = stringSetting(Constants.HEADER_URL_TC, Constants.HEADER_URL_TC_DEFAULT);
        Constants.STATIC_HEADER_URL_SC = stringSetting(Constants.HEADER_URL_SC, Constants.HEADER_URL_SC_DEFAULT);
        Constants.STATIC_HEADER_URL_EN = stringSetting(Constants.HEADER_URL_EN, Constants.HEADER_URL_EN_DEFAULT);

        Constants.STATIC_FOOTER_URL_TC = stringSetting(Constants.FOOTER_URL_TC, Constants.FOOTER_URL_TC_DEFAULT);
        Constants.STATIC_FOOTER_URL_SC = stringSetting(Constants.FOOTER_URL_SC, Constants.FOOTER_URL_SC_DEFAULT);
        Constants.STATIC_FOOTER_URL_EN = stringSetting(Constants.FOOTER_URL_EN, Constants.FOOTER_URL_EN_DEFAULT);

    }
}
