package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.EmailNotification;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by jaquesyang on 15/7/3.
 */
@Service
@Transactional
public class EmailNotificationDao extends JpaDao<EmailNotification, Long> {

    public EmailNotification create(EmailNotification bean, String user) {
        bean.setModified(new Date());
        bean.setCreated(new Date());
        bean.setStatus(Constants.STATUS_ACTIVE);
        bean.setCreatedBy(user);

        entityManager.persist(bean);

        createLog(user, Constants.ACTION_CREATE, "create email_notification " + bean.getId());

        return bean;

    }

    public EmailNotification update(EmailNotification bean, String user) {
        EmailNotification entity = findOne(bean.getId());
        if (entity != null) {
            entity.setEmail(bean.getEmail());
            entity.setModifiedBy(user);
            entity.setModified(new Date());

            createLog(user, Constants.ACTION_UPDATE, "update email_notification " + bean.getId());
        }


        return entity;

    }

    public int delete(Long id, String user) {
        EmailNotification entity = findOne(id);
        if (entity != null) {
            entity.setStatus(Constants.STATUS_DELETE);
            entity.setModifiedBy(user);
            entity.setModified(new Date());

            createLog(user, Constants.ACTION_DELETE, "delete email_notification " + id);

            return 1;
        }


        return 0;

    }

}
