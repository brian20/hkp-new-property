package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.EntryDoc;
import com.nirons.midland.propertycms.entity.UploadFile;
import com.nirons.midland.propertycms.entity.User;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import javax.swing.*;

/**
 * Created by jaquesyang on 15/6/26.
 */
@Service
@Transactional
public class EntryDocDao extends JpaDao<EntryDoc, Long> {

    public int updateUploadFile(Long id, String lang, UploadFile uploadFile) {
        if (id == null || lang == null || (uploadFile != null && uploadFile.getId() == null)) {
            return 0;
        }
        EntryDoc entity = findOne(id);
        if (entity != null) {
            String field;
            switch (lang) {
                case Constants.LANG_TC:
                    field = "tcFile";
                    break;
                case Constants.LANG_SC:
                    field = "scFile";
                    break;
                case Constants.LANG_EN:
                    field = "enFile";
                    break;
                default:
                    return 0;
            }

            String ql = "UPDATE EntryDoc e SET e." + field + " = :file WHERE e.id = :id";
            Query q = entityManager.createQuery(ql);
            q.setParameter("file", uploadFile);
            q.setParameter("id", id);

            return q.executeUpdate();
        }
        return 0;
    }

    public int delete(Long id, String user) {
        String ql = "UPDATE EntryDoc e SET e.status = :del, e.modifiedBy = :username WHERE e.id = :id";
        Query q = entityManager.createQuery(ql);
        q.setParameter("del", Constants.STATUS_DELETE);
        q.setParameter("id", id);
        q.setParameter("username", user);

        int res = q.executeUpdate();

        createLog(user, Constants.ACTION_DELETE, "delete entry_doc " + id);

        return res;
    }

}
