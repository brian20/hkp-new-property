package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.Developer;
import com.nirons.midland.propertycms.exception.NameExistsException;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by jaquesyang on 15/8/12.
 */
@Service
@Transactional
@SuppressWarnings("unchecked")
public class DeveloperDao extends JpaDao<Developer, Long> {

    public List<Developer> findAll() {
        List<Developer> list = entityManager.createQuery("SELECT e FROM Developer e ORDER BY e.ordering, e.id").getResultList();

        return list;
    }

    public void reloadDeveloperCache() {
        Cache.add("developers", findAll());
    }

    public Developer findByName(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        Query query = entityManager.createQuery("SELECT e FROM Developer e WHERE LOWER(e.urlKey) LIKE :name " +
                " OR LOWER(e.nameTc) LIKE :name" +
                " OR LOWER(e.nameEn) LIKE :name");
        query.setParameter("name", name.toLowerCase());


        return (Developer) Utils.getSingleResult(query);
    }

    @Override
    public Developer create(Developer entity) {

        if (findByName(entity.getNameTc()) != null) {
            throw new NameExistsException(entity.getNameTc());
        }
        if (findByName(entity.getNameEn()) != null) {
            throw new NameExistsException(entity.getNameEn());
        }
        /*
        if (findByName(entity.getNameSc()) != null) {
            throw new NameExistsException(entity.getNameSc());
        }
        */
        entity.setUrlKey(Utils.formatUrlKey(entity.getNameEn()));
        if (findByName(entity.getUrlKey()) != null) {
            throw new NameExistsException(entity.getUrlKey());
        }

        entityManager.persist(entity);

        return entity;
    }

    @Override
    public Developer update(Developer entity) {
        Developer sameName = findByName(entity.getNameTc());
        if (sameName != null && !sameName.getId().equals(entity.getId())) {
            throw new NameExistsException(entity.getNameTc());
        }

        sameName = findByName(entity.getNameEn());
        if (sameName != null && !sameName.getId().equals(entity.getId())) {
            throw new NameExistsException(entity.getNameEn());
        }

        /*
        sameName = findByName(entity.getNameSc());
        if (sameName != null && !sameName.getId().equals(entity.getId())) {
            throw new NameExistsException(entity.getNameSc());
        }
        */

        entity.setUrlKey(Utils.formatUrlKey(entity.getNameEn()));
        sameName = findByName(entity.getUrlKey());
        if (sameName != null && !sameName.getId().equals(entity.getId())) {
            throw new NameExistsException(entity.getUrlKey());
        }

        entity.setModified(new Date());

        return super.update(entity);
    }
}
