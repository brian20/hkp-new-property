package com.nirons.midland.propertycms.dao;

import com.nirons.midland.propertycms.entity.Category;
import com.nirons.midland.propertycms.entity.MoniterItem;
import com.nirons.midland.propertycms.repository.CategoryRepository;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/13.
 */
@Service
@Transactional
public class MoniterItemDao extends JpaDao<MoniterItem, Long> {
    @Autowired
    CategoryRepository categoryRepository;

    public void saveOrUpdateMoniterItem(Long propertyId, String[] trackingUrls) {
        if (trackingUrls == null || trackingUrls.length == 0) {
            return;
        }
        List<Category> categories = categoryRepository.findAll();
        for (int i = 0; i < categories.size(); i++) {
            String url = null;
            if (trackingUrls.length <= i) {
                break;
            }
            url = trackingUrls[i];
            Long categoryId = categories.get(i).getId();
            MoniterItem item = Utils.findByUniqueField(entityManager, MoniterItem.class, "propertyId", propertyId, "categoryId", categoryId);

            if (item != null) {
                item.setTrackingUrl(url);
            } else {
                item = new MoniterItem();
                item.setPropertyId(propertyId);
                item.setCategoryId(categoryId);
                item.setTrackingUrl(url);

                create(item);
            }
        }

    }
}
