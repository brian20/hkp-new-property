package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.dao.EntryDao;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.soap.SAAJResult;
import java.util.Date;

/**
 * Created by jaquesyang on 15/6/24.
 */
@RestController
@RequestMapping("/rest/entry/")
public class EntryRestController extends BaseController {
    @Autowired
    EntryRepository entryRepository;

    @Autowired
    EntryDao entryDao;

    @RequestMapping("")
    public RestResp list(@PageableDefault Pageable pageable,
                         @RequestParam(value = "status",required = false) String status,
                         @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        //Page<Entry> page = entryRepository.findByName(nameParam(name), pageable);
        Page<Entry> page = entryDao.listing(nameParam(Chinese.toTc(name)),status, pageable);

        if (page != null && page.hasContent()) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/{id}")
    public RestResp get(@PathVariable("id") Long id) {
        Entry record = entryRepository.findOne(id);

        if (record != null) {
            return new RestResp(Constants.REST_SUCCESS, "", Constants.REST_BEAN, record);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/uid/{uid}")
    public RestResp getByUid(@PathVariable("uid") String uid) {
        Entry record = entryRepository.findByUidAndStatus(uid, Constants.STATUS_APPROVED);

        if (record != null) {
            return new RestResp(Constants.REST_SUCCESS, "", Constants.REST_BEAN, record);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/search/findByNameAndStatus")
    public RestResp approvalList(@PageableDefault Pageable pageable,
                         @RequestParam(required = false, defaultValue = "") String name,
                         @RequestParam(required = false, defaultValue = "") String status) {

        Page<Entry> page = entryRepository.findByNameAndStatus(nameParam(Chinese.toTc(name)), status, pageable);

        if (page != null) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/updated_today")
    public RestResp updatedToday(@PageableDefault Pageable pageable,
                                 @RequestParam(required = false, defaultValue = "") String name) {
        Date today = new Date();
        Date next = Utils.dateAdd(today, 0, 0, 1);
        Page<Entry> page = entryRepository.findByNameAndModified(nameParam(name), today, next, pageable);

        if (page != null) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/pending")
    public RestResp pending(@PageableDefault Pageable pageable,
                            @RequestParam(required = false, defaultValue = "") String name) {
        Page<Entry> page = entryRepository.findByNameAndStatus(nameParam(name), Constants.STATUS_PENDING, pageable);

        if (page != null) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }
}
