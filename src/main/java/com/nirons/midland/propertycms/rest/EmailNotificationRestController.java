package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.EmailNotification;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.EmailNotificationRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jaquesyang on 15/7/3.
 */
@RestController
@RequestMapping("/rest/email_notification")
public class EmailNotificationRestController extends BaseController {
    @Autowired
    EmailNotificationRepository repository;

    @RequestMapping(value = {"", "/"})
    public RestResp list(@PageableDefault Pageable pageable, @RequestParam(value = "name", required = false, defaultValue = "") String name) {
        Page<EmailNotification> page = repository.findByEmail(nameParam(name), pageable);
        if (page != null) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }

    @RequestMapping(value = "/{id}")
    public RestResp get(@PathVariable Long id) {
        EmailNotification page = repository.findOne(id);
        if (page != null) {
            return new RestResp(Constants.REST_SUCCESS, "", page);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }
}
