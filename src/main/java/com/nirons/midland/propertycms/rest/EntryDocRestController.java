package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.entity.EntryDoc;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.model.SinglePage;
import com.nirons.midland.propertycms.repository.EntryDocRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jaquesyang on 15/6/24.
 */
@RestController
@RequestMapping("/rest/entry_doc")
public class EntryDocRestController extends BaseController {

    @Autowired
    EntryDocRepository checkRecordRepository;

    @RequestMapping("/{id}")
    public RestResp get(@PathVariable("id") Long id) {
        EntryDoc doc = checkRecordRepository.findOne(id);

        logger.debug("doc: {}", doc);

        if (doc != null) {
            return new RestResp(Constants.REST_SUCCESS, "", doc);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/entry_{entryId}/{type}")
    public RestResp listByEntryAndType(
            @PathVariable Long entryId,
            @PathVariable String type, @SortDefault(sort = "nameTc", direction = Sort.Direction.ASC) Sort sort) {

        if (type.equalsIgnoreCase(Constants.DOC_SALES_ARRANGEMENT)) {
            sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(postDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "postDate"),
                    new Sort.Order(Sort.Direction.ASC, "NVL2(updateDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "updateDate"),
                    new Sort.Order(Sort.Direction.DESC, "created"));
        } else if (type.equalsIgnoreCase(Constants.DOC_PRICE_LIST)) {
            sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(printDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "printDate"),
                    new Sort.Order(Sort.Direction.DESC, "created"));
        }

        List<EntryDoc> list = checkRecordRepository.findByEntryAndType(new Entry(entryId), type, sort);

        if (list != null) {
            return new RestResp(Constants.REST_SUCCESS, "", new SinglePage<>(list, sort));
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }
}
