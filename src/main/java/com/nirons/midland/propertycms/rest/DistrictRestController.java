package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.District;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.DistrictRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/18.
 */
@RestController
public class DistrictRestController extends BaseController {
    @Autowired
    DistrictRepository repository;

    @RequestMapping("/rest/district")
    public RestResp list() {
        List<District> list = repository.findAll(new Sort(Sort.Direction.ASC, "ordering", "id"));

        if (list != null) {
            return new RestResp(Constants.REST_SUCCESS, "", list);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }

    @RequestMapping("/rest/district/{id}")
    public RestResp detail(@PathVariable Long id) {
        District district = repository.findOne(id);

        if (district != null) {
            return new RestResp(Constants.REST_SUCCESS, "", district);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }
}
