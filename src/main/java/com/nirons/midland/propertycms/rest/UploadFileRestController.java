package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.UploadFile;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.UploadFileRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jaquesyang on 15/6/25.
 */
@RestController
@RequestMapping("/rest/upload_file")
public class UploadFileRestController extends BaseController {

    @Autowired
    UploadFileRepository uploadFileRepository;

    @RequestMapping("/{id}")
    public RestResp get(@PathVariable("id") Long id) {
        UploadFile uploadFile = uploadFileRepository.findOne(id);
        if (uploadFile != null) {
            return new RestResp(Constants.REST_SUCCESS, "", Constants.REST_BEAN, uploadFile);
        } else {
            return new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND);
        }
    }
}
