package com.nirons.midland.propertycms.rest;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by jaquesyang on 15/6/11.
 */
@RestController
@RequestMapping("/rest/user")
public class UserRestController extends BaseController {
    @RequestMapping(value = "/{id}")
    public User getUser(@PathVariable("id") Long id) {
        User user = new User();
        user.setUsername("Jaques");
        user.setPassword("Yang");

        return user;
    }

    @RequestMapping("")
    public List<User> users(@RequestParam(value = "first_result", defaultValue = "0") int firstResult, @RequestParam(value = "max_result", defaultValue = "0") int maxResult) {

        List<User> users = new ArrayList<>();
        User user = new User();
        user.setUsername("Jaques");
        user.setPassword("Yang");
        users.add(user);
        return users;
    }

    @RequestMapping(value = "/get", method = {RequestMethod.GET, RequestMethod.POST})
    public
    @ResponseBody
    String get(HttpServletRequest req) {

        Enumeration<String> params = req.getParameterNames();
        while (params.hasMoreElements()) {
            String param = params.nextElement();
            logger.debug("{}: {}", param, req.getParameter(param));
        }

        return "{}";
    }
}
