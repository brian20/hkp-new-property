package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.*;
import com.nirons.midland.propertycms.entity.Developer;
import com.nirons.midland.propertycms.entity.District;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.entity.EntryDoc;
import com.nirons.midland.propertycms.repository.DistrictRepository;
import com.nirons.midland.propertycms.repository.EntryDocRepository;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jaquesyang on 15/7/9.
 */
@Controller
@SuppressWarnings("unchecked")
public class FrontendController extends BaseController {
    @Autowired
    EntryRepository repository;

    @Autowired
    EntryDao dao;

    @Autowired
    EntryDocDao entryDocDao;

    @Autowired
    EntryDocRepository entryDocRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    DeveloperDao developerDao;

    @RequestMapping("/search")
    public String search(Model model, @RequestParam(required = false, defaultValue = "") String name) {
        if (isEscapedFragment()) {
            model.addAttribute("h4", StringUtils.isEmpty(name) ? getMessage("all") : name);
            String paramName = nameParam(Chinese.toTc(name));
            return escapedFragment(model, repository.findApprovedByName(paramName));
        }
        //name = Chinese.toTc(name);
        //String paramName = nameParam(name);
        //model.addAttribute("entries", repository.findApprovedByName(paramName));
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());
        model.addAttribute("searchBy", "name");
        model.addAttribute("searchKey", name);
        model.addAttribute("h4", StringUtils.isEmpty(name) ? getMessage("all") : name);
        return "frontend/index";
    }

    @RequestMapping("/district/{district}")
    public String listByDistrict(Model model, @PathVariable String district) {
        //model.addAttribute("entries", repository.findApprovedByDistrict(district));
        District dist = districtRepository.findByUrlKey(district);
        if (dist == null) {
            return "redirect:/";
        }

        if (isEscapedFragment()) {
            model.addAttribute("h4", dist.getName(getLocale()));
            return escapedFragment(model, repository.findApprovedByDistrict(district));
        }

        RequestContext requestContext = new RequestContext(request);
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());
        model.addAttribute("searchBy", "district");
        model.addAttribute("searchKey", district);
        model.addAttribute("district", dist);
        model.addAttribute("h4", dist.getName(requestContext.getLocale()));
        return "frontend/index";
    }

    /*
    @RequestMapping("/")
    public String defaultPage() {
        return "redirect:/new-property/";
    }
    */

    @RequestMapping("/district/{district}/{area}")
    public String listByArea(Model model, @PathVariable String district, @PathVariable String area) {
        //model.addAttribute("entries", repository.findApprovedByDistrict(district));
        District dist = districtRepository.findByUrlKey(district);
        if (dist == null) {
            return "redirect:/";
        }

        String paramArea = Chinese.toTc(area);

        if (isEscapedFragment()) {
            model.addAttribute("h4", dist.getName(getLocale()) + " | " + paramArea);
            return escapedFragment(model, repository.findApprovedByDistrictAndAreaAndName(district, paramArea, null));
        }

        RequestContext requestContext = new RequestContext(request);
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());
        model.addAttribute("searchBy", "area");
        model.addAttribute("searchKey", paramArea);
        model.addAttribute("district", dist);
        model.addAttribute("h4", dist.getName(requestContext.getLocale()) + " | " + paramArea);
        return "frontend/index";
    }

    @RequestMapping("/developer/{developer}")
    public String listByDeveloper(Model model, @PathVariable String developer) {
        //model.addAttribute("entries", repository.findApprovedByDeveloper(developer));
        Developer dev = developerDao.findByName(developer);
        if (dev == null) {
            return "redirect:/";
        }
        if (isEscapedFragment()) {
            model.addAttribute("h4", dev.getName(getLocale()));
            return escapedFragment(model, repository.findApprovedByDeveloper(dev.getId()));
        }

        RequestContext requestContext = new RequestContext(request);
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());
        model.addAttribute("searchBy", "developer");
        model.addAttribute("searchKey", developer);
        model.addAttribute("developer", dev);
        model.addAttribute("h4", dev.getName(requestContext.getLocale()));
        return "frontend/index";
    }

    @RequestMapping("/")
    public String hot(Model model) {
        if (isEscapedFragment()) {
            model.addAttribute("h4", getMessage("hot_items"));
            return escapedFragment(model, repository.findApprovedHot());
        }
        //model.addAttribute("entries", repository.findApprovedHot());
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());
        model.addAttribute("searchBy", "hot");
        model.addAttribute("h4", getMessage("hot_items"));
        return "frontend/index";
    }

    @RequestMapping("/{uid}")
    public String property(@PathVariable String uid, Model model) {
        List<Entry> entries = repository.findApprovedByUrlKey(uid);
        Entry entry = null;
        if (!entries.isEmpty()) {
            entry = entries.get(0);
        }
        if (entry == null || !Constants.STATUS_APPROVED.equalsIgnoreCase(entry.getStatus()) || entry.isHiddenForHkp()) {
            return "redirect:/";
        }
        model.addAttribute("entry", entry);
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());

        int lang = Utils.localeLang(getLocale());

        String[] types = {Constants.DOC_VIEW_RECORD,
                Constants.DOC_SALES_MANUAL_PART,
                Constants.DOC_SALES_MANUAL_FULL,
                Constants.DOC_PRICE_LIST,
                Constants.DOC_SALES_ARRANGEMENT,
                Constants.DOC_COMPLETED_RECORD
        };
        for (String type : types) {

            Sort.Order nameOrder = new Sort.Order(Sort.Direction.ASC, "nameTc");
            if (lang == 1) {
                nameOrder = new Sort.Order(Sort.Direction.ASC, "nameEn");
            }

            Sort sort = new Sort(nameOrder, new Sort.Order(Sort.Direction.DESC, "created"));
            if (type.equalsIgnoreCase(Constants.DOC_SALES_ARRANGEMENT)) {
                sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(postDate, 0, 1)"),
                        new Sort.Order(Sort.Direction.DESC, "postDate"),
                        new Sort.Order(Sort.Direction.ASC, "NVL2(updateDate, 0, 1)"),
                        new Sort.Order(Sort.Direction.DESC, "updateDate"),
                        new Sort.Order(Sort.Direction.DESC, "created"));
            } else if (type.equalsIgnoreCase(Constants.DOC_PRICE_LIST)) {
                sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(printDate, 0, 1)"),
                        new Sort.Order(Sort.Direction.DESC, "printDate"),
                        new Sort.Order(Sort.Direction.DESC, "created"));
            }
            List<EntryDoc> docs = entryDocRepository.findByEntryAndType(entry, type, sort);
            model.addAttribute(type + "s", docs);
        }

        return "frontend/detail";
    }

    public List<District> districts() {
        //return districtRepository.findAll(new Sort(Sort.Direction.ASC, "ordering"));
        return (List<District>) Cache.get("districts");
    }

    public List<Developer> developers() {
        //return repository.developers();
        return (List<Developer>) Cache.get("developers");
    }

    public String contentFromUrl(String url) {
        logger.debug("url: " + url);
        String content = (String) Cache.get(url);
        if (content != null) {
            logger.debug("content from cache");
            return content;
        }
        BufferedReader rd = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);

            // add request header
            //request.addHeader("User-Agent", USER_AGENT);
            HttpResponse response = client.execute(request);

            int statusCode = response.getStatusLine().getStatusCode();

            logger.debug("Response Code : " + statusCode);
            if (statusCode >= 400) {
                return "";
            }


            rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
                result.append("\n");
            }

            content = result.toString();

            Cache.add(url, content, Constants.URL_CACHE_TIMEOUT);

            return content;
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException e1) {
                }
            }
        }

        return "";
    }

    public String headerContent(Entry entry) {
        /*
        StringBuilder sb = new StringBuilder(Constants.STATIC_PROTOCOL);
        sb.append("://");
        sb.append(request.getServerName());
        sb.append(request.getContextPath());
        sb.append("/common/tpl-top.html?request-uri=");
        sb.append(request.getRequestURI() == null ? "" : paramEncode(request.getRequestURI()));
        sb.append("&query-string=");
        sb.append(request.getQueryString() == null ? "" : paramEncode(request.getQueryString()));

        String html = contentFromUrl(sb.toString());
        */
        String headerURL = Constants.STATIC_HEADER_URL_TC;
        int lang = Utils.localeLang(LocaleContextHolder.getLocale());
        switch (lang) {
            case 1:
                headerURL = Constants.STATIC_HEADER_URL_EN;
                break;
            case 2:
                headerURL = Constants.STATIC_HEADER_URL_SC;
                break;
        }
        headerURL = headerURL.replace("[request-uri]", request.getRequestURI() == null ? "" : paramEncode(request.getRequestURI()));
        headerURL = headerURL.replace("[query-string]", request.getQueryString() == null ? "" : paramEncode(request.getQueryString()));


        String html = contentFromUrl(headerURL);

        if (Constants.STATIC_TPL_TOP_MAIN_TAG != null && !Constants.STATIC_TPL_TOP_MAIN_TAG.trim().isEmpty() && !StringUtils.isEmpty(html)) {
            html = html.replace(Constants.STATIC_TPL_TOP_MAIN_TAG, "");
        }

        if (entry != null
                && Constants.STATIC_TPL_TOP_TITLE_TAG != null
                && !Constants.STATIC_TPL_TOP_TITLE_TAG.trim().isEmpty()
                && !StringUtils.isEmpty(html)) {
            String name = entry.getName(getLocale());
            if (!StringUtils.isEmpty(name)) {
                String ot = getMessage("frontend_title");

                Pattern pa = Pattern.compile(Constants.STATIC_TPL_TOP_TITLE_TAG);
                Matcher ma = pa.matcher(html);

                if (ma.find()) {
                    ot = Utils.removeHtmlTag(ma.group());
                }
                
                StringBuilder title = new StringBuilder("<title>");
                title.append(name);
                title.append(" | ");
                title.append(ot);
                title.append("</title>");
                html = html.replaceAll(Constants.STATIC_TPL_TOP_TITLE_TAG, title.toString());
            }
        }

        return html;

    }


    public String footerContent() {
        /*
        StringBuilder sb = new StringBuilder(Constants.STATIC_PROTOCOL);
        sb.append("://");
        sb.append(request.getServerName());
        sb.append(request.getContextPath());
        sb.append("/common/tpl-bottom.html?request-uri=");
        sb.append(request.getRequestURI() == null ? "" : paramEncode(request.getRequestURI()));
        sb.append("&query-string=");
        sb.append(request.getQueryString() == null ? "" : paramEncode(request.getQueryString()));
        */
        String footerURL = Constants.STATIC_FOOTER_URL_TC;
        int lang = Utils.localeLang(LocaleContextHolder.getLocale());
        switch (lang) {
            case 1:
                footerURL = Constants.STATIC_FOOTER_URL_EN;
                break;
            case 2:
                footerURL = Constants.STATIC_FOOTER_URL_SC;
                break;
        }
        footerURL = footerURL.replace("[request-uri]", request.getRequestURI() == null ? "" : paramEncode(request.getRequestURI()));
        footerURL = footerURL.replace("[query-string]", request.getQueryString() == null ? "" : paramEncode(request.getQueryString()));


        return contentFromUrl(footerURL);

    }

    public String paramEncode(String string) {
        if (false) {
            return Utils.urlEncode(string);
        }
        return string;
    }

    public String localeText(String tc, String en, String sc, HttpServletRequest request) {
        RequestContext requestContext = new RequestContext(request);
        String out = tc;
        if ("en".equalsIgnoreCase(requestContext.getLocale().getLanguage())) {
            out = en;
        } else if ("CN".equals(requestContext.getLocale().getCountry())) {
            out = Chinese.toSc(tc);
        }
        if (StringUtils.isEmpty(out)) {
            out = tc;
        }
        return out;
    }

    @RequestMapping(value = "/midland-api/departments", produces = "text/xml;charset=UTF-8")
    public
    @ResponseBody
    String departments() {
        String queryString = request.getQueryString();
        StringBuilder url = new StringBuilder("http://app.midland.com.hk/residential_ebook/ebook_service/department.jsp");
        if (!StringUtils.isEmpty(queryString)) {
            url.append("?");
            url.append(queryString);
        }

        String xml = contentFromUrl(url.toString());

        return xml;
    }

    @RequestMapping(value = "/midland-api/declare", produces = "json/application;charset=UTF-8")
    public
    @ResponseBody
    String declare() {
        String queryString = request.getQueryString();
        int lang = Utils.localeLang(LocaleContextHolder.getLocale());
        String serverName = "www.midland.com.hk";
        switch (lang) {
            case 1:
                serverName = "en.midland.com.hk";
                break;
            case 2:
                serverName = "sc.midland.com.hk";
                break;
        }
        StringBuilder url = new StringBuilder("http://" + serverName + "/new-property-declare/data/report");
        if (!StringUtils.isEmpty(queryString)) {
            url.append("?");
            url.append(queryString);
        }

        String xml = contentFromUrl(url.toString());

        return xml;
    }

    @RequestMapping("/escaped-fragment")
    public String escapedFragment(Model model, List<Entry> entries) {


        model.addAttribute("entries", entries);
        model.addAttribute("districts", districts());
        model.addAttribute("developers", developers());

        return "frontend/escaped_fragment";
    }



    @Autowired
    SettingDao settingDao;

    @Autowired
    DistrictDao districtDao;

    @RequestMapping("/frontend_reload_settings")
    public
    @ResponseBody
    String reloadCache(@RequestParam String key) {
        if ("2147483647".equals(key)) {

            Set<String> keys = new HashSet<>(Cache.keys());
            for (String aKey : keys) {
                if (aKey.startsWith("http")) {
                    Cache.remove(aKey);
                }
            }

            settingDao.reloadStaticSettings();

            districtDao.reloadDistrictCache();

            developerDao.reloadDeveloperCache();


            return "done";
        }
        return key;
    }
}
