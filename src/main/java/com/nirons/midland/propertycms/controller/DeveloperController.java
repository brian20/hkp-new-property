package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.DeveloperDao;
import com.nirons.midland.propertycms.entity.Developer;
import com.nirons.midland.propertycms.exception.NameExistsException;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.DeveloperRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


/**
 * Created by jaquesyang on 15/8/18.
 */
//@Controller
//@RequestMapping("/cms/developer")
public class DeveloperController extends BaseController {
    @Autowired
    DeveloperRepository repository;

    @Autowired
    DeveloperDao dao;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    RestResp add(@ModelAttribute Developer developer) {
        try {
            Developer entity = dao.create(developer);
            if (entity != null) {
                dao.reloadDeveloperCache();
                return new RestResp(Constants.REST_SUCCESS, "", entity);
            }
        } catch (NameExistsException e) {
            e.printStackTrace();
            if (e != null && !StringUtils.isEmpty(e.getMessage())) {
                String message = getMessage("developer_name_exists", e.getMessage());
                return new RestResp(Constants.REST_NAME_EXISTS, message);
            }
        }

        return new RestResp(Constants.REST_ERR_UNKNOW, "");
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public
    @ResponseBody
    RestResp add(@ModelAttribute Developer developer, @PathVariable Long id) {
        try {
            Developer entity = dao.findOne(id);
            if (entity != null) {
                entity.updateFields(developer);
                dao.update(entity);
                dao.reloadDeveloperCache();
                return new RestResp(Constants.REST_SUCCESS, "", entity);
            }
        } catch (NameExistsException e) {
            e.printStackTrace();
            if (e != null && !StringUtils.isEmpty(e.getMessage())) {
                String message = getMessage("developer_name_exists", e.getMessage());
                return new RestResp(Constants.REST_NAME_EXISTS, message);
            }
        }

        return new RestResp(Constants.REST_ERR_UNKNOW, "");
    }
}
