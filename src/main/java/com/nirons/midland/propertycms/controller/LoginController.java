package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.DeveloperDao;
import com.nirons.midland.propertycms.dao.DistrictDao;
import com.nirons.midland.propertycms.dao.SettingDao;
import com.nirons.midland.propertycms.entity.Developer;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.entity.User;
import com.nirons.midland.propertycms.repository.DeveloperRepository;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.repository.UserRepository;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jaquesyang on 15/6/10.
 */
//@Controller("loginController")
//@RequestMapping("/cms")
public class LoginController extends BaseController {


    @Autowired
    UserRepository userRepository;

    @Autowired
    EntryRepository entryRepository;

    @Autowired
    DeveloperRepository developerRepository;

    @RequestMapping("")
    public String index(@RequestParam(value = "logout", required = false) String logout) {
        Locale locale = LocaleContextHolder.getLocale();
        if ("CN".equalsIgnoreCase(locale.getCountry())) {

            return "redirect:/";
        }
        if (logout != null) {
            request.getSession().removeAttribute("session_user");
        }
        return "login";
    }

    @RequestMapping("/dashboard")
    public String dashboard(Model model, HttpServletRequest req) {
        model.addAttribute("entryUpdate", entryRepository.countByUpdateDate(new Date()));
        model.addAttribute("entryApproval", entryRepository.countByStatus(Constants.STATUS_PENDING));

        Page<Entry> entries = entryRepository.findByLastUpdate(new PageRequest(0, 15));
        model.addAttribute("entries", entries);

        if (!StringUtils.isEmpty(req.getRemoteUser())) {
            User user = userRepository.findByUsername(req.getRemoteUser());
            if (user != null) {
                req.getSession().setAttribute(SESSION_USER, user);
            }
        }


        return "dashboard";
    }

    public String entryStatus(String status) {
        if (status != null) {
            return messageSource.getMessage("Entry.status." + status, null, Locale.getDefault());

        }
        return null;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model, HttpServletRequest req) {
        User user = null;
        if (!StringUtils.isEmpty(req.getRemoteUser())) {
            user = userRepository.findByUsername(req.getRemoteUser());
        }
        if (user == null) {
            return "redirect:/cms";
        }
        model.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public ModelAndView updateProfile(@Valid User user,
                                      @RequestParam(required = false) String oriPwd,
                                      @RequestParam(required = false) String newPwd,
                                      @RequestParam(required = false) String cfmPwd,
                                      BindingResult bindingResult,
                                      final RedirectAttributes redirectAttributes) {

        logger.debug("update user {}", user);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/cms/profile");

        String username = request.getRemoteUser();
        User entity = null;
        if (!StringUtils.isEmpty(username)) {
            entity = userRepository.findByUsername(username);
        }

        if (entity == null) {
            modelAndView.setViewName("redirect:/cms");

            return modelAndView;
        }

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("profile");
        } else {
            if (!StringUtils.isEmpty(oriPwd)) {
                if (!entity.getPassword().equalsIgnoreCase(Utils.md5(oriPwd))) {
                    redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("password_not_correct"));

                    return modelAndView;
                }

                if (newPwd == null || newPwd.trim().length() < 6) {

                    redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("invalid_password"));

                    return modelAndView;
                }
                newPwd = newPwd.trim();
                if (!newPwd.equalsIgnoreCase(cfmPwd)) {

                    redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("password_not_match"));

                    return modelAndView;

                }

                entity.setPassword(Utils.md5(newPwd));
            }

            entity.setName(user.getName());
            entity.setEmail(user.getEmail());
            entity.setModified(new Date());


            try {
                userRepository.save(entity);
                redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("update_profile_success"));
                request.getSession().setAttribute(SESSION_USER, user);
            } catch (Exception e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("update_profile_failure"));
            }

        }


        return modelAndView;
    }

    public String getAssetsServer() {
        return Constants.STATIC_ASSETS_SERVER;
    }

    public String getAppServer() {
        return Constants.STATIC_APP_SERVER;
    }

    @Autowired
    SettingDao settingDao;

    @Autowired
    DistrictDao districtDao;

    @Autowired
    DeveloperDao developerDao;

    @RequestMapping("/reload_settings")
    public
    @ResponseBody
    String reloadCache(@RequestParam String key) {
        if ("2147483647".equals(key)) {

            Set<String> keys = new HashSet<>(Cache.keys());
            for (String aKey : keys) {
                if (aKey.startsWith("http")) {
                    Cache.remove(aKey);
                }
            }

            settingDao.reloadStaticSettings();

            districtDao.reloadDistrictCache();

            developerDao.reloadDeveloperCache();


            return "done";
        }
        return key;
    }

}
