package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.EmailNotificationDao;
import com.nirons.midland.propertycms.entity.EmailNotification;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.EmailNotificationRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jaquesyang on 15/7/3.
 */

//@Controller
//@RequestMapping("/cms/email_notification")
public class EmailNotificationController extends BaseController {
    @Autowired
    EmailNotificationRepository repository;

    @Autowired
    EmailNotificationDao dao;

    @RequestMapping("")
    public String list() {

        return "email_notification/list";
    }

    @RequestMapping(value = "/add", method = {RequestMethod.POST, RequestMethod.PUT})
    public
    @ResponseBody
    RestResp add(@RequestParam("email") String email, HttpServletRequest req) {
        EmailNotification emailNotification = repository.findByEmail(email);
        if (emailNotification != null) {
            return new RestResp(Constants.REST_NAME_EXISTS, getMessage("email_exists", email));
        }
        EmailNotification bean = new EmailNotification();
        bean.setEmail(email);
        try {
            bean = dao.create(bean, getSessionUsername(req));

            if (bean.getId() != null) {
                return new RestResp(Constants.REST_SUCCESS, getMessage("add_success"), bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return SYSTEM_ERROR_RESP;
    }

    @RequestMapping(value = "/edit", method = {RequestMethod.POST, RequestMethod.PUT})
    public
    @ResponseBody
    RestResp edit(@RequestParam("email") String email, @RequestParam("id") Long id, HttpServletRequest req) {
        EmailNotification emailNotification = repository.findByEmail(email);
        if (emailNotification != null && !emailNotification.getId().equals(id)) {

            return new RestResp(Constants.REST_NAME_EXISTS, getMessage("email_exists", email));
        }
        EmailNotification bean = new EmailNotification();
        bean.setId(id);
        bean.setEmail(email);
        try {
            bean = dao.update(bean, getSessionUsername(req));

            if (bean.getId() != null) {
                return new RestResp(Constants.REST_SUCCESS, getMessage("edit_success"), bean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return SYSTEM_ERROR_RESP;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.POST, RequestMethod.DELETE})
    public
    @ResponseBody
    RestResp delete(@RequestParam("id") Long id, HttpServletRequest req) {
        try {
            int i = dao.delete(id, getSessionUsername(req));

            if (i > 0) {
                return new RestResp(Constants.REST_SUCCESS, getMessage("delete_success"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return SYSTEM_ERROR_RESP;
    }
}
