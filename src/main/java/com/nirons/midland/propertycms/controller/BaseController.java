package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.entity.User;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by jaquesyang on 15/6/17.
 */
@Slf4j
public class BaseController {

    public static final String SESSION_USER = "session_user";

    public static final String INFO_MESSAGE = "info_message";

    public static final String SUCCESS_MESSAGE = "success_message";

    public static final String FAILURE_MESSAGE = "failure_message";

    public static final String WARNING_MESSAGE = "waring_message";


    public static final String INFO_MESSAGE2 = "info_message2";

    public static final String SUCCESS_MESSAGE2 = "success_message2";

    public static final String FAILURE_MESSAGE2 = "failure_message2";

    public static final String WARNING_MESSAGE2 = "waring_message2";


    public static final String INFO_MESSAGE3 = "info_message3";

    public static final String SUCCESS_MESSAGE3 = "success_message3";

    public static final String FAILURE_MESSAGE3 = "failure_message3";

    public static final String WARNING_MESSAGE3 = "waring_message3";


    public static final String INFO_MESSAGE4 = "info_message4";

    public static final String SUCCESS_MESSAGE4 = "success_message4";

    public static final String FAILURE_MESSAGE4 = "failure_message4";

    public static final String WARNING_MESSAGE4 = "waring_message4";


    public static final String INFO_MESSAGE5 = "info_message5";

    public static final String SUCCESS_MESSAGE5 = "success_message5";

    public static final String FAILURE_MESSAGE5 = "failure_message5";

    public static final String WARNING_MESSAGE5 = "waring_message5";

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    MessageSource messageSource;


    @Autowired
    HttpServletRequest request;

    public String getMessage(String key, Object... objects) {
        try {
            return messageSource.getMessage(key, objects, LocaleContextHolder.getLocale());
        } catch (Exception e) {
            return MessageFormat.format(key, objects);
        }
    }


    public final RestResp RECORD_NOT_FOUND_RESP = new RestResp(Constants.REST_ERR_RECORD_NOT_FOUND, getMessage("record_not_found"));

    public final RestResp NO_RECORD_FOUND_RESP = new RestResp(Constants.REST_ERR_NO_RECORD_FOUND, getMessage("no_record_found"));

    public final RestResp SYSTEM_ERROR_RESP = new RestResp(Constants.REST_ERR_UNKNOW, getMessage("system_error"));

    public String getSessionUsername(HttpSession session) {
        User user = (User) session.getAttribute(SESSION_USER);
        if (user != null) {
            return user.getUsername();
        }
        return null;
    }

    public String getSessionUsername(HttpServletRequest req) {
        String name = getSessionUsername(req.getSession());
        if (name == null) {
            name = req.getRemoteUser();
        }
        return name;
    }

    public User getSessionUser(HttpSession session) {
        return (User) session.getAttribute(SESSION_USER);
    }

    public void setSessionUser(HttpSession session, User user) {
        session.setAttribute(SESSION_USER, user);
    }

    public User getSessionUser(HttpServletRequest req) {
        return getSessionUser(req.getSession());
    }

    public void setSessionUser(HttpServletRequest req, User user) {
        setSessionUser(req.getSession(), user);
    }

    public String nameParam(String name) {
        if (name == null || name.isEmpty() || name.trim().isEmpty()) {
            return null;
        }

        return "%" + name.trim() + "%";
    }


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat(Constants.yyyy_MM_dd), true);
        binder.registerCustomEditor(Date.class, editor);
    }

    public Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    public int getLocaleLang() {
        return Utils.localeLang(getLocale());
    }

    public String getSrpeUrl() {
        int lang = getLocaleLang();
        switch (lang) {
            case 1:
                return Constants.STATIC_SRPE_URL_EN;
            case 2:
                return Constants.STATIC_SRPE_URL_SC;
        }

        return Constants.STATIC_SRPE_URL_TC;
    }

    public boolean isEscapedFragment() {
        return request.getParameter("_escaped_fragment_") != null;
    }
}

