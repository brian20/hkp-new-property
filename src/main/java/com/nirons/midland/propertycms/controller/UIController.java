package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Locale;

/**
 * Created by jaquesyang on 15/8/15.
 */
@Controller("ui")
public class UIController extends BaseController {

    @Autowired
    private EntryRepository entryRepository;

    public String localeText(String tc, String en, String sc, String defaultText) {
        Locale locale = LocaleContextHolder.getLocale();
        String out = tc;
        if ("en".equalsIgnoreCase(locale.getLanguage())) {
            out = en;
        } else if ("CN".equals(locale.getCountry())) {
            if (StringUtils.isEmpty(sc)) {
                out = tc;
            } else {
                out = sc;
            }
        }
        if (defaultText != null && StringUtils.isEmpty(out)) {
            out = defaultText;
        }

        return out;
    }

    public String localeText(String tc, String en, String sc) {
        return localeText(tc, en, sc, null);
    }

    @RequestMapping("/cms/approved/{uid}")
    public String approvedProperty(@PathVariable String uid) {
        Entry entry = entryRepository.findByUidAndStatus(uid, Constants.STATUS_APPROVED);
        if (entry != null) {
            return "redirect:/cms/entry/edit/" + entry.getId();
        } else {
            return "redirect:/cms/entry";
        }
    }


    public String getAssetsServer() {
        return Constants.STATIC_ASSETS_SERVER;
    }

    public String getAppServer() {
        return Constants.STATIC_APP_SERVER;
    }

    public String getEnAppServer() {
        return Constants.STATIC_EN_APP_SERVER;
    }

    public String getScAppServer() {
        return Constants.STATIC_SC_APP_SERVER;
    }


    public boolean isDevMode() {
        return Constants.STATIC_DEV_MODE;
    }


}
