package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.*;
import com.nirons.midland.propertycms.entity.*;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.*;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.soap.SAAJResult;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jaquesyang on 15/6/15.
 */
//@Controller
//@RequestMapping("/cms/entry")
public class EntryController extends BaseController {

    @Autowired
    DistrictRepository areaRepository;

    @Autowired
    EntryRepository entryRepository;

    @Autowired
    EntryDocRepository entryDocRepository;

    @Autowired
    UploadFileRepository uploadFileRepository;

    @Autowired
    SettingDao settingDao;

    @Autowired
    EntryDocDao entryDocDao;

    @Autowired
    UploadFileDao uploadFileDao;

    @Autowired
    EntryDao entryDao;

    @Autowired
    DistrictDao districtDao;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    MoniterItemRepository moniterItemRepository;

    @Autowired
    MoniterItemDao moniterItemDao;

    @Autowired
    DeveloperRepository developerRepository;

    @Autowired
    DeveloperDao developerDao;

    @Autowired
    EntryDocUrlRepository entryDocUrlRepository;

    @RequestMapping(value = {"", "/list"})
    public String list(Model model, @PageableDefault() Pageable p) {
        //model.addAttribute("page", entryRepository.findAll(p));


        return "entry/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String initAdd(Model model) {

        model.addAttribute("entry", new Entry());
        model.addAttribute("districts", getAreas());
        List<Category> categories = getCategories();
        model.addAttribute("categories", categories);
        model.addAttribute("trackingUrlMap", trackingUrlMap(null));

        model.addAttribute("mode", "add");


        return "entry/form";
    }


    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public ModelAndView view(@PathVariable("id") Long id) {

        ModelAndView modelAndView = new ModelAndView("entry/form");
        Entry entry = entryRepository.findOne(id);
        if (entry == null) {
            modelAndView.setViewName("redirect:/cms/entry/");
        } else {

            modelAndView.addObject("entry", entry);

            //setEntryDocs(entry, modelAndView);
        }

        return modelAndView;
    }


    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView initEdit(@PathVariable("id") Long id) {

        ModelAndView modelAndView = new ModelAndView("entry/form");
        Entry entry = entryRepository.findOne(id);
        if (entry == null) {
            modelAndView.setViewName("redirect:/cms/entry/");
        } else if (Constants.STATUS_OVERRIDED.equalsIgnoreCase(entry.getStatus()) || Constants.STATUS_REJECTED.equalsIgnoreCase(entry.getStatus())) {
            modelAndView.setViewName("redirect:/cms/entry/");
        } else {
            if (!Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
                Entry pending = entryDao.findPending(entry.getUid());
                if (pending != null) {
                    modelAndView.setViewName("redirect:/cms/entry/edit/" + pending.getId());
                    return modelAndView;
                }
            }

            entry.setDocUrls(entryDocUrlRepository.findByPropertyId(entry.getId()));
            entry.setupDocUrls();

            modelAndView.addObject("entry", entry);
            modelAndView.addObject("districts", getAreas());
            modelAndView.addObject("mode", "edit");

            List<Category> categories = getCategories();
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("trackingUrlMap", trackingUrlMap(entry.getId()));

            //setEntryDocs(entry, modelAndView);
        }

        return modelAndView;
    }

    Iterable<District> getAreas() {
        return areaRepository.findAll(new Sort(Sort.Direction.ASC, "ordering", "id"));
    }

    List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    void setEntryDocs(Entry entry, ModelAndView modelAndView) {

        List<EntryDoc> checkRecords = entryDocRepository.findByEntry(entry);
        modelAndView.addObject("entryDocs", checkRecords);

        List<EntryDoc> vrs = new ArrayList<>();
        List<EntryDoc> mps = new ArrayList<>();
        List<EntryDoc> mfs = new ArrayList<>();
        List<EntryDoc> pls = new ArrayList<>();
        List<EntryDoc> sas = new ArrayList<>();
        List<EntryDoc> crs = new ArrayList<>();

        for (EntryDoc record : checkRecords) {
            if (StringUtils.isEmpty(record.getType())) {
                continue;
            }
            switch (record.getType()) {
                case Constants.DOC_VIEW_RECORD:
                    vrs.add(record);
                    break;
                case Constants.DOC_SALES_MANUAL_PART:
                    mps.add(record);
                    break;
                case Constants.DOC_SALES_MANUAL_FULL:
                    mfs.add(record);
                    break;
                case Constants.DOC_PRICE_LIST:
                    pls.add(record);
                    break;
                case Constants.DOC_SALES_ARRANGEMENT:
                    sas.add(record);
                    break;
                case Constants.DOC_COMPLETED_RECORD:
                    crs.add(record);
                    break;
            }
        }

        modelAndView.addObject("vrs", vrs);
        modelAndView.addObject("mps", mps);
        modelAndView.addObject("mfs", mfs);
        modelAndView.addObject("pls", pls);
        modelAndView.addObject("sas", sas);
        modelAndView.addObject("crs", crs);
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView doAdd(@Valid Entry valid,
                              BindingResult bindingResult,
                              @RequestParam(value = "imageFile", required = false) MultipartFile image,
                              @RequestParam(value = "trackingUrl", required = false) String[] trackingUrls) {
        ModelAndView modelAndView = new ModelAndView();

        logger.debug("create entry: {}", valid);


        if (bindingResult.hasErrors()) {

            modelAndView.addObject("districts", getAreas());
            modelAndView.addObject("mode", "add");

            List<Category> categories = getCategories();
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("trackingUrlMap", trackingUrlMap(categories, trackingUrls));

            modelAndView.setViewName("entry/form");

        } else {

            String invalidUid = null;

            if (!valid.getUid().matches("[a-zA-Z0-9]+")) {
                invalidUid = valid.getUid();
            }

            String uid = null;

            if (invalidUid == null) {
                if (entryDao.uniqueIdExists(valid.getUid())) {
                    uid = valid.getUid();
                }
            }

            String name = null;
            if (invalidUid == null && uid == null) {
                if (!entryDao.validName(valid.getUid(), valid.getNameTc())) {
                    name = valid.getNameTc();
                }

                if (!entryDao.validName(valid.getUid(), valid.getNameSc())) {
                    name = valid.getNameSc();
                }

                if (!entryDao.validName(valid.getUid(), valid.getNameEn())) {
                    name = valid.getNameEn();
                }
                if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeyTc())) {
                    name = valid.getUrlKeyTc();
                }

                /*if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeySc())) {
                    name = valid.getUrlKeySc();
                }*/

                if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeyEn())) {
                    name = valid.getUrlKeyEn();
                }
            }

            if (invalidUid != null || name != null || uid != null) {
                modelAndView.addObject("districts", getAreas());
                modelAndView.addObject("mode", "add");
                modelAndView.addObject("entry", valid);

                List<Category> categories = getCategories();
                modelAndView.addObject("categories", categories);
                modelAndView.addObject("trackingUrlMap", trackingUrlMap(categories, trackingUrls));

                modelAndView.setViewName("entry/form");
                if (name != null) {
                    modelAndView.addObject(FAILURE_MESSAGE, getMessage("name_exists", name));
                }
                if (uid != null) {
                    modelAndView.addObject(FAILURE_MESSAGE, getMessage("uid_exists", uid));

                }
                if (invalidUid != null) {
                    modelAndView.addObject(FAILURE_MESSAGE, getMessage("invalid_uid"));
                }

                return modelAndView;
            }

            if (!StringUtils.isEmpty(valid.getAreaTc())) {
                valid.setAreaTc(valid.getAreaTc().trim());
            }

            if (!StringUtils.isEmpty(valid.getAreaEn())) {
                valid.setAreaEn(valid.getAreaEn().trim());
            }


            Entry entity = entryRepository.findByUidAndStatus(valid.getUid(), Constants.STATUS_PENDING);

            if (image != null && image.getSize() > 0) {
                UploadFile file = new UploadFile();
                file.setMultipartFile(image);
                file.setOrdering(0);

                valid.setUploadFile(file);
            }

            if (entity != null) {

                try {
                    Entry pending = entryDao.update(valid, entity.getId(), getSessionUsername(request));
                    if (pending.isTracking()) {
                        moniterItemDao.saveOrUpdateMoniterItem(pending.getId(), trackingUrls);
                    }

                    districtDao.reloadDistrictCache();
                    developerDao.reloadDeveloperCache();

                    modelAndView.setViewName("redirect:/cms/entry/step_2/" + entity.getId());
                } catch (Exception e) {
                    modelAndView.addObject(FAILURE_MESSAGE, e.getLocalizedMessage());
                    e.printStackTrace();
                }
            } else {
                valid.setModified(new Date());
                valid.setCreated(new Date());
                valid.setStatus(Constants.STATUS_PENDING);
                try {
                    Entry entry = entryDao.create(valid, getSessionUsername(request));

                    if (entry.isTracking()) {
                        moniterItemDao.saveOrUpdateMoniterItem(entry.getId(), trackingUrls);
                    }

                    districtDao.reloadDistrictCache();
                    developerDao.reloadDeveloperCache();

                    modelAndView.setViewName("redirect:/cms/entry/step_2/" + entry.getId());
                } catch (Exception e) {
                    modelAndView.addObject(FAILURE_MESSAGE, e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }
        return modelAndView;
    }


    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public ModelAndView doEdit(@Valid Entry valid, BindingResult bindingResult,
                               @RequestParam(value = "imageFile", required = false) MultipartFile image,
                               @PathVariable("id") Long id,
                               @RequestParam(value = "trackingUrl", required = false) String[] trackingUrls,
                               final RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/cms/entry/");

        Entry entry = entryRepository.findOne(id);

        if (entry == null) {
            return modelAndView;
        } else if (Constants.STATUS_OVERRIDED.equalsIgnoreCase(entry.getStatus()) || Constants.STATUS_REJECTED.equalsIgnoreCase(entry.getStatus())) {
            return modelAndView;
        }

        logger.debug("edit entry: {}", valid);

        if (bindingResult.hasErrors()) {


            modelAndView.addObject("districts", getAreas());
            modelAndView.addObject("mode", "edit");

            List<Category> categories = getCategories();
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("trackingUrlMap", trackingUrlMap(categories, trackingUrls));

            modelAndView.setViewName("entry/form");


        } else {
            String name = null;
            if (!entryDao.validName(valid.getUid(), valid.getNameTc())) {
                name = valid.getNameTc();
            }

            if (!entryDao.validName(valid.getUid(), valid.getNameSc())) {
                name = valid.getNameSc();
            }

            if (!entryDao.validName(valid.getUid(), valid.getNameEn())) {
                name = valid.getNameEn();
            }
            if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeyTc())) {
                name = valid.getUrlKeyTc();
            }

            /*if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeySc())) {
                name = valid.getUrlKeySc();
            }*/

            if (!entryDao.validUrlKey(valid.getUid(), valid.getUrlKeyEn())) {
                name = valid.getUrlKeyEn();
            }

            if (name != null) {
                valid.setId(entry.getId());
                modelAndView.addObject("districts", getAreas());
                modelAndView.addObject("mode", "edit");
                modelAndView.addObject("entry", valid);

                List<Category> categories = getCategories();
                modelAndView.addObject("categories", categories);
                modelAndView.addObject("trackingUrlMap", trackingUrlMap(categories, trackingUrls));

                modelAndView.setViewName("entry/form");
                modelAndView.addObject(FAILURE_MESSAGE, getMessage("name_exists", name));
                return modelAndView;
            }

            if (!StringUtils.isEmpty(valid.getAreaTc())) {
                valid.setAreaTc(valid.getAreaTc().trim());
            }

            if (!StringUtils.isEmpty(valid.getAreaEn())) {
                valid.setAreaEn(valid.getAreaEn().trim());
            }


            if (image != null && image.getSize() > 0) {
                UploadFile uploadFile = new UploadFile();
                uploadFile.setMultipartFile(image);
                valid.setUploadFile(uploadFile);
            }


            try {


                if (valid.getDocUrls() == null) {
                    valid.setDocUrls(new ArrayList<>());
                }

                if (valid.getDocUrlSb() != null) {
                    valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_SALES_BROCHURE, valid.getDocUrlSb()));
                }
                if (valid.getDocUrlPl() != null) {
                    valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_PRICE_LIST, valid.getDocUrlPl()));
                }
                if (valid.getDocUrlSa() != null) {
                    valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_SALES_ARRANGEMENT, valid.getDocUrlSa()));
                }
                if (valid.getDocUrlCr() != null) {
                    valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_COMPLETED_RECORD, valid.getDocUrlCr()));
                }

                Entry pending = entryDao.update(valid, id, getSessionUsername(request));
                if (pending != null) {

                    if (pending.isTracking()) {
                        moniterItemDao.saveOrUpdateMoniterItem(pending.getId(), trackingUrls);
                    }

                    districtDao.reloadDistrictCache();
                    developerDao.reloadDeveloperCache();

                    redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("edit_success"));
                } else {
                    redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("edit_failure"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("edit_failure"));
            }

        }
        return modelAndView;
    }

    private Map<Long, String> trackingUrlMap(Long entryId) {
        Map<Long, String> trackingUrlMap = new HashMap<>();
        if (entryId != null) {
            List<MoniterItem> moniterItems = moniterItemRepository.findByPropertyId(entryId);
            for (MoniterItem moniterItem : moniterItems) {
                trackingUrlMap.put(moniterItem.getCategoryId(), moniterItem.getTrackingUrl());
            }
        }

        return trackingUrlMap;
    }

    private Map<Long, String> trackingUrlMap(List<Category> categories, String[] trackingUrls) {
        Map<Long, String> trackingUrlMap = new HashMap<>();
        for (int i = 0; i < categories.size(); i++) {
            String url = "";
            if (trackingUrls != null && trackingUrls.length > i) {
                url = trackingUrls[i];
            }
            trackingUrlMap.put(categories.get(i).getId(), url);
        }

        return trackingUrlMap;
    }

    @RequestMapping(value = "/step_2/{id}", method = RequestMethod.GET)
    public ModelAndView step2(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("entry/step_2");

        Entry entry = entryRepository.findOne(id);
        if (entry == null) {
            modelAndView.setViewName("redirect:/cms/entry/");
        } else if (!Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
            modelAndView.setViewName("redirect:/cms/entry/");
        } else {

            if (!Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
                Entry pending = entryDao.findPending(entry.getUid());
                if (pending != null) {
                    modelAndView.setViewName("redirect:/cms/entry/step_2/" + pending.getId());
                    return modelAndView;
                }
            }

            entry.setDocUrls(entryDocUrlRepository.findByPropertyId(entry.getId()));
            entry.setupDocUrls();

            modelAndView.addObject("entry", entry);

            //setEntryDocs(entry, modelAndView);
        }

        return modelAndView;
    }

    @RequestMapping(value = {"/step_2/{id}/add_entry_doc", "/edit/{id}/add_entry_doc"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public
    @ResponseBody
    RestResp addEntryDoc(@ModelAttribute AddFileModel addFileModel, @PathVariable("id") Long entryId) {
        logger.debug("add file model: {}", addFileModel);
        Entry entry = entryDao.findOne(entryId);
        if (entry == null) {
            return NO_RECORD_FOUND_RESP;
        } else if (Constants.STATUS_OVERRIDED.equalsIgnoreCase(entry.getStatus()) || Constants.STATUS_REJECTED.equalsIgnoreCase(entry.getStatus())) {
            return SYSTEM_ERROR_RESP;
        } else {

            EntryDoc record = new EntryDoc();

            record.setType(addFileModel.getDocType());
            record.setPrintDate(addFileModel.getPrintDate());
            record.setUpdateDate(addFileModel.getUpdateDate());
            record.setPostDate(addFileModel.getPostDate());

            record.setNameTc(addFileModel.getNameTc());
            record.setNameSc(addFileModel.getNameSc());
            record.setNameEn(addFileModel.getNameEn());

            for (MultipartFile file : addFileModel.getFiles()) {
                if (file.getSize() == 0) {
                    continue;
                }
                UploadFile uploadFile = new UploadFile();
                uploadFile.setMultipartFile(file);
                uploadFile.setOrdering(0);
                record.setFile(uploadFile);
                break;
            }
            try {

                EntryDoc doc = entryDao.addEntryDoc(record, entryId, getSessionUsername(request));

                if (doc != null) {
                    return new RestResp(0, getMessage("add_new_file_success"), "entry_doc", doc, "pending_id", doc.getEntry().getId());
                } else {
                    return NO_RECORD_FOUND_RESP;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new RestResp(999, e.getMessage());
            }
        }


    }

    @RequestMapping(value = {"/step_2/{entry_id}/delete_entry_doc", "/edit/{entry_id}/delete_entry_doc"}, method = {RequestMethod.POST, RequestMethod.DELETE})
    public
    @ResponseBody
    RestResp deleteEntryDoc(@RequestParam("entry_doc_id") Long docId, @PathVariable("entry_id") Long entryId) {

        logger.debug("entry id: {}", entryId);
        logger.debug("doc id: {}", docId);

        Entry entry = entryDao.findOne(entryId);

        if (entry == null || Constants.STATUS_OVERRIDED.equalsIgnoreCase(entry.getStatus()) || Constants.STATUS_REJECTED.equalsIgnoreCase(entry.getStatus())) {
            return SYSTEM_ERROR_RESP;
        }

        EntryDoc doc = entryDocRepository.findOne(docId);
        if (doc != null) {
            try {
                int res = entryDao.deleteEntryDoc(entryId, docId, getSessionUsername(request));
                if (res > 0) {
                    Entry pending = entryDao.findPending(doc.getEntry().getUid());
                    return new RestResp(Constants.REST_SUCCESS, getMessage("del_file_success"), "pending_id", pending == null ? null : pending.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("error: {}", e.getMessage());

                return new RestResp(Constants.REST_ERR_UNKNOW, e.getLocalizedMessage());
            }
        }
        return RECORD_NOT_FOUND_RESP;
    }

    @RequestMapping(value = {"/step_2/{entry_id}/edit_entry_doc", "/edit/{entry_id}/edit_entry_doc"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public
    @ResponseBody
    RestResp editEntryDoc(@RequestParam String nameTc,
                          @RequestParam String nameEn,
                          @RequestParam(required = false) String nameSc,
                          @RequestParam(required = false) Date updateDate,
                          @RequestParam(required = false) Date postDate,
                          @RequestParam(required = false) Date printDate,
                          @RequestParam Long docId,
                          @RequestParam(value = "file", required = false) MultipartFile multipartFile,
                          @PathVariable("entry_id") Long entryId) {

        logger.debug("entry id: {}", entryId);
        logger.debug("nameTc: {}", nameTc);
        logger.debug("nameEn: {}", nameEn);
        logger.debug("nameSc: {}", nameSc);
        logger.debug("printDate: {}", printDate);
        logger.debug("docId: {}", docId);

        Entry entry = entryDao.findOne(entryId);

        if (entry == null || Constants.STATUS_OVERRIDED.equalsIgnoreCase(entry.getStatus()) || Constants.STATUS_REJECTED.equalsIgnoreCase(entry.getStatus())) {
            return SYSTEM_ERROR_RESP;
        }

        EntryDoc docEntity = entryDocRepository.findOne(docId);
        if (docEntity != null) {
            try {
                EntryDoc doc = new EntryDoc();
                doc.setId(docEntity.getId());
                doc.setType(docEntity.getType());
                doc.setOrdering(docEntity.getOrdering());

                if (multipartFile != null && multipartFile.getSize() > 0) {

                    UploadFile newFile = new UploadFile();
                    newFile.setMultipartFile(multipartFile);
                    //newFile.setLang(Constants.LANG_TC);
                    newFile.setOrdering(0);

                    doc.setFile(newFile);
                }


                doc.setNameTc(nameTc);
                doc.setNameEn(nameEn);
                doc.setNameSc(nameSc);
                doc.setPrintDate(printDate);
                doc.setUpdateDate(updateDate);
                doc.setPostDate(postDate);


                doc = entryDao.updateEntryDoc(entryId, doc, getSessionUsername(request));
                if (doc != null) {
                    return new RestResp(Constants.REST_SUCCESS, getMessage("edit_success"), "pending_id", doc.getEntry().getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("error: {}", e.getMessage());

                return new RestResp(Constants.REST_ERR_UNKNOW, e.getLocalizedMessage());
            }
        }
        return RECORD_NOT_FOUND_RESP;
    }


    @RequestMapping(value = "/step_2/{entry_id}",
            method = {RequestMethod.POST, RequestMethod.PUT})
    public ModelAndView completeCreate(@RequestParam(required = false) Date printDate,
                                       @RequestParam(required = false) Date viewDate,
                                       @RequestParam(required = false) String mortage,
                                       @RequestParam(required = false) String docUrlSb,
                                       @RequestParam(required = false) String docUrlPl,
                                       @RequestParam(required = false) String docUrlSa,
                                       @RequestParam(required = false) String docUrlCr,
                                       @PathVariable("entry_id") Long entryId,
                                       RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/cms/entry/");
        logger.debug("entry id: {}", entryId);
        logger.debug("printDate: {}", printDate);
        logger.debug("viewDate: {}", viewDate);
        logger.debug("mortage: {}", mortage);
        Entry entry = entryDao.findOne(entryId);
        if (entry != null && Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {

            Entry valid = new Entry(entry);
            valid.setPrintDate(printDate);
            valid.setViewDate(viewDate);
            valid.setMortage(mortage);


            if (valid.getDocUrls() == null) {
                valid.setDocUrls(new ArrayList<>());
            }

            if (docUrlSb != null) {
                valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_SALES_BROCHURE, docUrlSb));
            }
            if (docUrlPl != null) {
                valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_PRICE_LIST, docUrlPl));
            }
            if (docUrlSa != null) {
                valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_SALES_ARRANGEMENT, docUrlSa));
            }
            if (docUrlCr != null) {
                valid.getDocUrls().add(new EntryDocUrl(Constants.DOC_COMPLETED_RECORD, docUrlCr));
            }


            try {
                Entry updated = entryDao.update(valid, entryId, getSessionUsername(request));
                if (updated != null) {
                    redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("add_success"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, e.getLocalizedMessage());
            }
        }
        return modelAndView;
    }


    @RequestMapping(value = "/sort", method = RequestMethod.GET)
    public String initSort(Model model) {

        List<Entry> list = entryRepository.findForInitReorder();

        List<EntryReorder> unsorted = new ArrayList<>();
        List<EntryReorder> sorted = new ArrayList<>();

        for (Entry entry : list) {
            EntryReorder reorder = new EntryReorder(entry.getUid(), entry.getNameTc(), entry.getNameEn(), entry.getNameSc());
            if (false && Constants.UNSORTED_VALUE == entry.getOrdering()) {
                if (!unsorted.contains(reorder)) {
                    unsorted.add(reorder);
                }
            } else {
                if (!sorted.contains(reorder)) {
                    sorted.add(reorder);
                }
            }
        }

        model.addAttribute("unsorted", unsorted);
        model.addAttribute("sorted", sorted);

        return "entry/sort";
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public String sort(@RequestParam(required = false) String[] unsorted,
                       @RequestParam(required = false) String[] sorted,
                       RedirectAttributes redirectAttributes) {
        logger.debug("Unsorted");
        if (unsorted != null) {
            for (String s : unsorted) {
                logger.debug(s);
            }
        }


        logger.debug("Sorted");
        if (sorted != null) {
            for (String s : sorted) {
                logger.debug(s);
            }
        }

        try {
            entryDao.sort(unsorted == null ? null : Arrays.asList(unsorted), sorted == null ? null : Arrays.asList(sorted), getSessionUsername(request));

            redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("sort_success"));
        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, e.getLocalizedMessage());
        }

        return "redirect:/cms/entry/";
    }

    @RequestMapping(value = {"/updated-today"})
    public String updatedToday(Model model) {


        return "entry/updated_today";
    }

    @RequestMapping(value = {"/pending"})
    public String pending(Model model) {


        return "entry/pending";
    }

    @RequestMapping(value = "/uidExists")
    public
    @ResponseBody
    RestResp uidExists(@RequestParam(required = false, defaultValue = "") String uid) {
        boolean exists = entryDao.uniqueIdExists(uid);

        return new RestResp(Constants.REST_SUCCESS, "", "exists", exists);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestParam(required = false, defaultValue = "") Long id, RedirectAttributes redirectAttributes) {
        try {
            Entry entry = entryDao.findOne(id);
            if (entry != null) {
                entryDao.delete(id, getSessionUsername(request));

                districtDao.reloadDistrictCache();

                redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("delete_success"));
            } else {
                redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("delete_failure"));

            }
        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("delete_failure"));
        }
        return "redirect:/cms/entry/";
    }
}

@Data
@NoArgsConstructor
class AddFileModel {
    @NotNull
    private String nameTc;

    private String nameSc;

    @NotNull
    private String nameEn;

    private Date postDate;

    private Date updateDate;

    private Date printDate;

    private String docType;

    private Long docId;

    private List<MultipartFile> files;
}

@Data
@AllArgsConstructor
class EntryReorder {
    private String uid;
    private String nameTc;
    private String nameEn;
    private String nameSc;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntryReorder that = (EntryReorder) o;

        return uid.equals(that.uid);

    }

    @Override
    public int hashCode() {
        return uid.hashCode();
    }
}

