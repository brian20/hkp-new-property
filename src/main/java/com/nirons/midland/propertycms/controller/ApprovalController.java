package com.nirons.midland.propertycms.controller;

import com.nirons.midland.propertycms.dao.DistrictDao;
import com.nirons.midland.propertycms.dao.EntryDao;
import com.nirons.midland.propertycms.entity.Entry;
import com.nirons.midland.propertycms.entity.EntryDoc;
import com.nirons.midland.propertycms.repository.EntryDocRepository;
import com.nirons.midland.propertycms.repository.EntryDocUrlRepository;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jaquesyang on 15/7/3.
 */
//@Controller
//@RequestMapping("/cms/approval")
public class ApprovalController extends BaseController {
    @Autowired
    EntryRepository repository;

    @Autowired
    EntryDao dao;

    @Autowired
    EntryDocRepository entryDocRepository;

    @Autowired
    DistrictDao districtDao;

    @Autowired
    EntryDocUrlRepository entryDocUrlRepository;

    @RequestMapping(value = "")
    public String list(Model model) {

        return "approval/list";
    }

    @RequestMapping(value = "/review/{id}")
    public String review(Model model, @PathVariable Long id) {
        Entry entry = repository.findOne(id);
        if (entry == null) {
            return "redirect:/cms/approval";
        }

        entry.setDocUrls(entryDocUrlRepository.findByPropertyId(entry.getId()));
        entry.setupDocUrls();

        Entry approved = null;
        if (Constants.STATUS_APPROVED.equalsIgnoreCase(entry.getStatus())) {
            approved = entry;
        } else {
            approved = repository.findByUidAndStatus(entry.getUid(), Constants.STATUS_APPROVED);
        }

        if (approved != null) {
            approved.setDocUrls(entryDocUrlRepository.findByPropertyId(approved.getId()));
            approved.setupDocUrls();
        }

        model.addAttribute("entry", entry);
        model.addAttribute("approved", approved);
        model.addAttribute("mode", "approval");

        List<EntryDoc> docs = entryDocRepository.findByEntry(entry);
        List<EntryDoc> approvedDocs = null;
        if (approved != null && !approved.getId().equals(entry.getId())) {
            approvedDocs = entryDocRepository.findByEntry(approved);
        }

        String[] types = {Constants.DOC_VIEW_RECORD, Constants.DOC_SALES_MANUAL_PART, Constants.DOC_SALES_MANUAL_FULL, Constants.DOC_PRICE_LIST, Constants.DOC_SALES_ARRANGEMENT, Constants.DOC_COMPLETED_RECORD};
        Map<String, List<EntryDoc>> docMap = new HashMap<>();
        for (String type : types) {
            docMap.put(type, new ArrayList<>());
        }
        for (EntryDoc doc : docs) {

            if (doc.getType() == null || docMap.get(doc.getType()) == null) {
                continue;
            }
            docMap.get(doc.getType()).add(doc);
            if (approvedDocs != null) {
                EntryDoc sameDoc = doc.findSameDoc(approvedDocs);
                if (sameDoc != null) {
                    doc.setChanged(doc.isDocChanged(sameDoc));
                } else {
                    doc.setChanged(true);
                }
            }

        }
        model.addAttribute("docMap", docMap);

        return "approval/form";
    }


    @RequestMapping(value = "/approve/{id}", method = RequestMethod.POST)
    public String approve(@PathVariable Long id, HttpServletRequest req, RedirectAttributes redirectAttributes) {
        Entry entry = dao.findOne(id);
        if (entry != null && Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
            Entry approved = dao.approve(id, getSessionUsername(req));

            if (approved != null) {

                if (Constants.STATIC_TRICK_FIRST_TIME_URL != null && !Constants.STATIC_TRICK_FIRST_TIME_URL.trim().isEmpty()) {
                    String firstTrick = Utils.callURL(Constants.STATIC_TRICK_FIRST_TIME_URL.replace("[uid]", entry.getId().toString()));
                    logger.debug("first trick: " + firstTrick);
                }

                districtDao.reloadDistrictCache();

                redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("approve_success"));
            }
        } else {

            redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("record_not_found"));
        }

        return "redirect:/cms/approval/";
    }


    @RequestMapping(value = "/reject/{id}", method = RequestMethod.POST)
    public String reject(@PathVariable Long id, HttpServletRequest req, RedirectAttributes redirectAttributes) {
        Entry entry = dao.findOne(id);
        if (entry != null && Constants.STATUS_PENDING.equalsIgnoreCase(entry.getStatus())) {
            Entry rejected = dao.reject(id, getSessionUsername(req));

            if (rejected != null) {
                redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("reject_success"));
            }
        } else {

            redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("record_not_found"));
        }

        return "redirect:/cms/approval/";
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestParam(required = false, defaultValue = "") Long id, RedirectAttributes redirectAttributes) {
        try {
            Entry entry = dao.findOne(id);
            if (entry != null) {
                dao.delete(id, getSessionUsername(request));
                redirectAttributes.addFlashAttribute(SUCCESS_MESSAGE, getMessage("delete_success"));
            } else {
                redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("delete_failure"));

            }
        } catch (Exception e) {
            e.printStackTrace();
            redirectAttributes.addFlashAttribute(FAILURE_MESSAGE, getMessage("delete_failure"));
        }
        return "redirect:/cms/approval/";
    }
}
