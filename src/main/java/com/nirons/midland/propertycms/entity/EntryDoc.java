package com.nirons.midland.propertycms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nirons.midland.propertycms.util.Utils.localeText;

/**
 * Created by jaquesyang on 15/6/22.
 */
@Entity
@Table(name = "fh_property_doc")
@Data
@NoArgsConstructor
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class EntryDoc implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_property_doc_seq")
    @SequenceGenerator(name = "fh_property_doc_seq", sequenceName = "fh_property_doc_seq", allocationSize = 1)
    @Column(name = "property_doc_id")
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "property_id")
    private Entry entry;

    private String type;

    @Temporal(TemporalType.DATE)
    private Date printDate;

    @Temporal(TemporalType.DATE)
    private Date updateDate;

    @Temporal(TemporalType.DATE)
    private Date postDate;

    private String nameTc;

    @Transient
    private String nameSc;

    private String nameEn;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "upload_file_id")
    private UploadFile file;

    private int ordering;

    private String status = Constants.STATUS_ACTIVE;

    @JsonIgnore
    @Column(updatable = false)
    private String createdBy;

    @JsonIgnore
    private String modifiedBy;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();

    private Long categoryId;

    @Transient
    @JsonIgnore
    private boolean changed;


    public void updateFields(EntryDoc doc) {
        setNameTc(doc.getNameTc());
        setNameSc(doc.getNameSc());
        setNameEn(doc.getNameEn());

        setType(doc.getType());
        setFile(doc.getFile());

        setOrdering(doc.getOrdering());

        setPrintDate(doc.getPrintDate());

        setUpdateDate(doc.getUpdateDate());

        setPostDate(doc.getPostDate());

        setCategoryId(doc.getCategoryId());
    }


    public String getFileUrl() {
        if (file != null) {
            return file.getFileUrl();
        }
        return "";
    }

    public boolean isDocChanged(EntryDoc doc) {
        if (doc == null) {
            return true;
        }
        if (!Utils.equals(nameTc, doc.nameTc)) {
            return true;
        }
        /*
        if (!Utils.equals(nameSc, doc.nameSc)) {
            return true;
        }*/
        if (!Utils.equals(nameEn, doc.nameEn)) {
            return true;
        }
        if (!Utils.equals(printDate, doc.printDate)) {
            return true;
        }
        if (!Utils.equals(updateDate, doc.updateDate)) {
            return true;
        }
        if (!Utils.equals(postDate, doc.postDate)) {
            return true;
        }

        return false;
    }

    public EntryDoc findSameDoc(List<EntryDoc> docs) {
        if (type == null || file == null || file.getId() == null) {
            return null;
        }
        for (EntryDoc doc : docs) {
            if (doc.getType() == null || doc.getFile() == null || doc.getFile().getId() == null) {
                continue;
            }
            if (type.equalsIgnoreCase(doc.getType()) && file.getId().equals(doc.getFile().getId())) {
                return doc;
            }
        }
        return null;
    }


    public Map<String, Object> toApiListJson(int lang) {
        Map<String, Object> map = new HashMap<>();

        map.put("id", id);
        map.put("name", localeText(nameTc, nameEn, getNameSc(), lang));
        map.put("size", getFile() == null ? "0 KB" : getFile().getBytes());
        map.put("ordering", ordering);
        map.put("fileUrl", getFileUrl());


        return map;
    }

    public String getNameSc() {
        if (this.nameSc == null) {
            this.nameSc = Chinese.toSc(nameTc);
        }
        return this.nameSc;
    }


}
