package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jaquesyang on 15/6/30.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "fh_log")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_log_seq")
    @SequenceGenerator(name = "fh_log_seq", sequenceName = "fh_log_seq", allocationSize = 1)
    private Long id;

    private String username;

    private String action;

    private String description;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();
}
