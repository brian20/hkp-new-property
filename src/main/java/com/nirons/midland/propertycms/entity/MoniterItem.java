package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jaquesyang on 15/8/13.
 */
@Entity
@Table(name = "fh_grab_moniter_item")
@Data
@NoArgsConstructor
public class MoniterItem {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_grab_moniter_item_seq")
    @SequenceGenerator(name = "fh_grab_moniter_item_seq", sequenceName = "fh_grab_moniter_item_seq", allocationSize = 1)
    @Column(name = "item_id")
    private Long id;

    private Long propertyId;

    private Long categoryId;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "last_modified")
    private Date modified = new Date();

    private String trackingUrl;

    private String content;
}
