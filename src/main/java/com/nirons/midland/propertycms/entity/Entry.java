package com.nirons.midland.propertycms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

import static com.nirons.midland.propertycms.util.Utils.localeText;
import static com.nirons.midland.propertycms.util.Utils.validKey;

/**
 * Created by jaquesyang on 15/6/15.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "fh_property")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Entry {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_property_seq")
    @SequenceGenerator(name = "fh_property_seq", sequenceName = "fh_property_seq", allocationSize = 1)
    @Column(name = "property_id")
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "district_id")
    private District district = new District();

    private boolean hot = true;

    @NotNull
    @Column(updatable = false, name = "u_id")
    private String uid;

    private String nameEn;

    private String urlKeyEn;

    private String nameTc;

    private String urlKeyTc;

    @Transient
    private String nameSc;

    @Transient
    private String urlKeySc;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "upload_file_id")
    private UploadFile uploadFile;

    private int ordering = Constants.UNSORTED_VALUE;

    private String youtube;

    private String phaseNum;

    private String phaseNumEn;

    @Transient
    private String phaseNumSc;

    private String phaseNameEn;

    private String phaseNameTc;

    @Transient
    private String phaseNameSc;

    private String areaTc;

    private String areaEn;

    @Transient
    private String areaSc;

    private String addrEn;

    private String addrTc;

    @Transient
    private String addrSc;

    private String devProjectTc;

    private String devProjectEn;

    @Transient
    private String devProjectSc;

    @ManyToOne
    @JoinColumn(name = "developer_id")
    private Developer developer = new Developer();

    private String lat;

    private String lng;

    private String projectUrl;

    private String salesManualUrl;

    @Temporal(TemporalType.DATE)
    private Date printDate;

    @Temporal(TemporalType.DATE)
    private Date viewDate;

    private String mortage;

    @Temporal(TemporalType.DATE)
    private Date keyDate;

    private String kindergartenSchoolNetwork;
    private String kindergartenSchoolNetworkEn;
    @Transient
    private String kindergartenSchoolNetworkSc;

    private String primarySchoolNetwork;
    private String primarySchoolNetworkEn;
    @Transient
    private String primarySchoolNetworkSc;

    private String secondarySchoolNetwork;
    private String secondarySchoolNetworkEn;
    @Transient
    private String secondarySchoolNetworkSc;

    private String status = Constants.STATUS_PENDING;

    @Column(updatable = false)
    private String createdBy;

    private String modifiedBy;

    private String approvedBy;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();

    private boolean tracking = true;

    private boolean hidden;

    private boolean hiddenForHkp;

    @Transient
    private List<EntryDocUrl> docUrls = new ArrayList<>();

    @Transient
    private String docUrlSb;

    @Transient
    private String docUrlPl;

    @Transient
    private String docUrlSa;

    @Transient
    private String docUrlCr;


    public Entry(Long id) {
        this();
        setId(id);
    }

    public Entry(Entry entry) {
        this();
        updateFields(entry);
    }

    public String getImageUrl() {
        if (uploadFile != null) {
            return uploadFile.getFileUrl();
        }
        return "";
    }

    public String getUrlKeySc() {
        if (this.urlKeySc == null) {
            this.urlKeySc = Chinese.toSc(urlKeyTc);
        }
        return this.urlKeySc;
    }

    public String getNameSc() {
        if (this.nameSc == null) {
            this.nameSc = Chinese.toSc(nameTc);
        }
        return this.nameSc;
    }

    public String getPhaseNameSc() {
        if (this.phaseNameSc == null) {
            this.phaseNameSc = Chinese.toSc(phaseNameTc);
        }
        return this.phaseNameSc;
    }

    public String getPhaseNumSc() {
        if (this.phaseNumSc == null) {
            this.phaseNumSc = Chinese.toSc(phaseNum);
        }
        return this.phaseNumSc;
    }

    public String getAreaSc() {
        if (this.areaSc == null) {
            this.areaSc = Chinese.toSc(areaTc);
        }
        return this.areaSc;
    }

    public String getAddrSc() {
        if (this.addrSc == null) {
            this.addrSc = Chinese.toSc(addrTc);
        }
        return this.addrSc;
    }

    public String getKindergartenSchoolNetworkSc() {
        if (this.kindergartenSchoolNetworkSc == null) {
            this.kindergartenSchoolNetworkSc = Chinese.toSc(kindergartenSchoolNetwork);
        }
        return this.kindergartenSchoolNetworkSc;
    }

    public String getPrimarySchoolNetworkSc() {
        if (this.primarySchoolNetworkSc == null) {
            this.primarySchoolNetworkSc = Chinese.toSc(primarySchoolNetwork);
        }
        return this.primarySchoolNetworkSc;
    }

    public String getSecondarySchoolNetworkSc() {
        if (this.secondarySchoolNetworkSc == null) {
            this.secondarySchoolNetworkSc = Chinese.toSc(secondarySchoolNetwork);
        }
        return this.secondarySchoolNetworkSc;
    }

    public String getDevProjectSc() {
        if (this.devProjectSc == null) {
            this.devProjectSc = Chinese.toSc(devProjectTc);
        }
        return this.devProjectSc;
    }

    public void updateFields(Entry valid) {
        setDistrict(valid.getDistrict());
        setUid(valid.getUid());
        setHot(valid.isHot());
        setNameEn(valid.getNameEn());
        setNameTc(valid.getNameTc());
        setNameSc(valid.getNameSc());
        setYoutube(valid.getYoutube());

        setPhaseNum(valid.getPhaseNum());
        setPhaseNumEn(valid.getPhaseNumEn());
        setPhaseNameEn(valid.getPhaseNameEn());
        setPhaseNameTc(valid.getPhaseNameTc());
        setPhaseNameSc(valid.getPhaseNameSc());

        setAreaEn(valid.getAreaEn());
        setAreaTc(valid.getAreaTc());
        setAreaSc(valid.getAreaSc());

        setAddrEn(valid.getAddrEn());
        setAddrTc(valid.getAddrTc());
        setAddrSc(valid.getAddrSc());

        setLat(valid.getLat());
        setLng(valid.getLng());
        setProjectUrl(valid.getProjectUrl());
        setSalesManualUrl(valid.getSalesManualUrl());
        setPrintDate(valid.getPrintDate());
        setViewDate(valid.getViewDate());

        setUploadFile(valid.getUploadFile());

        setMortage(valid.getMortage());

        setDeveloper(valid.getDeveloper());

        setKeyDate(valid.getKeyDate());
        setKindergartenSchoolNetwork(valid.getKindergartenSchoolNetwork());
        setKindergartenSchoolNetworkEn(valid.getKindergartenSchoolNetworkEn());
        setKindergartenSchoolNetworkSc(valid.getKindergartenSchoolNetworkSc());

        setPrimarySchoolNetwork(valid.getPrimarySchoolNetwork());
        setPrimarySchoolNetworkEn(valid.getPrimarySchoolNetworkEn());
        setPrimarySchoolNetworkSc(valid.getPrimarySchoolNetworkSc());

        setSecondarySchoolNetwork(valid.getSecondarySchoolNetwork());
        setSecondarySchoolNetworkEn(valid.getSecondarySchoolNetworkEn());
        setSecondarySchoolNetworkSc(valid.getSecondarySchoolNetworkSc());

        setUrlKeyEn(valid.getUrlKeyEn());
        setUrlKeyTc(valid.getUrlKeyTc());


        setTracking(valid.isTracking());

        setDevProjectTc(valid.getDevProjectTc());
        setDevProjectEn(valid.getDevProjectEn());

        setHidden(valid.isHidden());
        setHiddenForHkp(valid.isHiddenForHkp());
    }

    public String getName(Locale locale) {
        if ("en".equalsIgnoreCase(locale.getLanguage())) {
            return getNameEn();
        }
        if ("cn".equalsIgnoreCase(locale.getCountry())) {
            return getNameSc();
        }

        return getNameTc();
    }

    public Map<String, Object> toApiListJson(int lang) {
        Map<String, Object> map = new HashMap<>();
        map.put("addr", localeText(getAddrTc(), getAddrEn(), getAddrSc(), lang));
        map.put("district", localeText(areaTc, areaEn, getAreaSc(), lang));
        map.put("name_en", nameEn);
        map.put("est_id", uid);
        map.put("name_zh", localeText(nameTc, nameTc, getNameSc(), lang));
        map.put("region", district == null || district.getId() == null ? "" : localeText(district.getNameTc(), district.getNameEn(), district.getNameSc(), lang));
        map.put("wan_doc_path", getImageUrl());

        return map;
    }


    public Map<String, Object> toApiDetailJson(int lang) {
        Map<String, Object> map = new HashMap<>();
        map.put("est_id", uid);
        map.put("addr", localeText(addrTc, addrEn, getAddrSc(), lang));
        map.put("developer", developer == null || developer.getId() == null ? "" : localeText(developer.getNameTc(), developer.getNameEn(), developer.getNameSc(), lang));
        map.put("district", localeText(areaTc, areaEn, getAreaSc(), lang));
        //map.put("firstPrintDate", printDate);
        map.put("keyDate", keyDate);
        map.put("lat", lat);
        map.put("lng", lng);
        map.put("name_en", nameEn);
        map.put("name_zh", localeText(nameTc, nameTc, getNameSc(), lang));
        map.put("phaseName", localeText(phaseNameTc, phaseNameEn, getPhaseNameSc(), lang));
        map.put("phaseNum", localeText(phaseNum, phaseNumEn, getPhaseNumSc(), lang));
        map.put("kgNetwork", localeText(kindergartenSchoolNetwork, kindergartenSchoolNetworkEn, getKindergartenSchoolNetworkSc(), lang));
        map.put("psNetwork", localeText(primarySchoolNetwork, primarySchoolNetworkEn, getPrimarySchoolNetworkSc(), lang));
        map.put("ssNetwork", localeText(secondarySchoolNetwork, secondarySchoolNetworkEn, getSecondarySchoolNetworkSc(), lang));
        map.put("region", district == null || district.getId() == null ? "" : localeText(district.getNameTc(), district.getNameEn(), district.getNameSc(), lang));
        //map.put("salesManualUrl", salesManualUrl);
        //map.put("srpeUrl", Constants.STATIC_SRPE_URL);
        map.put("videoUrl", youtube);
        map.put("viewDate", viewDate);
        map.put("wan_doc_path", getImageUrl());
        map.put("websiteUrl", projectUrl);
        map.put("mortgageUrl", mortage);
        map.put("develop_project", localeText(devProjectTc, devProjectEn, getDevProjectSc(), lang));


        return map;
    }

    public String textInFrontend(String tc, String en, String sc, Locale locale) {
        int lang = Utils.localeLang(locale);
        StringBuilder sb = new StringBuilder();

        String name1, name2;

        if (lang == 1) {
            name1 = (en == null ? "" : en);
            name2 = (tc == null ? "" : tc);
        } else {
            name1 = (localeText(tc, tc, sc, lang));

            name2 = (en == null ? "" : en);
        }

        if (StringUtils.isEmpty(name1) && StringUtils.isEmpty(name2)) {
            sb.append("--");
        } else {

            sb.append("<span class='nr-blue'>");
            sb.append(name1);
            sb.append("</span>");
            if (lang == -1) {
                sb.append("<span class='nr-blue nr-second-name'>");
                sb.append(name2);
                sb.append("</span>");
            }
        }

        return sb.toString();
    }


    public String nameInFrontendTitle(Locale locale) {

        String tc = nameTc;
        String en = nameEn;
        String sc = getNameSc();

        int lang = Utils.localeLang(locale);
        StringBuilder sb = new StringBuilder();

        String name1, name2;

        if (lang == 1) {
            name1 = (en == null ? "" : en);
            name2 = (tc == null ? "" : tc);
        } else {
            name1 = (localeText(tc, tc, sc, lang));

            name2 = (en == null ? "" : en);
        }

        if (StringUtils.isEmpty(name1) && StringUtils.isEmpty(name2)) {
            sb.append("--");
        } else {

            sb.append("<span class='nr-blue'>");
            sb.append(name1);
            sb.append("</span>");
            sb.append("<span class='nr-blue nr-second-name'>");
            sb.append(name2);
            sb.append("</span>");
        }

        return sb.toString();
    }

    public String nameInFrontend(Locale locale) {
        return textInFrontend(nameTc, nameEn, getNameSc(), locale);
    }

    public String devProjectInFrontend(Locale locale) {
        return textInFrontend(devProjectTc, devProjectEn, getDevProjectSc(), locale);
    }

    public String phaseNumInFrontend(Locale locale) {
        return textInFrontend(phaseNum, phaseNumEn, getPhaseNumSc(), locale);
    }

    public String phaseNameInFrontend(Locale locale) {
        return textInFrontend(phaseNameTc, phaseNameEn, getPhaseNameSc(), locale);
    }

    public String addrInFrontend(Locale locale) {
        return textInFrontend(addrTc, addrEn, getAddrSc(), locale);
    }

    public String areaInFrontend(Locale locale) {
        return textInFrontend(areaTc, areaEn, getAreaSc(), locale);
    }


    public String docUrlForType(String type) {
        if (docUrls == null || docUrls.isEmpty()) {
            return null;
        }
        for (EntryDocUrl docUrl : docUrls) {
            if (type.equalsIgnoreCase(docUrl.getType())) {
                return docUrl.getUrl();
            }
        }

        return null;
    }

    public void setupDocUrls() {
        setDocUrlSb(docUrlForType(Constants.DOC_SALES_BROCHURE));
        setDocUrlPl(docUrlForType(Constants.DOC_PRICE_LIST));
        setDocUrlSa(docUrlForType(Constants.DOC_SALES_ARRANGEMENT));
        setDocUrlCr(docUrlForType(Constants.DOC_COMPLETED_RECORD));
    }
	
	public boolean isHidden(){
		return this.hiddenForHkp;
	}
}
