package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by jaquesyang on 15/6/10.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "fh_user")
public class User implements UserDetails {

    @Id
    private String username;

    private String password;

    @NotNull
    @Size(min = 2, max = 50, message = "{User.name.validation}")
    private String name;

    @NotNull
    @Size(min = 2, max = 255)
    private String email;

    private boolean active;

    /**
     * <ul>
     * <li>A: admin</li>
     * <li>U: user</li>
     * </ul>
     */
    private String type;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        if ("A".equalsIgnoreCase(type)) {
            //authorities.add(() -> "ROLE_ADMIN");
            authorities.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "ROLE_ADMIN";
                }
            });
        } else {
            //authorities.add(() -> "ROLE_USER");
            authorities.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "ROLE_USER";
                }
            });
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isActive();
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isActive();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isActive();
    }

    @Override
    public boolean isEnabled() {
        return this.isActive();
    }
}
