package com.nirons.midland.propertycms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nirons.midland.propertycms.util.Chinese;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jaquesyang on 15/6/15.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "fh_developer")
public class Developer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_developer_seq")
    @SequenceGenerator(name = "fh_developer_seq", sequenceName = "fh_developer_seq", allocationSize = 1)
    @Column(name = "developer_id")
    private Long id;

    private String nameEn;

    private String nameTc;

    @Transient
    private String nameSc;

    private String urlKey;

    private int ordering;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();


    public String getNameSc() {
        if (this.nameSc == null) {
            this.nameSc = Chinese.toSc(nameTc);
        }
        return this.nameSc;
    }

    public String getName(Locale locale) {
        if ("en".equalsIgnoreCase(locale.getLanguage())) {
            return nameEn;
        }
        if ("cn".equalsIgnoreCase(locale.getCountry())) {
            return getNameSc();
        }

        return nameTc;
    }


    public void updateFields(Developer developer) {

        setNameTc(developer.getNameTc());
        setNameEn(developer.getNameEn());
        setNameSc(developer.getNameSc());
        setUrlKey(developer.getUrlKey());

    }
}
