package com.nirons.midland.propertycms.entity;

import com.nirons.midland.propertycms.util.Chinese;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Created by jaquesyang on 15/8/12.
 */
@Entity
@Data
@NoArgsConstructor
public class Area {
    @Id
    @Column(name = "area_tc")
    private String nameTc;


    @Column(name = "area_en")
    private String nameEn;


    @Transient
    private String nameSc;


    public String getNameSc() {
        if (this.nameSc == null) {
            this.nameSc = Chinese.toSc(nameTc);
        }
        return this.nameSc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        return nameTc.equals(area.nameTc);

    }

    @Override
    public int hashCode() {
        return nameTc.hashCode();
    }
}
