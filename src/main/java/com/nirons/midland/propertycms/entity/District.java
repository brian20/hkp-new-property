package com.nirons.midland.propertycms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Utils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by jaquesyang on 15/6/15.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "fh_district")
public class District implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_district_seq")
    @SequenceGenerator(name = "fh_district_seq", sequenceName = "fh_district_seq", allocationSize = 1)
    @Column(name = "district_id")
    private Long id;

    private String nameEn;

    private String nameTc;

    @Transient
    private String nameSc;

    private String urlKey;

    private int ordering;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();

    @Transient
    private List<Area> areas = new ArrayList<>();


    public String getName(Locale locale) {
        if ("en".equalsIgnoreCase(locale.getLanguage())) {
            return nameEn;
        }
        if ("cn".equalsIgnoreCase(locale.getCountry())) {
            return getNameSc();
        }

        return nameTc;
    }


    public String getNameSc() {
        if (this.nameSc == null) {
            this.nameSc = Chinese.toSc(nameTc);
        }
        return this.nameSc;
    }

    public List<Area> getAreas(Locale locale) {
        List<Area> list = new ArrayList<>(areas);

        int lang = Utils.localeLang(locale);

        Collections.sort(list, (o1, o2) -> {
            if (lang == 1) {
                if (o1.getNameEn() == null) {
                    return -1;
                }
                return o1.getNameEn().compareToIgnoreCase(o2.getNameEn());
            }
            if (o1.getNameTc() == null) {
                return -1;
            }
            return o1.getNameTc().compareToIgnoreCase(o2.getNameTc());
        });

        return list;
    }
}
