package com.nirons.midland.propertycms.entity;

import com.nirons.midland.propertycms.util.Constants;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by jaquesyang on 15/7/3.
 */
@Entity
@Table(name = "fh_email_notification")
@Data
@NoArgsConstructor
public class EmailNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_email_notification_seq")
    @SequenceGenerator(name = "fh_email_notification_seq", sequenceName = "fh_email_notification_seq", allocationSize = 1)
    private Long id;

    @NotNull
    private String email;

    @Column(updatable = false)
    private String createdBy;

    private String modifiedBy;

    private String status = Constants.STATUS_ACTIVE;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();
}
