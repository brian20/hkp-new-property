package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jaquesyang on 15/9/9.
 */
@Entity
@Table(name = "fh_property_doc_url")
@Data
@NoArgsConstructor
public class EntryDocUrl implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_property_doc_url_seq")
    @SequenceGenerator(name = "fh_property_doc_url_seq", sequenceName = "fh_property_doc_url_seq", allocationSize = 1)
    private Long id;

    private Long propertyId;

    private String type;

    private String url;

    public EntryDocUrl(String type, String url) {
        this();

        setType(type);
        setUrl(url);
    }
}
