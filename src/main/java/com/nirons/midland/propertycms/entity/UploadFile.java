package com.nirons.midland.propertycms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import lombok.*;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by jaquesyang on 15/6/22.
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "fh_upload_file")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class UploadFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fh_upload_file_seq")
    @SequenceGenerator(name = "fh_upload_file_seq", sequenceName = "fh_upload_file_seq", allocationSize = 1)
    @Column(name = "upload_file_id")
    private Long id;

    private String name;

    @Column(name = "file_size")
    private long size;

    private String path;

    private byte[] data;

    private int ordering;

    private String status = Constants.STATUS_ACTIVE;

    @Column(updatable = false)
    private String createdBy;

    private String modifiedBy;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(updatable = false)
    private Date created = new Date();

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date modified = new Date();

    @Transient
    @JsonIgnore
    private MultipartFile multipartFile;

    @Transient
    private String refId;

    public String getBytes() {
        return Utils.bytesToSize(size);
    }

    public String getFileUrl() {
        if (!StringUtils.isEmpty(path)) {
            return Constants.STATIC_FILE_SERVER + "/" + path;
        }

        return "";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UploadFile that = (UploadFile) o;

        return !(id != null ? !id.equals(that.id) : true);

    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
        if (multipartFile != null) {
            setName(multipartFile.getOriginalFilename());
            setSize(multipartFile.getSize());
        }
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();
    }


}
