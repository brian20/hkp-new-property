package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by jaquesyang on 15/8/13.
 */
@Entity
@Table(name = "fh_grab_category")
@Data
@NoArgsConstructor
public class Category {
    @Id
    @Column(name = "category_id")
    private Long id;

    private String name;
}
