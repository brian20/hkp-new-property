package com.nirons.midland.propertycms.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by jaquesyang on 15/6/22.
 */
@Entity
@Table(name = "fh_setting")
@Data
@NoArgsConstructor
public class Setting {
    @Id
    private String key;

    private String value;
}
