package com.nirons.midland.propertycms.api;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.entity.*;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.DistrictRepository;
import com.nirons.midland.propertycms.repository.EntryDocRepository;
import com.nirons.midland.propertycms.repository.EntryDocUrlRepository;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import javassist.compiler.ast.ASTList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.temporal.ValueRange;
import java.util.*;

/**
 * Created by jaquesyang on 15/8/27.
 */
@RestController
@RequestMapping("/data")
public class DataRestController extends BaseController {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    EntryRepository entryRepository;

    @Autowired
    EntryDocRepository entryDocRepository;

    @Autowired
    EntryDocUrlRepository entryDocUrlRepository;

    private static final String[] sub_types = {Constants.DOC_VIEW_RECORD,
            Constants.DOC_SALES_MANUAL_PART,
            Constants.DOC_SALES_MANUAL_FULL,
            Constants.DOC_PRICE_LIST,
            Constants.DOC_SALES_ARRANGEMENT,
            Constants.DOC_COMPLETED_RECORD};

    @RequestMapping("/list")
    public RestResp list(@RequestParam(required = false) Integer page,
                         @RequestParam(value = "pageSize", required = false) Integer size,
                         @RequestParam(required = false) String region,
                         @RequestParam(value = "district", required = false) String area,
                         @RequestParam(value = "searchname", required = false) String name) {
        if (page == null) {
            page = 0;
        }

        if (size == null) {
            size = 20;
        }

        if (size <= 0) {
            size = Integer.MAX_VALUE;
            page = 0;
        }

        final int pageNumber = page.intValue();

        final int pageSize = size.intValue();

        Pageable pageable = new Pageable() {
            @Override
            public int getPageNumber() {
                return pageNumber;
            }

            @Override
            public int getPageSize() {
                return pageSize;
            }

            @Override
            public int getOffset() {
                return pageNumber * pageSize;
            }

            @Override
            public Sort getSort() {
                return new Sort(Sort.Direction.ASC, "ordering");
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        };

        Page<Entry> entryPage;

        int lang = Utils.localeLang(LocaleContextHolder.getLocale());
        if (!StringUtils.isEmpty(name)) {
            name = name.trim().toLowerCase();
            if (lang == 2) {
                name = Chinese.toTc(name);
            }
        }

        if (!StringUtils.isEmpty(area)) {
            area = area.trim().toLowerCase();
            if (lang == 2) {
                area = Chinese.toTc(area);
            }
        }

        if ("hot".equalsIgnoreCase(region)) {
            entryPage = entryRepository.findApprovedHotByName(nameParam(name), pageable);
        } else {
            entryPage = entryRepository.findApprovedByDistrictAndAreaAndName(region, area, nameParam(name), pageable);
        }


        if (entryPage != null && entryPage.hasContent()) {

            List<Map<String, Object>> list = new ArrayList<>();
            for (Entry entry : entryPage) {

                Map<String, Object> map = entry.toApiListJson(lang);

                list.add(map);
            }
            long totalRecord = entryPage.getTotalElements();
            int totalPages = entryPage.getTotalPages();
            return new RestResp(Constants.REST_SUCCESS, "success", "list", list, "page", page, "size", size, "totalRecord", totalRecord, "totalPages", totalPages);
        }

        return NO_RECORD_FOUND_RESP;
    }

    @RequestMapping("/detail")
    public RestResp detail(@RequestParam String estId) {
        Entry entry = entryRepository.findByUidAndStatus(estId, Constants.STATUS_APPROVED);
        if (entry != null && !entry.isHidden()) {

            Map<String, Object> hdr = new HashMap<>();
            String[] fields = {"addr"
                    , "developer"
                    , "district"
                    /*, "firstPrintDate"*/
                    , "keyDate"
                    , "name"
                    , "phaseName"
                    , "phaseNum"
                    , "kgNetwork"
                    , "psNetwork"
                    , "ssNetwork"
                    , "region"
                    , "srpeUrl"
                    , "videoUrl"
                    , "viewDate"
                    , "websiteUrl"
                    , "mortgageUrl"
                    , "develop_project"
                    , "location"};
            for (String field : fields) {
                hdr.put(field + "_hdr", getMessage(field + "_hdr"));
            }
            int lang = Utils.localeLang(LocaleContextHolder.getLocale());
            Map<String, Object> map = entry.toApiDetailJson(lang);


            hdr.putAll(map);

            hdr.put("srpeUrl", getSrpeUrl());

            return new RestResp(Constants.REST_SUCCESS, "success", hdr);
        }
        return NO_RECORD_FOUND_RESP;
    }

    @RequestMapping("/documents")
    public RestResp documents(@RequestParam String estId, @RequestParam(value = "type", required = false) String queryType) {
        Entry entry = entryRepository.findByUidAndStatus(estId, Constants.STATUS_APPROVED);
        if (entry != null && !entry.isHidden()) {

            String[] types;
            if (queryType == null) {
                types = new String[]{Constants.DOC_SALES_BROCHURE, Constants.DOC_PRICE_LIST, Constants.DOC_SALES_ARRANGEMENT, Constants.DOC_COMPLETED_RECORD};
            } else {
                types = new String[]{queryType};
            }

            int lang = Utils.localeLang(LocaleContextHolder.getLocale());

            List<Map<String, Object>> result = new ArrayList<>();

            for (String _type : types) {

                Sort.Order nameOrder = new Sort.Order(Sort.Direction.ASC, "nameTc");
                if (lang == 1) {
                    nameOrder = new Sort.Order(Sort.Direction.ASC, "nameEn");
                }

                Sort sort = new Sort(nameOrder, new Sort.Order(Sort.Direction.DESC, "created"));

                String type = _type.toLowerCase();


                Map<String, List<EntryDoc>> subTypeMap = new HashMap<>();

                if (Constants.DOC_SALES_BROCHURE.equalsIgnoreCase(type)) {

                    List<EntryDoc> vrs = entryDocRepository.findByEntryAndType(entry, Constants.DOC_VIEW_RECORD, sort);
                    List<EntryDoc> mps = entryDocRepository.findByEntryAndType(entry, Constants.DOC_SALES_MANUAL_PART, sort);
                    List<EntryDoc> mfs = entryDocRepository.findByEntryAndType(entry, Constants.DOC_SALES_MANUAL_FULL, sort);

                    subTypeMap.put(Constants.DOC_VIEW_RECORD, vrs);
                    subTypeMap.put(Constants.DOC_SALES_MANUAL_PART, mps);
                    subTypeMap.put(Constants.DOC_SALES_MANUAL_FULL, mfs);
                } else {

                    if (type.equalsIgnoreCase(Constants.DOC_SALES_ARRANGEMENT)) {
                        sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(postDate, 0, 1)"),
                                new Sort.Order(Sort.Direction.DESC, "postDate"),
                                new Sort.Order(Sort.Direction.ASC, "NVL2(updateDate, 0, 1)"),
                                new Sort.Order(Sort.Direction.DESC, "updateDate"),
                                new Sort.Order(Sort.Direction.DESC, "created"));
                    } else if (type.equalsIgnoreCase(Constants.DOC_PRICE_LIST)) {
                        sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(printDate, 0, 1)"),
                                new Sort.Order(Sort.Direction.DESC, "printDate"),
                                new Sort.Order(Sort.Direction.DESC, "created"));
                    }

                    List<EntryDoc> docs = entryDocRepository.findByEntryAndType(entry, type, sort);

                    subTypeMap.put(type, docs);
                }

                Map<String, Object> data = new HashMap<>();
                Map<String, Object> hdr = new HashMap<>();
                String[] fields = {};

                hdr.put("date_hdr", "");

                switch (type) {
                    case Constants.DOC_SALES_BROCHURE:
                        fields = new String[]{"firstPrintDate", "viewDate"};
                        data.put("firstPrintDate", entry.getPrintDate() == null ? "" : entry.getPrintDate());
                        data.put("viewDate", entry.getViewDate() == null ? "" : entry.getViewDate());
                        break;
                    case Constants.DOC_PRICE_LIST:
                        hdr.put("date_hdr", getMessage("print_date_hdr"));
                        break;
                    case Constants.DOC_SALES_ARRANGEMENT:
                        hdr.put("date_hdr", getMessage("update_date_hdr"));
                        break;
                    case Constants.DOC_COMPLETED_RECORD:
                        break;
                }

                for (String field : fields) {
                    data.put(field + "_hdr", getMessage(field + "_hdr"));
                }
                data.put("hdr", getMessage("type_hdr_" + type));
                data.put("type_id", type.toUpperCase());
                List<EntryDocUrl> urls = entryDocUrlRepository.findByPropertyIdAndType(entry.getId(), type);
                if (!urls.isEmpty()) {
                    data.put("url", urls.get(0).getUrl());
                } else {
                    data.put("url", "");
                }
                data.put("url_hdr", getMessage("url_hdr_" + type));


                List<Map<String, Object>> subTypes = new ArrayList<>();
                for (Map.Entry<String, List<EntryDoc>> stringListEntry : subTypeMap.entrySet()) {
                    Map<String, Object> subType = new HashMap<>();
                    if (!type.equalsIgnoreCase(stringListEntry.getKey())) {
                        subType.put("type_id", stringListEntry.getKey().toUpperCase());
                        subType.put("hdr", getMessage("type_hdr_" + stringListEntry.getKey()));
                    } else {
                        subType.put("type_id", "");
                        subType.put("hdr", "");
                    }

                    List<EntryDoc> value = stringListEntry.getValue();
                    List<Map<String, Object>> docs = new ArrayList<>();

                    for (EntryDoc doc : value) {
                        Map<String, Object> map = doc.toApiListJson(lang);

                        map.put("date", null);
                        switch (type) {
                            case Constants.DOC_SALES_BROCHURE:

                                break;
                            case Constants.DOC_PRICE_LIST:
                                map.put("date", doc.getPrintDate());
                                break;
                            case Constants.DOC_SALES_ARRANGEMENT:
                                map.put("date", doc.getUpdateDate());
                                break;
                            case Constants.DOC_COMPLETED_RECORD:
                                break;
                        }
                        map.putAll(hdr);

                        docs.add(map);
                    }
                    subType.put("docs", docs);

                    subTypes.add(subType);
                }
                Collections.sort(subTypes, new Comparator<Map<String, Object>>() {
                    @Override
                    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                        String t1 = (String) o1.get("type_id");
                        String t2 = (String) o2.get("type_id");
                        Integer i1 = 999;
                        Integer i2 = 999;

                        for (int i = 0; i < sub_types.length; i++) {
                            String str = sub_types[i];
                            if (t1 != null && str.equalsIgnoreCase(t1)) {
                                i1 = i;
                            }
                            if (t2 != null && str.equalsIgnoreCase(t2)) {
                                i2 = i;
                            }
                        }


                        return i1.compareTo(i2);
                    }
                });
                data.put("sub_type", subTypes);


                result.add(data);
            }


            return new RestResp(Constants.REST_SUCCESS, "success", result);
        }
        return NO_RECORD_FOUND_RESP;
    }

    @RequestMapping("/district")
    public RestResp district(@RequestParam String region) {

        List<District> districts = (List<District>) Cache.get("districts");
        for (District district : districts) {
            if (region.equalsIgnoreCase(district.getUrlKey())) {
                int lang = Utils.localeLang(LocaleContextHolder.getLocale());
                List<Map<String, String>> data = new ArrayList<>();
                for (Area area : district.getAreas()) {
                    Map<String, String> map = new HashMap<>();
                    String value = Utils.localeText(area.getNameTc(), area.getNameEn(), area.getNameSc(), lang);
                    map.put("district", value);
                    data.add(map);
                }
                return new RestResp(Constants.REST_SUCCESS, "success", data);
            }
        }
        return NO_RECORD_FOUND_RESP;
    }
}
