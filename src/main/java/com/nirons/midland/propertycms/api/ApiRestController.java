package com.nirons.midland.propertycms.api;

import com.nirons.midland.propertycms.controller.BaseController;
import com.nirons.midland.propertycms.dao.DistrictDao;
import com.nirons.midland.propertycms.entity.*;
import com.nirons.midland.propertycms.model.RestResp;
import com.nirons.midland.propertycms.repository.DeveloperRepository;
import com.nirons.midland.propertycms.repository.DistrictRepository;
import com.nirons.midland.propertycms.repository.EntryDocRepository;
import com.nirons.midland.propertycms.repository.EntryRepository;
import com.nirons.midland.propertycms.util.Cache;
import com.nirons.midland.propertycms.util.Chinese;
import com.nirons.midland.propertycms.util.Constants;
import com.nirons.midland.propertycms.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by jaquesyang on 15/8/4.
 */
@RestController
@RequestMapping("/api")
@SuppressWarnings("unchecked")
public class ApiRestController extends BaseController {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    DistrictDao districtDao;

    @Autowired
    EntryRepository entryRepository;

    @Autowired
    EntryDocRepository entryDocRepository;

    @Autowired
    DeveloperRepository developerRepository;

    @RequestMapping("/districts")
    public RestResp districts() {
        //List<District> districts = districtDao.districtWithAreaList();
        List<District> districts = (List<District>) Cache.get("districts");

        if (districts != null) {
            for (District district : districts) {
                for (Area area : district.getAreas()) {
                    if (StringUtils.isEmpty(area.getNameEn())) {
                        area.setNameEn(area.getNameTc());
                    }
                    if (StringUtils.isEmpty(area.getNameSc())) {
                        area.setNameSc(area.getNameTc());
                    }
                }
            }
            return new RestResp(Constants.REST_SUCCESS, "", districts);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }


    @RequestMapping("/developers")
    public RestResp developers() {
        //List<String> developers = entryRepository.developers();
        List<Developer> developers = (List<Developer>) Cache.get("developers");
        if (developers != null) {

            return new RestResp(Constants.REST_SUCCESS, "", developers);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }


    @RequestMapping("/properties")
    public RestResp properties(@RequestParam(required = false, defaultValue = "") String searchname,
                               /*@RequestParam(required = false, defaultValue = "") String hkp,*/
                               @PageableDefault(sort = {"ordering", "nameTc"}) Pageable pageable) {
        //List<Entry> entries = entryRepository.findApprovedByName(nameParam(searchname));
        String paramName = nameParam(Chinese.toTc(searchname));
        logger.debug("Search name: " + paramName);
        Page<Entry> entries;
        if (/*"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)*/true) {
            entries = entryRepository.findHkpApprovedByName(paramName, pageable);
            /*
            for (Entry entry : entries) {
                UploadFile file = entry.getUploadFile();
                if (file != null) {
                    entry.getUploadFile().setHkp(true);
                }
            }
            */
        } else {
            entries = entryRepository.findApprovedByName(paramName, pageable);
        }
        if (entries != null) {
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }


    @RequestMapping("/properties/{uid}")
    public RestResp propertyDetails(@PathVariable String uid/*,
                                    @RequestParam(required = false, defaultValue = "") String hkp*/) {
        Entry entry = null;
        List<Entry> entries = entryRepository.findApprovedByUrlKey(uid);
        if (!entries.isEmpty()) {
            entry = entries.get(0);
        }
        boolean forHkp = true;//"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp);

        if (entry == null
                || !Constants.STATUS_APPROVED.equalsIgnoreCase(entry.getStatus())
                || (forHkp ? entry.isHiddenForHkp() : entry.isHidden())) {
            return NO_RECORD_FOUND_RESP;
        }

        if (forHkp) {
            /*
            UploadFile file = entry.getUploadFile();
            if (file != null) {
                entry.getUploadFile().setHkp(true);
            }
            */
        }
        return new RestResp(Constants.REST_SUCCESS, "", entry);

    }


    @RequestMapping("/hot/properties")
    public RestResp hotProperties(
            /*@RequestParam(required = false, defaultValue = "") String hkp,*/
            @PageableDefault(sort = {"ordering", "nameTc"}) Pageable pageable) {
        //List<Entry> entries = entryRepository.findApprovedHot();
        /*
        if (!pageable.getSort().iterator().hasNext()) {
            pageable.getSort().and(new Sort(Sort.Direction.ASC, "ordering", "nameTc"));
        }
        */

        Page<Entry> entries;
        if (/*"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)*/true) {
            entries = entryRepository.findHkpApprovedHot(pageable);
            /*
            for (Entry entry : entries) {
                UploadFile file = entry.getUploadFile();
                if (file != null) {
                    entry.getUploadFile().setHkp(true);
                }
            }*/
        } else {
            entries = entryRepository.findApprovedHot(pageable);
        }
        if (entries != null) {
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }

    @RequestMapping("/districts/{district}/properties")
    public RestResp districtProperties(@PathVariable Long district/*,
                                       @RequestParam(required = false, defaultValue = "") String hkp*/,
                                       @PageableDefault(sort = {"ordering", "nameTc"}) Pageable pageable) {
        //List<Entry> entries = entryRepository.findApprovedByDistrict(district);
        Page<Entry> entries;
        if (/*"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)*/true) {
            entries = entryRepository.findHkpApprovedByDistrict(district, pageable);
            /*
            for (Entry entry : entries) {
                UploadFile file = entry.getUploadFile();
                if (file != null) {
                    entry.getUploadFile().setHkp(true);
                }
            }*/
        } else {
            entries = entryRepository.findApprovedByDistrict(district, pageable);
        }
        if (entries != null) {
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }

    @RequestMapping("/developers/{developer}/properties")
    public RestResp developerProperties(@PathVariable Long developer/*,
                                        @RequestParam(required = false, defaultValue = "") String hkp*/,
                                        @PageableDefault(sort = {"ordering", "nameTc"}) Pageable pageable) {
        //List<Entry> entries = entryRepository.findApprovedByDeveloper(developer);
        Page<Entry> entries;
        if (/*"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)*/true) {
            entries =
                    entryRepository.findHkpApprovedByDeveloper(developer, pageable);
            /*
            for (Entry entry : entries) {
                UploadFile file = entry.getUploadFile();
                if (file != null) {
                    entry.getUploadFile().setHkp(true);
                }
            }
            */
        } else {
            entries =
                    entryRepository.findApprovedByDeveloper(developer, pageable);
        }
        if (entries != null) {
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }


    @RequestMapping("/properties/{entryId}/documents")
    public RestResp propertyDocuments(@PathVariable String entryId/*,
                                      @RequestParam(required = false, defaultValue = "") String hkp*/) {
        Entry entry = entryRepository.findByUidAndStatus(entryId, Constants.STATUS_APPROVED);
        if (entry == null) {
            return NO_RECORD_FOUND_RESP;
        }
        List<EntryDoc> entries = entryDocRepository.findByEntry(entry);
        if (entries != null) {
            /*
            if ("1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)) {
                for (EntryDoc entryDoc : entries) {
                    if (entryDoc.getFile() != null) {
                        entryDoc.getFile().setHkp(true);
                    }
                }
            }*/
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }


    @RequestMapping("/properties/{entryId}/documents/{type}")
    public RestResp propertyDocumentsByType(@PathVariable String entryId,
                                            @PathVariable String type/*,
                                            @RequestParam(required = false, defaultValue = "") String hkp*/) {
        Entry entry = entryRepository.findByUidAndStatus(entryId, Constants.STATUS_APPROVED);
        if (entry == null) {
            return NO_RECORD_FOUND_RESP;
        }

        Sort.Order nameOrder = new Sort.Order(Sort.Direction.ASC, "nameTc");
        if (Utils.localeLang(getLocale()) == 1) {
            nameOrder = new Sort.Order(Sort.Direction.ASC, "nameEn");
        }

        Sort sort = new Sort(nameOrder, new Sort.Order(Sort.Direction.DESC, "created"));

        if (type.equalsIgnoreCase(Constants.DOC_SALES_ARRANGEMENT)) {
            sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(postDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "postDate"),
                    new Sort.Order(Sort.Direction.ASC, "NVL2(updateDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "updateDate"),
                    new Sort.Order(Sort.Direction.DESC, "created"));
        } else if (type.equalsIgnoreCase(Constants.DOC_PRICE_LIST)) {
            sort = new Sort(new Sort.Order(Sort.Direction.ASC, "NVL2(printDate, 0, 1)"),
                    new Sort.Order(Sort.Direction.DESC, "printDate"),
                    new Sort.Order(Sort.Direction.DESC, "created"));
        }

        List<EntryDoc> entries = entryDocRepository.findByEntryAndType(entry, type, sort);
        if (entries != null) {
            /*
            if ("1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)) {
                for (EntryDoc entryDoc : entries) {
                    if (entryDoc.getFile() != null) {
                        entryDoc.getFile().setHkp(true);
                    }
                }
            }
            */
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }

    @RequestMapping("/districts/{district}/areas/{area}/properties")
    public RestResp districtAreaProperties(@PathVariable Long district,
                                           @PathVariable String area/*,
                                           @RequestParam(required = false, defaultValue = "") String hkp*/,
                                           @PageableDefault(sort = {"ordering", "nameTc"}) Pageable pageable) {
        //List<Entry> entries = entryRepository.findApprovedByDistrict(district);
        String areaParam = Chinese.toTc(area);
        logger.debug("area: {}", areaParam);
        Page<Entry> entries;
        if (/*"1".equalsIgnoreCase(hkp) || "true".equalsIgnoreCase(hkp)*/true) {
            entries = entryRepository.findHkpApprovedByDistrictAndArea(district, areaParam, pageable);
            /*
            for (Entry entry : entries) {
                UploadFile file = entry.getUploadFile();
                if (file != null) {
                    entry.getUploadFile().setHkp(true);
                }
            }*/
        } else {
            entries = entryRepository.findApprovedByDistrictAndArea(district, areaParam, pageable);
        }
        if (entries != null) {
            return new RestResp(Constants.REST_SUCCESS, "", entries);
        } else {
            return NO_RECORD_FOUND_RESP;
        }
    }

}
