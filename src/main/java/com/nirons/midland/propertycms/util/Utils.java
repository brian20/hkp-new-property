package com.nirons.midland.propertycms.util;

import com.nirons.midland.propertycms.util.Chinese;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by jaquesyang on 15/6/25.
 */
public final class Utils {
    protected static Logger logger = LoggerFactory.getLogger(Utils.class);

    public static final String bytesToSize(long bytes) {
        if (bytes == 0) {
            return "0 B";
        }
        int k = 1024;
        String[] sizes = {"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
        int i = (int) (Math.floor(Math.log(bytes) / Math.log(k)));
        return new DecimalFormat("0.00").format(bytes / Math.pow(k, i)) + " " + sizes[i];
    }

    public static String genRandomString(int length) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < length; i++) {
            buf.append(Constants.CHARACTERS.charAt((int) (Math.random() * Constants.CHARACTERS
                    .length())));
        }
        return buf.toString();
    }

    public static String genRandomNumber(int length) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < length; i++) {
            buf.append((int) (Math.random() * 10));
        }
        return buf.toString();
    }

    public static <E extends Object> boolean memberOf(E e, E[] array) {
        if (array == null) {
            return false;
        }
        for (E o : array) {
            if (o.equals(e)) {
                return true;
            }
        }
        return false;
    }


    public static String orderByString(String table, String[] ordering,
                                       String[] sorting, String... defaultOrdering) {
        StringBuffer buf = new StringBuffer();
        String order = "", sort = "";
        if (ordering == null || ordering.length == 0) {
            try {
                order = defaultOrdering[0];
            } catch (Exception e) {
            }
            try {
                sort = defaultOrdering[1];
            } catch (Exception e) {
            }
            if (!(sort.equalsIgnoreCase("")
                    || sort.equalsIgnoreCase(Constants.DESC) || sort
                    .equalsIgnoreCase(Constants.ASC))) {
                sort = Constants.ASC;
            }
            buf.append(table);
            buf.append(".");
            buf.append(order);
            buf.append(" ");
            buf.append(sort);
        } else {
            for (int i = 0; i < ordering.length; i++) {
                try {
                    sort = sorting[i];
                } catch (Exception e) {
                    sort = Constants.ASC;
                }
                if (!(sort.equalsIgnoreCase("")
                        || sort.equalsIgnoreCase(Constants.DESC) || sort
                        .equalsIgnoreCase(Constants.ASC))) {
                    sort = Constants.ASC;
                }
                buf.append(table);
                buf.append(".");
                buf.append(ordering[i]);
                buf.append(" ");
                buf.append(sort);
                if (i < ordering.length - 1) {
                    buf.append(",");
                }
            }
        }
        if (buf.length() > 0) {
            buf.insert(0, " ORDER BY ");
        }
        return buf.toString();
    }

    public static String orderByString(String[] ordering, String[] sorting,
                                       String... defaultOrdering) {
        return orderByString("e", ordering, sorting, defaultOrdering);
    }

    public static void loggingError(Logger _logger, Object... messages) {
        if (messages == null) {
            return;
        }
        for (Object msg : messages) {
            if (msg != null && msg.getClass().isArray()) {
                try {
                    for (Object o : ((Object[]) msg)) {
                        _logger.error(o.toString());
                    }
                } catch (Exception e) {
                    _logger.error(((Object[]) msg).toString());
                }
            } else {
                _logger.error(msg.toString());
            }
        }
    }

    public static void loggingInfo(Logger _logger, Object... messages) {
        if (messages == null) {
            return;
        }
        for (Object msg : messages) {
            if (msg != null && msg.getClass().isArray()) {
                try {
                    for (Object o : ((Object[]) msg)) {
                        _logger.info(o.toString());
                    }
                } catch (Exception e) {
                    _logger.info(((Object[]) msg).toString());
                }
            } else {
                _logger.info(msg.toString());
            }
        }

    }

    public static void loggingWarn(Logger _logger, Object... messages) {
        if (messages == null) {
            return;
        }
        for (Object msg : messages) {
            if (msg != null && msg.getClass().isArray()) {
                try {
                    for (Object o : ((Object[]) msg)) {
                        _logger.warn(o.toString());
                    }
                } catch (Exception e) {
                    _logger.warn(((Object[]) msg).toString());
                }
            } else {
                _logger.warn(msg.toString());
            }
        }

    }

    public static void loggingError(Object... messages) {
        loggingError(logger, messages);
    }

    public static void loggingInfo(Object... messages) {
        loggingInfo(logger, messages);
    }

    public static void loggerWarn(Object... messages) {
        loggingWarn(logger, messages);
    }

    @SuppressWarnings("unchecked")
    public static <E extends Object> E findByUniqueField(EntityManager em,
                                                         Class<E> entityClass, Object... fieldAndValues) {
        StringBuffer buf = new StringBuffer("SELECT e FROM ");
        buf.append(entityClass.getSimpleName());
        buf.append(" e WHERE 1=1");
        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                buf.append(" AND e.");
                buf.append(fieldAndValues[i]);
                buf.append("= :");
                buf.append(toSafeQueryParameter((String) fieldAndValues[i]));
            }
        }
        Query query = em.createQuery(buf.toString());

        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                query.setParameter(
                        toSafeQueryParameter((String) fieldAndValues[i]),
                        fieldAndValues[i + 1]);
            }
        }
        query.setMaxResults(1);
        try {
            E entity = (E) query.getSingleResult();
            return entity;
        } catch (Exception e) {
            loggingError(e.getMessage(), buf.toString(), entityClass,
                    fieldAndValues);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static <E extends Object> List<E> findByField(EntityManager em,
                                                         Class<E> entityClass, Object... fieldAndValues) {
        StringBuffer buf = new StringBuffer("SELECT e FROM ");
        buf.append(entityClass.getSimpleName());
        buf.append(" e WHERE 1=1");
        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                buf.append(" AND e.");
                buf.append(fieldAndValues[i]);
                buf.append("= :");
                buf.append(toSafeQueryParameter((String) fieldAndValues[i]));
            }
        }
        Query query = em.createQuery(buf.toString());

        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                query.setParameter(
                        toSafeQueryParameter((String) fieldAndValues[i]),
                        fieldAndValues[i + 1]);
            }
        }
        try {
            return query.getResultList();
        } catch (Exception e) {
            loggingError(e.getMessage(), buf.toString(), entityClass,
                    fieldAndValues);
            return null;
        }
    }

    public static <E extends Object> int findCountByField(EntityManager em,
                                                          Class<E> entityClass, Object... fieldAndValues) {
        StringBuffer buf = new StringBuffer("SELECT COUNT(e) FROM ");
        buf.append(entityClass.getSimpleName());
        buf.append(" e WHERE 1=1");
        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                buf.append(" AND e.");
                buf.append(fieldAndValues[i]);
                buf.append("= :");
                buf.append(toSafeQueryParameter((String) fieldAndValues[i]));
            }
        }
        logger.debug(buf.toString());
        Query query = em.createQuery(buf.toString());

        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                query.setParameter(
                        toSafeQueryParameter((String) fieldAndValues[i]),
                        fieldAndValues[i + 1]);
            }
        }
        try {
            return getQueryCount(query);
        } catch (Exception e) {
            loggingError(e.getMessage(), buf.toString(), entityClass,
                    fieldAndValues);
            return 0;
        }
    }

    public static String toSafeQueryParameter(String param) {
        if (param.contains(".") || param.contains("!") || param.contains(" ")) {
            return param.replace(".", "_").replace("!", "").replace(" ", "");
        }
        return param;
    }

    public static <E extends Object> int deleteByFields(EntityManager em,
                                                        Class<E> entityClass, Object... fieldAndValues) {
        StringBuffer buf = new StringBuffer("DELETE FROM ");
        buf.append(entityClass.getSimpleName());
        buf.append(" e WHERE 1=1");
        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                buf.append(" AND e.");
                buf.append(fieldAndValues[i]);
                buf.append("= :");
                buf.append(toSafeQueryParameter((String) fieldAndValues[i]));
            }
        }
        Query query = em.createQuery(buf.toString());

        if (fieldAndValues != null) {
            for (int i = 0; i < fieldAndValues.length - 1; i += 2) {
                query.setParameter(
                        toSafeQueryParameter((String) fieldAndValues[i]),
                        fieldAndValues[i + 1]);
            }
        }
        try {
            return query.executeUpdate();
        } catch (Exception e) {
            loggingError(e.getMessage(), buf.toString(), entityClass,
                    fieldAndValues);
            return -1;
        }
    }

    public static void setQueryResultRange(Query query, int firstResult,
                                           int maxResult) {
        if (firstResult > 0) {
            query.setFirstResult(firstResult);
        }
        if (maxResult > 0) {
            query.setMaxResults(maxResult);
        }
    }

    public static int getQueryCount(Query query) {
        try {
            query.setMaxResults(1);
            Number n = (Number) query.getSingleResult();
            if (n == null) {
                return 0;
            } else {
                return n.intValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return 0;
        }
    }

    public static Long getAggregateCount(Query query) {
        try {
            query.setMaxResults(1);
            Number n = ((Number) query.getSingleResult());
            if (n != null) {
                return n.longValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static Object getSingleResult(Query query) {
        try {
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (NoResultException e) {
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return null;
    }

    public static String getArrayParamsQueryString(Object[] array,
                                                   String table, String field) {
        return getArrayParamsQueryString(array, table, field, field);
    }

    public static String getArrayParamsQueryString(Object[] array,
                                                   String table, String field, String param) {
        if (array != null && array.length > 0) {
            StringBuffer buf = new StringBuffer(" ");
            buf.append(table);
            buf.append(".");
            buf.append(field);
            buf.append(" IN (");
            for (int i = 0; i < array.length; i++) {
                Object value = array[i];
                if (value != null) {
                    buf.append(" :");
                    buf.append(table);
                    buf.append(param);
                    buf.append(i);
                }
                if (i < array.length - 1) {
                    buf.append(", ");
                }
            }
            buf.append(")");
            return buf.toString();
        }
        return "";
    }

    public static String getArrayParamsQueryStringADDNull(Object[] array,
                                                          String table, String field) {
        if (array != null && array.length > 0) {
            StringBuffer buf = new StringBuffer(" (");
            buf.append(table);
            buf.append(".");
            buf.append(field);
            buf.append(" IS NULL ");
            buf.append(" OR ");
            for (int i = 0; i < array.length; i++) {
                Object value = array[i];
                buf.append(table);
                buf.append(".");
                buf.append(field);
                if (value != null) {
                    buf.append(" = :");
                    buf.append(table);
                    buf.append(field);
                    buf.append(i);
                } else {
                    buf.append(" IS NULL ");
                }
                if (i < array.length - 1) {
                    buf.append(" OR ");
                }
            }
            buf.append(")");
            return buf.toString();
        }
        return "";
    }

    public static void setArrayParamsQuery(Query query, Object[] array,
                                           String table, String field) {
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                Object value = array[i];
                if (value != null) {
                    query.setParameter(table + field + i, array[i]);
                }
            }
        }
    }

    public static String md5(String source) {
        return md5(source.getBytes());
    }

    public static String md5(byte[] source) {
        String s = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            java.security.MessageDigest md = java.security.MessageDigest
                    .getInstance("MD5");
            md.update(source);
            byte tmp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {

                byte byte0 = tmp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];

                str[k++] = hexDigits[byte0 & 0xf];
            }
            s = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public static <E extends Object> boolean equals(E obj1, E obj2) {
        if (obj1 == obj2) {
            return true;
        }
        if (obj1 == null || obj2 == null) {
            return false;
        }

        return obj1.equals(obj2);
    }

    public static int getDateField(Date date, int field) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(date);
        return cale.get(field);
    }

    public static int getDayOfMonth(Date date) {
        return getDateField(date, Calendar.DAY_OF_MONTH);
    }

    public static int getDayOfWeek(Date date) {
        return getDateField(date, Calendar.DAY_OF_WEEK);
    }

    public static int getMonth(Date date) {
        return getDateField(date, Calendar.MONTH);
    }

    public static int getYear(Date date) {
        return getDateField(date, Calendar.YEAR);
    }

    public static int getHourOfDay(Date date) {
        return getDateField(date, Calendar.HOUR_OF_DAY);
    }

    public static long dateCompare(Date date1, Date date2, String pattern) {
        if (date1 == date2) {
            return 0;
        }
        if (date1 == null || date2 == null) {
            return -1;
        }
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return Long.parseLong(df.format(date1))
                - Long.parseLong(df.format(date2));
    }

    public static long dateTimeCompare(Date date1, Date date2) {
        return dateCompare(date1, date2, "yyyyMMddHHmmss");
    }

    public static long dateCompare(Date date1, Date date2) {
        return dateCompare(date1, date2, "yyyyMMdd");
    }

    public static boolean dateBetween(Date date, Date dateFrom, Date dateTo) {
        return (dateFrom == null || dateCompare(date, dateFrom) >= 0)
                && (dateTo == null || dateCompare(date, dateTo) <= 0);
    }

    public static boolean dateTimeBetween(Date date, Date dateFrom, Date dateTo) {
        return dateBetween(date, dateFrom, dateTo, "yyyyMMddHHmmss");
    }

    public static boolean dateBetween(Date date, Date dateFrom, Date dateTo,
                                      String pattern) {
        return (dateFrom == null || dateCompare(date, dateFrom, pattern) >= 0)
                && (dateTo == null || dateCompare(date, dateTo, pattern) <= 0);
    }

    public static Date dateAdd(Date oriDate, int year, int month, int date) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(oriDate);
        cale.add(Calendar.YEAR, year);
        cale.add(Calendar.MONTH, month);
        cale.add(Calendar.DATE, date);

        return cale.getTime();
    }

    public static Date timeAdd(Date oriDate, int hour, int minute, int second) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(oriDate);
        cale.add(Calendar.HOUR, hour);
        cale.add(Calendar.MINUTE, minute);
        cale.add(Calendar.SECOND, second);

        return cale.getTime();
    }

    public static Date lastDayOfMonth(Date date) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(date);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        cale.add(Calendar.MONTH, 1);
        cale.add(Calendar.DATE, -1);

        return cale.getTime();
    }

    public static Date firstDayOfMonth(Date date) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(date);
        cale.set(Calendar.DAY_OF_MONTH, 1);

        return cale.getTime();
    }

    public static String getEventId(String prefix) {
        String randomNumber = new DecimalFormat("00")
                .format(Math.random() * 99);
        String timestamp = new SimpleDateFormat("yyMMddHHmmss")
                .format(new Date());
        return prefix + timestamp + randomNumber;
    }


    public static String genCsvCell(Number in) {
        return in.toString();
    }

    public static String genCsvCell(String in) {
        if (in == null) {
            return "";
        }
        String out = in;
        boolean quot = false;
        if (in.contains(",")) {
            quot = true;
        }
        if (in.contains("\r")) {
            quot = true;
        }
        if (in.contains("\n")) {
            quot = true;
        }
        if (in.contains("\"")) {
            quot = true;
            out = out.replaceAll("\"", "\"\"");
        }
        if (in.contains(" ")) {
            // quot = true;
        }
        if (quot) {
            out = "\"" + out + "\"";
        }
        return out;
    }

    public static String getRemoteAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    public static String getServerName(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-server");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getServerName();
        }
        return ip;
    }

    public static long diff(Date date1, Date date2, int field) {
        long d1 = date1 == null ? 0 : date1.getTime();
        long d2 = date2 == null ? 0 : date2.getTime();
        int n = 1;
        Calendar cale1;
        Calendar cale2;
        switch (field) {
            case Calendar.YEAR:
                cale1 = Calendar.getInstance();
                cale1.setTime(date1);
                cale2 = Calendar.getInstance();
                cale2.setTime(date2);

                return cale1.get(Calendar.YEAR) - cale2.get(Calendar.YEAR);
            case Calendar.MONTH:
                cale1 = Calendar.getInstance();
                cale1.setTime(date1);
                cale2 = Calendar.getInstance();
                cale2.setTime(date2);
                int y1 = cale1.get(Calendar.YEAR);
                int m1 = cale1.get(Calendar.MONTH);
                int y2 = cale2.get(Calendar.YEAR);
                int m2 = cale2.get(Calendar.MONTH);

                return (y1 - y2) * 12 + m1 - m2;
            case Calendar.DATE:
                n = 24 * 3600 * 1000;
                break;
            case Calendar.HOUR:
                n = 3600 * 1000;
                break;

            case Calendar.MINUTE:
                n = 60 * 1000;
                break;
            case Calendar.SECOND:
                n = 1000;
                break;
            case Calendar.MILLISECOND:
                n = 1;
                break;
            default:
                throw new IllegalArgumentException();
        }

        return (d1 - d2) / n;
    }

    public static <E extends Object> void syncCollection(Collection<E> list1,
                                                         Collection<E> list2) {
        List<E> listtem = new ArrayList<E>();
        for (E e : list2) {
            listtem.add(e);
        }

        for (E e : list1) {
            if (!list2.contains(e)) {
                list2.add(e);
            }
        }

        for (E e : listtem) {
            if (!list1.contains(e)) {
                list2.remove(e);
            }
        }
        listtem.clear();
    }

    public static <E extends Object> boolean contains(Collection<E> c1,
                                                      Collection<E> c2) {
        if (c1.size() >= c2.size()) {
            for (E e : c2) {
                if (Collections.frequency(c1, e) < Collections.frequency(c2, e)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static <E extends Object> void remove(List<E> c1, List<E> c2) {
        for (E e : c2) {
            c1.remove(e);
        }
    }

    public static <E extends Object> List<Integer> index(List<E> c1, List<E> c2) {
        List<E> tmp = new ArrayList<E>(c1);
        List<Integer> idx = new ArrayList<Integer>();
        for (E e : c2) {
            if (tmp.contains(e)) {
                int i = tmp.indexOf(e);
                idx.add(i);
                tmp.set(i, null);
            }
        }
        tmp.clear();

        return idx;
    }

    public static <E extends Object> void permutation(List<List<E>> list,
                                                      int mass, List<E> data, List<E> target) {
        if (target.size() == mass) {
            list.add(target);
            return;
        }
        for (int i = 0; i < data.size(); i++) {
            List<E> newD = new ArrayList<E>(data);
            List<E> newT = new ArrayList<E>(target);
            newT.add(newD.get(i));
            newD.remove(i);
            permutation(list, mass, newD, newT);
        }
    }

    public static <E extends Object> void removeIndex(List<E> c1,
                                                      List<Integer> index) {
        for (Integer i : index) {
            c1.set(i, null);
        }
        c1.removeAll(Collections.singleton(null));
    }

    public static <E extends Object> List<E> array2ArrayList(E[] array) {
        return new ArrayList<E>(Arrays.asList(array));
    }

    public static <E extends Object> List<E> subList(List<E> c1,
                                                     List<Integer> index) {
        List<E> sub = new ArrayList<E>();
        for (Integer i : index) {
            sub.add(c1.get(i));
        }

        return sub;
    }

    public static int maxValueIndex(List<Number> list) {
        Number max = list.get(0);
        int idx = 0;
        for (int i = 1; i < list.size(); i++) {
            if (max.doubleValue() < list.get(i).doubleValue()) {
                idx = i;
            }
        }
        return idx;
    }


    public static String escapeXML(String input) {
        if (input == null) {
            return null;
        }
        return input.replace("&", "&amp;").replace(">", "&gt;")
                .replace("<", "&lt;").replace("'", "&apos;")
                .replaceAll("\"", "&quot;");
    }

    public static String replace(String str, String tag, String rep) {
        if (str != null && tag != null && rep != null) {
            return str.replace(tag, rep);
        }
        return str;
    }

    public static String charPrefix(String str, char c, int len) {
        if (str != null && str.length() < len) {
            StringBuilder sb = new StringBuilder(str);
            for (int i = 0; i < len - str.length(); i++) {
                sb.insert(0, c);
            }
            return sb.toString();
        }
        return str;
    }

    public static String zeroPrefix(String str, int len) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        return charPrefix(str, '0', len);
    }

    public static void deleteOldFile(String fp, String newFile, String oldFile) {
        try {
            if (!equals(newFile, oldFile) && oldFile != null
                    && !oldFile.isEmpty()) {
                File file = new File(fp + File.separator + oldFile);
                logger.debug("Delete Old File: " + file.toString());
                if (file.exists()) {
                    file.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteOldFile(String newFile, String oldFile) {
        deleteOldFile(Constants.STATIC_FILE_STORE_DIR, newFile, oldFile);
    }

    public static String array2String(String[] array) {
        if (array != null && array.length != 0) {
            StringBuffer sb = new StringBuffer();
            for (String s : array) {
                sb.append("," + s);
            }
            if (sb.length() != 0)
                sb.deleteCharAt(0);
            return sb.toString();
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public static Object[] getObjectArray(String[] ss, Class c) {
        Object[] objs = null;
        try {
            if (c == Boolean.class) {
                objs = new Boolean[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    if (ss[i].equals("1"))
                        objs[i] = true;
                    else
                        objs[i] = false;
                }
            } else if (c == Integer.class) {
                objs = new Integer[ss.length];
                for (int i = 0; i < ss.length; i++) {
                    objs[i] = Integer.parseInt(ss[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objs;
    }

    public static String getSQLSafedSingleQuotedValue(String value) {
        if (value == null) {
            return "";
        }
        return value.replaceAll("\\\\", "\\\\\\\\").replaceAll("'", "\\\\'");
    }

    public static String jsonSafe(String in) {
        if (in == null) {
            return null;
        }
        return in.replace("\t", "\\t").replace("\u0003", "");
    }


    public static boolean checkEmailFormat(String email) {
        boolean check = false;
        if (email == null) {
            return false;
        }
        if (email.length() == 0) {
            return false;
        }

        String checkEmail = "^([^@ \"]+@[^@\\. \"]+(\\.[^@\\. \"]+)+)?$";
        Pattern pattern = Pattern.compile(checkEmail);
        Matcher emailMatch = null;
        emailMatch = pattern.matcher(email);
        if (emailMatch.matches()) {
            check = true;
        }

        return check;
    }

    public static Map<String, String> validKeyMap = new HashMap<String, String>();

    public static String genNewValidKey(String platform, String deviceId) {
        String newValidKeyString = genRandomString(16);
        validKeyMap.put(platform + "_" + deviceId, newValidKeyString);

        return newValidKeyString;
    }

    public static boolean validKey(String platform, String deviceId, String key) {
        try {
            String str = validKeyMap.get(platform + "_" + deviceId);
            return str.substring(0, 1).equalsIgnoreCase(key.substring(0, 1))
                    && str.substring(5, 6)
                    .equalsIgnoreCase(key.substring(1, 2))
                    && str.substring(10, 11).equalsIgnoreCase(
                    key.substring(2, 3))
                    && str.substring(15, 16).equalsIgnoreCase(
                    key.substring(3, 4));
        } catch (Exception e) {
        }
        return false;
    }


    public static String safeEmail(String email) {
        if (email == null || email.isEmpty() || !email.contains("@")) {
            return email;
        }
        String[] strs = email.split("@");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strs.length; i++) {
            if (i == 1) {
                sb.append("@");
            }
            String string = strs[i];
            if (string.length() <= 4) {
                sb.append("****");
            } else if (i == 0) {
                sb.append(string.substring(0, string.length() - 4));
                sb.append("****");
            } else {
                sb.append("****");
                sb.append(string.substring(4));
            }
        }

        return sb.toString();
    }

    public static String getImageURL(String filename) {
        if (filename == null || filename.isEmpty()
                || filename.toLowerCase().startsWith("http")) {
            return filename;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.STATIC_FILE_SERVER);
        sb.append("/");
        sb.append(filename.replace('\\', '/'));

        return sb.toString();
    }

    public static final SimpleDateFormat yyMMddHHmmssSSS = new SimpleDateFormat(
            "yyMMddHHmmssSSS", Locale.ENGLISH);

    private static final SimpleDateFormat MMdd = new SimpleDateFormat("MMdd",
            Locale.ENGLISH);


    public static String writeFile(String filename, byte[] data, String folder) {
        try {
            String filepath = Utils.imageFilePath(folder, filename);
            File file = new File(Constants.STATIC_FILE_STORE_DIR + File.separator
                    + filepath);
            Utils.writeFile(data, file.getPath());

            return filepath;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeFile(byte[] b, String outputFile) {
        BufferedOutputStream stream = null;
        File file = null;

        try {
            file = new File(outputFile);
            FileOutputStream fstream = new FileOutputStream(file);
            stream = new BufferedOutputStream(fstream);
            stream.write(b);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                }
            }
        }

    }

    public static String writeFile(Part part, String folder) {
        try {
            String filename = Utils.getFileName(part);
            String filepath = Utils.imageFilePath(folder, filename);
            File file = new File(Constants.STATIC_FILE_STORE_DIR + File.separator
                    + filepath);
            part.write(file.getPath());

            return filepath;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String imageFilePath(String photoRoot, String folder,
                                       Date date, String filename) {
        StringBuilder sb = new StringBuilder();
        if (folder != null && !folder.isEmpty()) {
            sb.append(File.separator);
            sb.append(folder);
        }
        if (date != null) {
            sb.append(File.separator);
            sb.append(getYear(date));
            sb.append(File.separator);
            sb.append(MMdd.format(date));
            sb.append(File.separator);
        }
        String refPath = sb.toString();
        sb.insert(0, photoRoot);
        File dir = new File(sb.toString());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = null;
        do {

            String stamp = yyMMddHHmmssSSS.format(date) + genRandomString(4);
            String fileExt = "";
            try {
                fileExt = filename.substring(filename.lastIndexOf("."),
                        filename.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            String name = stamp + fileExt;
            file = new File(dir.getPath(), name);
        } while (file.exists());

        // logger.debug(photoRoot);
        // logger.debug(file.getPath());

        return refPath + file.getName();

    }

    public static String imageFilePath(String folder, Date date, String filename) {
        return imageFilePath(Constants.STATIC_FILE_STORE_DIR, folder, date,
                filename);
    }

    public static String imageFilePath(String photoRoot, String folder,
                                       String filename) {
        return imageFilePath(photoRoot, folder, new Date(), filename);
    }

    public static String imageFilePath(String folder, String filename) {
        return imageFilePath(Constants.STATIC_FILE_STORE_DIR, folder, filename);
    }

    public static String getFileName(Part part) {
        String cotentDesc = part.getHeader("content-disposition");
        String fileName = null;
        Pattern pattern = Pattern.compile("filename=\".+\"");
        Matcher matcher = pattern.matcher(cotentDesc);
        if (matcher.find()) {
            fileName = matcher.group();
            fileName = fileName.substring(10, fileName.length() - 1);
        }
        return fileName;
    }

    public static void createFolder(String Path) throws IOException {
        File file = new File(Path.endsWith("/") ? Path.substring(0,
                Path.length() - 1) : Path);
        if (!file.exists())
            file.mkdir();
    }

    public static String thumbnailImageFilePath(String imageFilePath) {
        String prefix = imageFilePath;
        String ext = "";
        int idx = imageFilePath.lastIndexOf(".");
        if (idx > -1) {
            prefix = imageFilePath.substring(0, idx);
            ext = imageFilePath.substring(idx);
        }
        return prefix + "_t" + ext;
    }

    /*
    public static String convertThumbnail(String fullImageFilePath, int width) {
        if (fullImageFilePath == null || fullImageFilePath.isEmpty()) {
            return null;
        }
        try {
            String thumbnailImageFilePath = thumbnailImageFilePath(fullImageFilePath);

            File fullImageFile = new File(Constants.STATIC_FILE_STORE_DIR
                    + File.separator + fullImageFilePath);

            File thumbnailFile = new File(Constants.STATIC_FILE_STORE_DIR
                    + File.separator + thumbnailImageFilePath);

            ConvertCmd cmd = new ConvertCmd();

            IMOperation op = new IMOperation();
            op.addImage(fullImageFile.getPath());
            op.resize(width);
            op.addImage(thumbnailFile.getPath());

            cmd.run(op);

            return thumbnailImageFilePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertThumbnail(String fullImageFilePath) {
        return convertThumbnail(fullImageFilePath,
                Constants.STATIC_THUMBNAIL_WIDTH);
    }
    */


    public static String trimBigSpace(String s) {
        if (s == null)
            return s;
        if (s.trim().length() == 0)
            return s.trim();

        s = s.replaceAll(" ", " ");
        s = s.trim();
        while (s.startsWith("\u3000")) {
            s = s.substring(1).trim();
        }

        if (s.length() == 0)
            return s;

        while (s.endsWith("\u3000")) {
            s = s.substring(0, s.length() - 1).trim();
        }

        return s;
    }

    @SuppressWarnings("deprecation")
    public static String urlEncode(String string, String charset) {
        if (StringUtils.isEmpty(string)) {
            return "";
        }
        try {
            return URLEncoder.encode(string, charset);
        } catch (UnsupportedEncodingException e) {
            return URLEncoder.encode(string);
        }
    }

    public static String urlEncode(String string) {
        return urlEncode(string, Constants.ENCODING);
    }


    public static boolean stringEmpty(String st) {

        return StringUtils.isEmpty(st);
    }

    public static int versionCompare(String v1, String v2) {
        if (Utils.equals(v1, v2)) {
            return 0;
        }
        if (v1 == null) {
            return -1;
        }
        if (v2 == null) {
            return 1;
        }
        if (v1.isEmpty()) {
            return -1;

        }
        if (v2.isEmpty()) {
            return 1;
        }

        String[] v1Strings = v1.split("\\.");
        String[] v2Strings = v2.split("\\.");
        int n = v1Strings.length > v2Strings.length ? v1Strings.length
                : v2Strings.length;
        for (int i = 0; i < n; i++) {
            int n1 = 0, n2 = 0;
            try {
                n1 = Integer.valueOf(v1Strings[i]);
            } catch (Exception e) {
            }
            try {
                n2 = Integer.valueOf(v2Strings[i]);
            } catch (Exception e) {
            }

            if (n1 > n2) {
                return 1;
            } else if (n2 > n1) {
                return -1;
            }
        }

        return 0;
    }


    @SuppressWarnings("unchecked")
    public static <E extends Object, O extends Object> List<O> listField(
            EntityManager em, Class<E> entityClass, String field) {
        String qlString = "SELECT DISTINCT(e." + field + ") FROM "
                + entityClass.getSimpleName() + " e WHERE NOT e." + field
                + " IS NULL ORDER BY e." + field + "";
        //System.out.println(qlString);
        return em.createQuery(qlString).getResultList();
    }


    public static String uploadFileName(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return "";
        }
        /*
        try {
            return new String(fileName.getBytes("ISO8859-1"),"utf-8");
        } catch (UnsupportedEncodingException e) {
            return fileName;
        }
        */
        StringBuilder sb = new StringBuilder(md5(fileName));

        int idx = fileName.lastIndexOf(".");
        if (idx > -1) {
            sb.append(fileName.substring(idx));
        }

        return sb.toString();
    }

    public static String formatUrlKey(String name) {
        if (name != null && name.length() > 0) {
            name = name.replaceAll("\\\\+", " ");
            name = name.replaceAll("/+", " ");
            name = name.replaceAll("\\?+", " ");
            name = name.replaceAll("&+", " ");
            name = name.replaceAll("=+", " ");
            name = name.replaceAll("\\++", " ");
            name = name.replaceAll("-+", " ");
            name = name.replaceAll("\\.+", " ");
            name = name.trim().toLowerCase();
            return name.replaceAll(" +", "-");
        }
        return "";
    }

    public static String callURL(String url) {
        try {
            logger.debug("callURL: " + url);
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();
            request.setConfig(requestConfig);

            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
                result.append("\n");
            }

            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static int localeLang(Locale locale) {

        if ("en".equals(locale.getLanguage())) {
            return 1;
        } else if ("CN".equalsIgnoreCase(locale.getCountry())) {
            return 2;
        }
        return 0;
    }

    public static String localeText(String tc, String en, String sc, int lang) {
        String out = tc;
        switch (lang) {
            case 1:
                out = en;
                break;
            case 2:
                if (sc == null || tc == sc) {
                    out = Chinese.toSc(tc);
                } else {
                    out = sc;
                }
                break;
        }
        if (StringUtils.isEmpty(out)) {
            out = tc;
        }
        if (out == null) {
            out = "";
        }
        return out;
    }

    public static String removeHtmlTag(String s) {
        if (stringEmpty(s)) {
            return "";
        }
        return s.replaceAll("<.*?>", "");
    }
}
