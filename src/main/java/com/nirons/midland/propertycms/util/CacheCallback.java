package com.nirons.midland.propertycms.util;

/**
 * Created by jaquesyang on 15/7/14.
 */
public interface CacheCallback {
    void execute(String key);
}
