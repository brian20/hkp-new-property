package com.nirons.midland.propertycms.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by jaquesyang on 15/6/18.
 */
public final class Constants {
    public static final String STATUS_PENDING = "P";
    public static final String STATUS_APPROVED = "A";
    public static final String STATUS_REJECTED = "R";
    public static final String STATUS_OVERRIDED = "O";
    public static final String STATUS_ACTIVE = "A";
    public static final String STATUS_INACTIVE = "I";
    public static final String STATUS_DELETE = "DEL";


    public static final String USER_ADMIN = "A";
    public static final String USER_USER = "U";

    public static final String LANG_TC = "zh_HK";
    public static final String LANG_SC = "zh_CN";
    public static final String LANG_EN = "en_HK";

    public static final String PROTOCOL = "protocol";
    public static final String PROTOCOL_DEFAULT = "http";
    public static String STATIC_PROTOCOL = PROTOCOL_DEFAULT;

    public static final String FILE_STORE_DIR = "file_store_dir";
    public static final String FILE_STORE_DIR_DEFAULT = "/home/midland/files";
    public static String STATIC_FILE_STORE_DIR = FILE_STORE_DIR_DEFAULT;

    public static final String FILE_SERVER = "file_server";
    public static final String FILE_SERVER_DEFAULT = "http://127.0.0.1:8003/";
    public static String STATIC_FILE_SERVER = FILE_SERVER_DEFAULT;

    public static final String HKP_FILE_SERVER = "hkp_file_server";
    public static final String HKP_FILE_SERVER_DEFAULT = "http://127.0.0.1:8003/";
    public static String STATIC_HKP_FILE_SERVER = HKP_FILE_SERVER_DEFAULT;


    public static final String ASSETS_SERVER = "assets_server";
    public static final String ASSETS_SERVER_DEFAULT = "http://127.0.0.1:8003/assets/";
    public static String STATIC_ASSETS_SERVER = ASSETS_SERVER_DEFAULT;

    public static final String APP_SERVER = "app_server";
    public static final String APP_SERVER_DEFAULT = "http://127.0.0.1:8081";
    public static String STATIC_APP_SERVER = APP_SERVER_DEFAULT;

    public static final String EN_APP_SERVER = "en_app_server";
    public static final String EN_APP_SERVER_DEFAULT = "http://192.168.31.88:8081";
    public static String STATIC_EN_APP_SERVER = EN_APP_SERVER_DEFAULT;

    public static final String SC_APP_SERVER = "sc_app_server";
    public static final String SC_APP_SERVER_DEFAULT = "http://localhost:8081";
    public static String STATIC_SC_APP_SERVER = SC_APP_SERVER_DEFAULT;

    public static final String DEV_MODE = "dev_mode";
    public static final Boolean DEV_MODE_DEFAULT = Boolean.FALSE;
    public static Boolean STATIC_DEV_MODE = DEV_MODE_DEFAULT;

    public static final String TRICK_FIRST_TIME_URL = "track_first_time_url";
    public static final String TRICK_FIRST_TIME_URL_DEFAULT = "http://midland-fhgrab-uat.nirons.com/index.php/Tracker/track_first_time/[uid]";
    public static String STATIC_TRICK_FIRST_TIME_URL = TRICK_FIRST_TIME_URL_DEFAULT;

    public static final String TPL_TOP_MAIN_TAG = "tpl_top_main_tag";
    public static final String TPL_TOP_MAIN_TAG_DEFAULT = "<main id=\"content\" class=\"container\">";
    public static String STATIC_TPL_TOP_MAIN_TAG = TPL_TOP_MAIN_TAG_DEFAULT;

    public static final String TPL_TOP_TITLE_TAG = "tpl_top_title_tag";
    public static final String TPL_TOP_TITLE_TAG_DEFAULT = "<title>.*?</title>";
    public static String STATIC_TPL_TOP_TITLE_TAG = TPL_TOP_TITLE_TAG_DEFAULT;

    public static final String SRPE_URL_TC = "srpe_url_tc";
    public static final String SRPE_URL_TC_DEFAULT = "https://www.srpe.gov.hk/opip/disclaimer_index_for_all_residential.htm?lang=zh_TW";
    public static String STATIC_SRPE_URL_TC = SRPE_URL_TC_DEFAULT;

    public static final String SRPE_URL_EN = "srpe_url_en";
    public static final String SRPE_URL_EN_DEFAULT = "https://www.srpe.gov.hk/opip/disclaimer_index_for_all_residential.htm?lang=en_US";
    public static String STATIC_SRPE_URL_EN = SRPE_URL_EN_DEFAULT;

    public static final String SRPE_URL_SC = "srpe_url_sc";
    public static final String SRPE_URL_SC_DEFAULT = "https://www.srpe.gov.hk/opip/disclaimer_index_for_all_residential.htm?lang=zh_CN";
    public static String STATIC_SRPE_URL_SC = SRPE_URL_SC_DEFAULT;



    public static final String FOOTER_URL_TC = "footer_url_tc";
    public static final String FOOTER_URL_TC_DEFAULT = "http://midland-fh.nirons.com/new-property/common/tpl-bottom.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_FOOTER_URL_TC = FOOTER_URL_TC_DEFAULT;

    public static final String FOOTER_URL_SC = "footer_url_sc";
    public static final String FOOTER_URL_SC_DEFAULT = "http://sc.midland-fh.nirons.com/new-property/common/tpl-bottom.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_FOOTER_URL_SC = FOOTER_URL_SC_DEFAULT;

    public static final String FOOTER_URL_EN = "footer_url_en";
    public static final String FOOTER_URL_EN_DEFAULT = "http://en.midland-fh.nirons.com/new-property/common/tpl-bottom.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_FOOTER_URL_EN = FOOTER_URL_EN_DEFAULT;





    public static final String HEADER_URL_TC = "header_url_tc";
    public static final String HEADER_URL_TC_DEFAULT = "http://midland-fh.nirons.com/new-property/common/tpl-top-hkp.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_HEADER_URL_TC = HEADER_URL_TC_DEFAULT;

    public static final String HEADER_URL_SC = "header_url_sc";
    public static final String HEADER_URL_SC_DEFAULT = "http://sc.midland-fh.nirons.com/new-property/common/tpl-top-hkp.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_HEADER_URL_SC = HEADER_URL_SC_DEFAULT;

    public static final String HEADER_URL_EN = "header_url_en";
    public static final String HEADER_URL_EN_DEFAULT = "http://en.midland-fh.nirons.com/new-property/common/tpl-top-hkp.html?request-uri=[request-uri]&query-string=[query-string]";
    public static String STATIC_HEADER_URL_EN = HEADER_URL_EN_DEFAULT;



    public static final String yyMMdd = "yyMMdd";
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final DateFormat DF_yyMMdd = new SimpleDateFormat(yyMMdd, Locale.ENGLISH);
    public static final DateFormat DF_yyyy_MM_dd = new SimpleDateFormat(yyyy_MM_dd, Locale.ENGLISH);

    public static final String DOC_VIEW_RECORD = "vr";
    public static final String DOC_SALES_MANUAL_PART = "mp";
    public static final String DOC_SALES_MANUAL_FULL = "mf";
    public static final String DOC_SALES_BROCHURE = "sb";
    public static final String DOC_PRICE_LIST = "pl";
    public static final String DOC_SALES_ARRANGEMENT = "sa";
    public static final String DOC_COMPLETED_RECORD = "cr";

    public static final int REST_SUCCESS = 0;
    public static final int REST_ERR_UNKNOW = 999;
    public static final int REST_ERR_RECORD_NOT_FOUND = 1;
    public static final int REST_ERR_NO_RECORD_FOUND = 2;
    public static final int REST_ERR_FIELD_NOT_FOUND = 3;
    public static final int REST_NAME_EXISTS = 4;
    public static final int REST_ERR_AUTH = 101;

    public static final String REST_BEAN = "bean";
    public static final String REST_LIST = "list";

    public static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    public static final String DESC = "DESC";
    public static final String ASC = "ASC";

    public static final String ENCODING = "UTF-8";

    public static final String ACTION_CREATE = "create";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_APPROVE = "approve";
    public static final String ACTION_OVERRIDE = "override";
    public static final String ACTION_REJECT = "reject";
    public static final String ACTION_SORT = "sort";

    public static final int UNSORTED_VALUE = 9999999;

    public static final long URL_CACHE_TIMEOUT = 12 * 60 * 60 * 1000;

    public static final String[] KEY_WORDS = {"cms", "search", "rest", "api", "district", "developer", "midland-api", "hot", "data", "escaped-fragment"};
}
