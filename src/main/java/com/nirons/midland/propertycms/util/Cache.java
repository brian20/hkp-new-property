package com.nirons.midland.propertycms.util;

import java.util.Date;
import java.util.Hashtable;
import java.util.Set;

/**
 * Created by jaquesyang on 15/7/14.
 */
public final class Cache {

    public static final Hashtable<String, CacheItem> _cacheMap = new Hashtable<>();

    public static Set<String> keys(){
        return _cacheMap.keySet();
    }

    public synchronized static void add(String key, Object value, long timeOut, CacheCallback callback) {
        if (timeOut > 0) {
            timeOut += new Date().getTime();
        }
        CacheItem item = new CacheItem(key, value, timeOut, callback);
        Cache._cacheMap.put(key, item);
    }

    public synchronized static void add(String key, Object value, long timeOut) {
        Cache.add(key, value, timeOut, null);
    }

    public synchronized static void add(String key, Object value) {
        Cache.add(key, value, -1);
    }

    public synchronized static Object get(String key) {
        CacheItem item = Cache._cacheMap.get(key);
        if (item == null) {
            return null;
        }
        boolean expired = Cache.cacheExpired(key);
        if (expired == true) {
            if (item.getCallback() == null) {
                Cache.remove(key);
                return null;
            } else {
                CacheCallback callback = item.getCallback();
                callback.execute(key);
                expired = Cache.cacheExpired(key);
                if (expired == true) {
                    Cache.remove(key);
                    return null;
                }
            }
        }
        return item.getValue();
    }

    public synchronized static void remove(String key) {
        CacheItem item = Cache._cacheMap.get(key);
        if (item != null) {
            Cache._cacheMap.remove(key);
        }
    }

    public synchronized static void clear() {
        Cache._cacheMap.clear();
    }

    private static boolean cacheExpired(String key) {
        CacheItem item = Cache._cacheMap.get(key);
        if (item == null) {
            return false;
        }
        long millisNow = new Date().getTime();
        long millisExpire = item.getTimeOut();
        if (millisExpire <= 0) {
            return false;
        } else if (millisNow >= millisExpire) {
            return true;
        } else {
            return false;
        }
    }
}
