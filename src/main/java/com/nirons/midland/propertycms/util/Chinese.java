package com.nirons.midland.propertycms.util;

import org.springframework.util.StringUtils;

/**
 * Created by jaquesyang on 15/9/2.
 */
public final class Chinese {

    public static final zhcode CONVERT = new zhcode();


    public static String toTc(String string, zhcode convert) {
        if (StringUtils.isEmpty(string)) {
            return string;
        }

        return convert.convertString(string, Encoding.UTF8S, Encoding.UTF8T);
    }


    public static String toTc(String string) {

        return toTc(string, CONVERT);
    }


    public static String toSc(String string, zhcode convert) {
        if (StringUtils.isEmpty(string)) {
            return string;
        }

        return convert.convertString(string, Encoding.UTF8T, Encoding.UTF8S);
    }


    public static String toSc(String string) {

        return toSc(string, CONVERT);
    }
}
