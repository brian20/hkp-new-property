package com.nirons.midland.propertycms.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jaquesyang on 15/7/14.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheItem {

    private String key;
    private Object value;
    private long timeOut;
    private CacheCallback callback;
}
