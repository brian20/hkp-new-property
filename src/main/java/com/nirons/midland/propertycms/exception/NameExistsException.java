package com.nirons.midland.propertycms.exception;

/**
 * Created by jaquesyang on 15/8/18.
 */
public class NameExistsException extends RuntimeException {
    public NameExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NameExistsException(String message) {

        super(message);
    }
}
