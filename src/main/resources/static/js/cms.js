/**
 * Created by jaquesyang on 15/6/27.
 */

var _ie, _firefox, _chrome, _opera, _safari;

function getExplorer() {
    _ie = false;
    _firefox = false;
    _chrome = false;
    _opera = false;
    _safari = false;

    var explorer = window.navigator.userAgent;

    if (explorer.indexOf("MSIE") >= 0) {
        _ie = true;
        return ("ie");
    } else if (explorer.indexOf("Firefox") >= 0) {
        _firefox = true;
        return ("Firefox");
    } else if (explorer.indexOf("Chrome") >= 0) {
        _chrome = true;
        return ("Chrome");
    } else if (explorer.indexOf("Opera") >= 0) {
        _opera = true;
        return ("Opera");
    } else if (explorer.indexOf("Safari") >= 0) {
        _safari = true;
        return ("Safari");
    }

    return "Others";
}

function isIE() {
    if (_ie == undefined) {
        return "ie" == getExplorer();
    }
    return _ie;
}

function isFirefox() {
    if (_firefox == undefined) {
        return "Firefox" == getExplorer();
    }
    return _firefox;
}

function isChrome() {
    if (_chrome == undefined) {
        return "Chrome" == getExplorer();
    }
    return _chrome;
}

function isOpera() {
    if (_opera == undefined) {
        return "Opera" == getExplorer();
    }
    return _opera;
}

function isSafari() {
    if (_safari == undefined) {
        return "Safari" == getExplorer();
    }
    return _safari;
}


function bytesToSize(bytes) {
    if (bytes == 0) {
        return '0 B';
    }
    var k = 1024;
    var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

var loading = "";

$(document).ajaxStart(function () {
    if (loading && loading.length > 0) {
        $(loading).show();
    }
});

$(document).ajaxComplete(function () {
    if (loading && loading.length > 0) {
        $(loading).hide();
    }
});

function showDateFormat(date, format) {
    if (date && $.format.date) {
        if (typeof(date) == 'string' && date.length <= 10) {
            date += ' 00:00:00';
        }
        return $.format.date(date, format);
    }
    return '';
}

var datatable2Rest = function (sSource, aoData, fnCallback) {

    //extract name/value pairs into a simpler map for use later
    var paramMap = {};
    for (var i = 0; i < aoData.length; i++) {
        paramMap[aoData[i].name] = aoData[i].value;
    }

    //page calculations
    var pageSize = paramMap.iDisplayLength;
    var start = paramMap.iDisplayStart;
    var pageNum = (start == 0) ? 1 : (start / pageSize) + 1; // pageNum is 1 based

    // extract sort information
    var sortCol = paramMap.iSortCol_0;
    var sortDir = paramMap.sSortDir_0;
    var sortName = paramMap['mDataProp_' + sortCol];
    var echo = paramMap['sEcho'];

    //create new json structure for parameters for REST request
    var restParams = new Array();
    restParams.push({"name": "size", "value": pageSize});
    restParams.push({"name": "page", "value": pageNum - 1});
    if (sortName && sortName.length > 0) {
        if (sortDir && sortDir.length > 0) {
            sortName += ("," + sortDir);
        }
        restParams.push({"name": "sort", "value": sortName});
    }
    restParams.push({"name": "sEcho", "value": echo});

    var url = sSource;
    if (paramMap.sSearch != '') {
        restParams.push({"name": "name", "value": paramMap.sSearch});
    }

    var selectedStatus = $('.dataTables_status select option:selected').val();

    if (selectedStatus && selectedStatus.length > 0) {
        restParams.push({"name": "status", "value": selectedStatus});
    }

    //finally, make the request
    $.ajax({
        "dataType": 'json',
        "type": "GET",
        "url": url,
        "data": restParams,
        "success": function (data) {
            data.iTotalRecords = data.data.totalElements;
            data.iTotalDisplayRecords = data.data.totalElements;

            fnCallback(data);
        }
    });
};

function dataTableConfig(sAjaxSource, bLengthChange, bFilter, bPaginate, info, bSort, aoColumns, columnDefs, order) {
    return {
        responsive: true,
        "bLengthChange": bLengthChange,
        "bProcessing": true,
        "bAutoWidth": false,
        "bServerSide": true,
        "info": info,
        "sAjaxSource": sAjaxSource,
        "fnServerData": datatable2Rest,
        "bPaginate": bPaginate,
        "bFilter": bFilter,
        "sPaginationType": "full_numbers",
        sAjaxDataProp: "data.content",
        "bSort": bSort,
        "aoColumns": aoColumns,

        "language": dtLanguage,
        columnDefs: columnDefs,
        "order": order
    };
}


function entryDocsDataTableConfirg(url) {
    //var url = cp + "/rest/entry_doc/entry_" + entryId + "/" + docType;
    var aoColumns = [
        {"mDataProp": "nameTc"},
        {"mDataProp": "nameEn"},
        {
            "mDataProp": "updateDate",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.updateDate, dateFormat));
            }
        },
        {
            "mDataProp": "postDate",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.postDate, dateFormat));
            }
        },
        {
            "mDataProp": "printDate",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.printDate, dateFormat));
            }
        },
        {
            "mDataProp": "file.name",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html('<a style="word-wrap: break-word; word-break: break-all;" target="_black" href="' + oData.fileUrl + '">' + oData.file.name + '</a>');
            }
        },
        {
            "mDataProp": "file.created",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.created, dateTimeFormat));
            }
        },
        {"mDataProp": "file.bytes"},
        {
            "mDataProp": "id",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html('<button type="button" class="btn" onclick="showFileReplaceModal(\'' + oData.type + '\', ' + oData.id + ')">Edit</button>');
            }
        },
        {
            "mDataProp": "id",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html('<button type="button" class="btn" onclick="showFileDeleteModal(\'' + oData.type + '\', ' + oData.id + ')">Delete</button>');
            }
        }
    ]
    return dataTableConfig(url, false, false, false, false, false, aoColumns, [], []);
}

function entryDataTableConfirg() {
    var url = cp + "/rest/entry/";
    var aoColumns = [
        {"mDataProp": "uid"},
        {"mDataProp": "nameTc"},
        {"mDataProp": "nameEn"},
        {
            "mDataProp": "hidden",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(oData.hidden ? 'Yes' : 'No');
            }
        },
        {
            "mDataProp": "hiddenForHkp",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(oData.hiddenForHkp ? 'Yes' : 'No');
            }
        },
        {
            "mDataProp": "created",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.created, dateTimeFormat));
            }
        },
        {
            "mDataProp": "modified",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(showDateFormat(oData.modified, dateTimeFormat));
            }
        },
        {
            "mDataProp": "status",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html(entryStatus(oData.status));
            }
        },
        {
            "mDataProp": "id",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html('<a href="' + cp + '/cms/entry/edit/' + oData.id + '"><i class="fa fa-edit fa-2x"/></a>');
            }
        },
        {
            "mDataProp": "id",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html('<a href="javascript:deleteEntry(' + oData.id + ')"><i class="fa fa-trash-o fa-2x"/></a>');
            }
        }
    ]

    return dataTableConfig(url, true, true, true, true, true, aoColumns, [{
        'orderable': false,
        aTargets: [7, 8]
    }], [[5, 'desc']]);
}

function alertMessage(selector, clazz, message) {
    if (!message || message.length == 0) {
        $(selector).removeAttr("class");
        $(selector).html("");
    } else {
        $(selector).attr("class", clazz);
        $(selector).html(message);
    }
}

function successAlert(selector, message) {
    alertMessage(selector, 'alert alert-success', message);
}


function infoAlert(selector, message) {
    alertMessage(selector, 'alert alert-info', message);
}
function failureAlert(selector, message) {
    alertMessage(selector, 'alert alert-danger', message);
}

function toUrlKey(name) {
    if (name && name.length > 0) {
        name = name.replace(/\\+/g, " ");
        name = name.replace(/\/+/g, " ");
        name = name.replace(/\?+/g, " ");
        name = name.replace(/&+/g, " ");
        name = name.replace(/\.+/g, " ");
        name = name.replace(/-+/g, " ");
        name = name.trim();
        return name.replace(/ +/g, "-");
    }
    return "";
}

function messageModal(message, title) {
    var modal = $('#message-modal');
    if (modal) {
        if (title != undefined) {
            $('#message-modal-abel').html(title);
        }
        if (message != undefined) {
            $('#message-modal .modal-body').html(message);
        }
        modal.modal('show');
    }
}